﻿<%@ Page Title="Dist Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DistDetailEU.aspx.cs" Inherits="Haeger2018.DistAdminEU.DistDetailEU" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }

    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">  
         
         <div class="section-title">
            <h2 style="margin: 0;">Haeger European Distributor Details Page</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
         <br />
         <div class="row">
             <div class="col-xs-12">
                <asp:Button ID="btnBackMain" class="btn btn-primary" runat ="server" Text="Back to Main" OnClick="btnBackMain_Click" />
                <asp:Button ID="btnBackAll" class="btn btn-primary" runat ="server" Text="All Distributors" OnClick="btnBackAll_Click" />

             </div>

         </div>
         <div class="row">
             <div class="col-sm-6 col-sm-push-3 col-xs-12">

                 <asp:DetailsView ID="dtvDistEU" CssClass="table table-striped table-bordered table-condensed" runat="server" 
                     Height="100px" Width="500px" AutoGenerateEditButton="True" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="sqlDistEU">
                     <Fields>
                         <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" Visible="false" />
                         <asp:BoundField DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName" />
                         <asp:BoundField DataField="ContactName" HeaderText="Contact Name" SortExpression="ContactName" />
                         <asp:BoundField DataField="ContactPosition" HeaderText="Contact Position" SortExpression="ContactPosition" />
                         <asp:BoundField DataField="ContactPhone" HeaderText="Contact Phone" SortExpression="ContactPhone" />
                         <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                         <asp:BoundField DataField="StreetAddress" HeaderText="StreetAddress" SortExpression="StreetAddress" />
                         <asp:BoundField DataField="CityUSA" HeaderText="City" SortExpression="CityUSA" />
                         <asp:BoundField DataField="StateUSA" HeaderText="State" SortExpression="StateUSA" />
                         <asp:BoundField DataField="ZipCode" HeaderText="Zip Code" SortExpression="ZipCode" />
                         <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
                         <asp:BoundField DataField="FaxNumber" HeaderText="Fax Number" SortExpression="FaxNumber" />
                         <asp:BoundField DataField="LoginCount" HeaderText="Login Count" SortExpression="LoginCount" />
                         <asp:CheckBoxField DataField="LockoutEnabled" HeaderText="Approved" SortExpression="LockoutEnabled" />
                     </Fields>
                 </asp:DetailsView>
                 <asp:SqlDataSource ID="sqlDistEU" runat="server" ConflictDetection="CompareAllValues" 
                     ConnectionString="<%$ ConnectionStrings:aspnet-Haeger2018-MembershipConnectionString-dist-admin-EU %>" 
                     DeleteCommand="DELETE FROM [AspNetUsers] WHERE [Id] = @original_Id AND (([CompanyName] = @original_CompanyName) OR ([CompanyName] IS NULL AND @original_CompanyName IS NULL)) AND (([ContactName] = @original_ContactName) OR ([ContactName] IS NULL AND @original_ContactName IS NULL)) AND (([ContactPosition] = @original_ContactPosition) OR ([ContactPosition] IS NULL AND @original_ContactPosition IS NULL)) AND (([ContactPhone] = @original_ContactPhone) OR ([ContactPhone] IS NULL AND @original_ContactPhone IS NULL)) AND (([Email] = @original_Email) OR ([Email] IS NULL AND @original_Email IS NULL)) AND (([StreetAddress] = @original_StreetAddress) OR ([StreetAddress] IS NULL AND @original_StreetAddress IS NULL)) AND (([CityUSA] = @original_CityUSA) OR ([CityUSA] IS NULL AND @original_CityUSA IS NULL)) AND (([StateUSA] = @original_StateUSA) OR ([StateUSA] IS NULL AND @original_StateUSA IS NULL)) AND (([ZipCode] = @original_ZipCode) OR ([ZipCode] IS NULL AND @original_ZipCode IS NULL)) AND (([Country] = @original_Country) OR ([Country] IS NULL AND @original_Country IS NULL)) AND [FaxNumber] = @original_FaxNumber AND [LoginCount] = @original_LoginCount AND [LockoutEnabled] = @original_LockoutEnabled" 
                     InsertCommand="INSERT INTO [AspNetUsers] ([Id], [CompanyName], [ContactName], [ContactPosition], [ContactPhone], [Email], [StreetAddress], [CityUSA], [StateUSA], [ZipCode], [Country], [FaxNumber], [LoginCount], [LockoutEnabled]) VALUES (@Id, @CompanyName, @ContactName, @ContactPosition, @ContactPhone, @Email, @StreetAddress, @CityUSA, @StateUSA, @ZipCode, @Country, @FaxNumber, @LoginCount, @LockoutEnabled)" 
                                        OldValuesParameterFormatString="original_{0}" 
                     SelectCommand="SELECT [Id], [CompanyName], [ContactName], [ContactPosition], [ContactPhone], [Email], [StreetAddress], [CityUSA], [StateUSA], [ZipCode], [Country], [FaxNumber], [LoginCount], [LockoutEnabled] FROM [AspNetUsers] WHERE ([Id] = @Id)" 
                     UpdateCommand="UPDATE [AspNetUsers] SET [CompanyName] = @CompanyName, [ContactName] = @ContactName, [ContactPosition] = @ContactPosition, [ContactPhone] = @ContactPhone, [Email] = @Email, [StreetAddress] = @StreetAddress, [CityUSA] = @CityUSA, [StateUSA] = @StateUSA, [ZipCode] = @ZipCode, [Country] = @Country, [FaxNumber] = @FaxNumber, [LoginCount] = @LoginCount, [LockoutEnabled] = @LockoutEnabled WHERE [Id] = @original_Id AND (([CompanyName] = @original_CompanyName) OR ([CompanyName] IS NULL AND @original_CompanyName IS NULL)) AND (([ContactName] = @original_ContactName) OR ([ContactName] IS NULL AND @original_ContactName IS NULL)) AND (([ContactPosition] = @original_ContactPosition) OR ([ContactPosition] IS NULL AND @original_ContactPosition IS NULL)) AND (([ContactPhone] = @original_ContactPhone) OR ([ContactPhone] IS NULL AND @original_ContactPhone IS NULL)) AND (([Email] = @original_Email) OR ([Email] IS NULL AND @original_Email IS NULL)) AND (([StreetAddress] = @original_StreetAddress) OR ([StreetAddress] IS NULL AND @original_StreetAddress IS NULL)) AND (([CityUSA] = @original_CityUSA) OR ([CityUSA] IS NULL AND @original_CityUSA IS NULL)) AND (([StateUSA] = @original_StateUSA) OR ([StateUSA] IS NULL AND @original_StateUSA IS NULL)) AND (([ZipCode] = @original_ZipCode) OR ([ZipCode] IS NULL AND @original_ZipCode IS NULL)) AND (([Country] = @original_Country) OR ([Country] IS NULL AND @original_Country IS NULL)) AND [FaxNumber] = @original_FaxNumber AND [LoginCount] = @original_LoginCount AND [LockoutEnabled] = @original_LockoutEnabled">
                     <DeleteParameters>
                         <asp:Parameter Name="original_Id" Type="String" />
                         <asp:Parameter Name="original_CompanyName" Type="String" />
                         <asp:Parameter Name="original_ContactName" Type="String" />
                         <asp:Parameter Name="original_ContactPosition" Type="String" />
                         <asp:Parameter Name="original_ContactPhone" Type="String" />
                         <asp:Parameter Name="original_Email" Type="String" />
                         <asp:Parameter Name="original_StreetAddress" Type="String" />
                         <asp:Parameter Name="original_CityUSA" Type="String" />
                         <asp:Parameter Name="original_StateUSA" Type="String" />
                         <asp:Parameter Name="original_ZipCode" Type="String" />
                         <asp:Parameter Name="original_Country" Type="String" />
                         <asp:Parameter Name="original_FaxNumber" Type="Int32" />
                         <asp:Parameter Name="original_LoginCount" Type="Int32" />
                         <asp:Parameter Name="original_LockoutEnabled" Type="Boolean" />
                     </DeleteParameters>
                     <InsertParameters>
                         <asp:Parameter Name="Id" Type="String" />
                         <asp:Parameter Name="CompanyName" Type="String" />
                         <asp:Parameter Name="ContactName" Type="String" />
                         <asp:Parameter Name="ContactPosition" Type="String" />
                         <asp:Parameter Name="ContactPhone" Type="String" />
                         <asp:Parameter Name="Email" Type="String" />
                         <asp:Parameter Name="StreetAddress" Type="String" />
                         <asp:Parameter Name="CityUSA" Type="String" />
                         <asp:Parameter Name="StateUSA" Type="String" />
                         <asp:Parameter Name="ZipCode" Type="String" />
                         <asp:Parameter Name="Country" Type="String" />
                         <asp:Parameter Name="FaxNumber" Type="Int32" />
                         <asp:Parameter Name="LoginCount" Type="Int32" />
                         <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                     </InsertParameters>
                     <SelectParameters>
                         <asp:SessionParameter Name="Id" SessionField="selectedItem" Type="String" />
                     </SelectParameters>
                     <UpdateParameters>
                         <asp:Parameter Name="CompanyName" Type="String" />
                         <asp:Parameter Name="ContactName" Type="String" />
                         <asp:Parameter Name="ContactPosition" Type="String" />
                         <asp:Parameter Name="ContactPhone" Type="String" />
                         <asp:Parameter Name="Email" Type="String" />
                         <asp:Parameter Name="StreetAddress" Type="String" />
                         <asp:Parameter Name="CityUSA" Type="String" />
                         <asp:Parameter Name="StateUSA" Type="String" />
                         <asp:Parameter Name="ZipCode" Type="String" />
                         <asp:Parameter Name="Country" Type="String" />
                         <asp:Parameter Name="FaxNumber" Type="Int32" />
                         <asp:Parameter Name="LoginCount" Type="Int32" />
                         <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                         <asp:Parameter Name="original_Id" Type="String" />
                         <asp:Parameter Name="original_CompanyName" Type="String" />
                         <asp:Parameter Name="original_ContactName" Type="String" />
                         <asp:Parameter Name="original_ContactPosition" Type="String" />
                         <asp:Parameter Name="original_ContactPhone" Type="String" />
                         <asp:Parameter Name="original_Email" Type="String" />
                         <asp:Parameter Name="original_StreetAddress" Type="String" />
                         <asp:Parameter Name="original_CityUSA" Type="String" />
                         <asp:Parameter Name="original_StateUSA" Type="String" />
                         <asp:Parameter Name="original_ZipCode" Type="String" />
                         <asp:Parameter Name="original_Country" Type="String" />
                         <asp:Parameter Name="original_FaxNumber" Type="Int32" />
                         <asp:Parameter Name="original_LoginCount" Type="Int32" />
                         <asp:Parameter Name="original_LockoutEnabled" Type="Boolean" />
                     </UpdateParameters>
                 </asp:SqlDataSource>
             </div>
         </div>
        

         
     </div>
    



</asp:Content>
