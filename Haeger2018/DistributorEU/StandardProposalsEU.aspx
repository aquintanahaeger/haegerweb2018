﻿<%@ Page Title="Standard Proposals EU" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StandardProposalsEU.aspx.cs" Inherits="Haeger2018.DistributorEU.StandardProposalsEU" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Europe, BV Standard Proposals</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <div class="row">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">
                <h3>English Quotations</h3>
                <hr />
                <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/618MSPe-September-2018-English.xlsx">618 MSPe September 2018</a>
                <br />
                <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/824MSPe-September-2018-English.xlsx">824 MSPe September 2018</a>
                <br />
                <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/824WT4e-September-2018-English.xlsx">824 WT4e September 2018</a>
                <br />
                <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/824OTL-4e-Lite-September-2018-English.xlsx">824 OT4e Lite September 2018</a>              
                <br />
                <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/824OT4e-September-2018-English.xlsx">824 OT4e September 2018</a>
                <br />
                 <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/PS4-Aug-2018-English.xlsx">PS4 August 2018</a>
                <br />
                <br />
                <h3>German Quotations</h3>
                <hr />
                <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/618MSPe-September-2018-German.xlsx">618 MSPe September 2018</a>
                <br />
                <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/824MSPe-September-2018-German.xlsx">824 MSPe September 2018</a>
                <br />
                <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/824WT4e-September-2018-German.xlsx">824 WT4e September 2018</a>
                <br />
                <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/824OTL-4e-Lite-September-2018-German.xlsx">824 OT4e Lite September 2018</a>              
                <br />
                <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/824OT-4e-September-2018-German.xlsx">824 OT4e September 2018</a>
                <br />
                 <a href="/DistributorEU/excelEU/2018MachineStandardProposalEU/PS4-Aug-2018-German.xlsx">PS4 August 2018</a>
                <br />

                <h3>Time Studies</h3>
                <hr />
                <a href="/DistributorEU/excelEU/Energy_savings_calculation_europe.xls">Energy Savings</a>
                <br />
                <a href="/DistributorEU/excelEU/Time_Study_Haeger_2012_WT4e_OT4e_Lite_OT4e_europe.xls">TimeStudy</a>
            </div>
        </div>
    </div>
</asp:Content>
