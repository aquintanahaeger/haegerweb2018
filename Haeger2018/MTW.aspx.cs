﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

namespace Haeger2018
{
    public partial class MTW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check the User Agent
            var agent = Request.UserAgent.ToLower();

            // Check if iOS, Android, or other
            if (agent.Contains("ios"))
            {
                Response.Redirect("https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&uo=4");
            }
            else if (agent.Contains("android"))
            {
                Response.Redirect("https://play.google.com/store/apps/details?id=edu.haeger.haegerwizard&hl=en");
            }
            else if (Request.Browser.IsMobileDevice)
            {
                Response.Redirect("https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&uo=4");
            }

            Wizard1.PreRender += new EventHandler(wzd_PreRender);
        }

        protected void wzd_PreRender(object sender, EventArgs e)
        {
            

        }

        public string GetClassForWizardStep(object wizardStep)
        {
            WizardStep step = wizardStep as WizardStep;

            if (step == null)
            {
                return "";
            }

            int stepIndex = Wizard1.WizardSteps.IndexOf(step);

            if (stepIndex < Wizard1.ActiveStepIndex)
            {
                return "stepCompleted";
            }
            else if (stepIndex > Wizard1.ActiveStepIndex)
            {
                return "stepNotCompleted";
            }
            else
            {
                return "stepCurrent";
            }
        }

        protected void rblMachineSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["mtwMachineSelected"] = rblMachineSelection.SelectedItem.Text;
            var machineSelectedImage = rblMachineSelection.SelectedItem.Value;

            imgMachineSelected.ImageUrl = "images/pngs/" + machineSelectedImage + ".png";
            lblMachineType.Text = "Machine Type:   " + rblMachineSelection.SelectedItem.Text;
            imgMachineSelected.Visible = true;
            lblMachineType.Visible = true;
            lblSetType.Text = "Set Type:   Manual";
            lblSetType.Visible = true;
            Wizard1.MoveTo(this.WizardStep2);
        }

        protected void rblFastener_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["mtwFastenerSelected"] = rblFastener.SelectedItem.Text;

            lblFastener.Text = "Fastener:   " + rblFastener.SelectedItem.Text;
            lblFastener.Visible = true;
            Wizard1.MoveTo(this.WizardStep3);
        }
        protected void rblThreadType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["mtwThreadTypeSelected"] = rblThreadType.SelectedItem.Text;

            lblThreadType.Text = "Thread Type:   " + rblThreadType.SelectedItem.Text;
            lblThreadType.Visible = true;

            // Retrieve data from query
            DataView dvDashLength = (DataView)sqlDashLength.Select(DataSourceSelectArguments.Empty);
            // DataView Length
            var dvDashLengthCount = dvDashLength.Count;
            // ArrayList to hold query results
            ArrayList lstDashLengths = new ArrayList();
            lstDashLengths.Clear();

            // Loop to add query data to array
            for (int i = 0; i <= (dvDashLengthCount - 1); i++)
            {
                for (int j = 0; j <= 1; j++)
                {
                    // Retrieve string from query.
                    var strDash = dvDashLength[i][j].ToString();
                    // Convert query result to integer for sorting.
                    int intDash = Convert.ToInt32(strDash);
                    // Add integer to ArrayList
                    lstDashLengths.Add(intDash);
                }
            }

            // Sort Array List.
            lstDashLengths.Sort();
            // Array list count.
            var lstDashLengthsCount = lstDashLengths.Count - 1;
            int dashLengthMin = Convert.ToInt32(lstDashLengths[0]);
            int dashLengthMax = Convert.ToInt32(lstDashLengths[lstDashLengthsCount]);

            // Clear Radio button list in case user moves to previous before reaching results.
            rblDashLength.Items.Clear();

            // Loop to add dash lengths to radio button list.
            for (int k = dashLengthMin; k <= dashLengthMax; k++)
            {

                rblDashLength.Items.Add(new ListItem(k.ToString(), k.ToString()));

            }

            Wizard1.MoveTo(this.WizardStep4);

        }

        protected void rblDashLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["mtwDashLengthSelected"] = rblDashLength.SelectedItem.Text;

            lblDashLength.Text = "Dash Length:   " + rblDashLength.SelectedItem.Text;
            lblDashLength.Visible = true;
            Wizard1.MoveTo(this.WizardStep5);
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("MTW.aspx");
        }

        protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {


        }

        protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {


        }


    }
}