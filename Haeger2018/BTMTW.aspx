﻿<%@ Page Title="BTM Tooling" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BTMTW.aspx.cs" Inherits="Haeger2018.BTMTW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .ListControl
        {
            display:flex;
            align-items:center;
            justify-content:center;
            margin-left:auto;
            margin-right:auto;

        }
        .ListControl input[type=checkbox], input[type=radio]
        {
            background-color:none;
            display:inline-grid;
            align-items:center;
            width:auto;
            border: 15px solid red;
            margin-left:auto;
            margin-right:auto;
        }
        .ListControl label
        {
            
            width:auto;
            margin-left:10px;
            margin-right:10px;
            padding-left:20px;
            padding-right:10px;
            color:black;
        }
        

        .ValidateControl {
            color:black;
        }

        .machineImage
        {
            display:flex;
            align-items:center;
            justify-content:center;
            width:100%;
            height:100%;
        }

        .resultTop
        {
            color: black;
            font-size: small;

        }

           /* WIZARD */
        .stepNotCompleted
        {
            background-color: rgb(153,153,153);
            width: 15px;
            border: 1px solid rgb(153,153,153);
            margin-right: 5px;
            color: black;
            font-family: Arial;
            font-size: 12px;
            text-align: center;
        }

        .stepCompleted
        {
            background-color: #4d4d4d;
            width: 15px;
            border: 1px solid #4d4d4d;
            color: black;
            font-family: Arial;
            font-size: 12px;
            text-align: center;
        }

        .stepCurrent
        {
            background-color: #e01122;
            width: 15px;
            border: 1px solid #e01122;
            color: Black;
            font-family: Arial;
            font-size: 12px;
            font-weight: normal;
            text-align: center;
        }

        .stepBreak
        {
            width: 3px;
            background-color: Transparent;
        }

        .wizardProgress
        {
            padding-right: 10px;
            font-family: Arial;
            color: #333333;
            font-size: 12px;

        }

        .wizardTitle {
            font-family: Arial;
            font-size: 120%;
            font-weight: normal;
            color: black;
            vertical-align: middle;
            padding-right: 10px;
        }
         span1{
            color:#0085ca;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="container-fluid">
            <div class="section-title">
                    <h2 style="margin: 0;">Haeger BTM&reg Tooling Wizard</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                   
               </div>
            <br />

            <div class="row">
                <div class="col-sm-2">
                    <asp:Image ID="imgBTMMachineSelected" CssClass="machineImage" runat="server" Visible="false" />
                 
                </div>
                <div class="col-sm-3">
                    <asp:Label ID="lblBTMMachineType" CssClass="resultTop" runat="server" Text="Machine Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblPunchThickness" CssClass="resultTop" runat="server" Text="Machine Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblDieThickness" CssClass="resultTop" runat="server" Text="Set Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblPunchDiameter" CssClass="resultTop" runat="server" Text="Thread Type" Visible="false"></asp:Label>
                    <br />
                </div>

                 <%--BTM Tooling Wizard--%>
       
                <div class="col-sm-6">
                    <asp:Wizard ID="BTMWizard" CssClass="ListControl" runat="server" Width="600px" DisplaySideBar="False" 
                                
                                ActiveStepIndex="0" FinishCompleteButtonStyle-CssClass="hidden" StartNextButtonStyle-CssClass="hidden" StepNextButtonStyle-CssClass="hidden">
                        <HeaderTemplate>
                            <table style="width: 100%" >
                                <tr>
                                    <td class="wizardTitle">
                                        <%= BTMWizard.ActiveStep.Title%>
                                    </td>
                                   
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <SideBarTemplate>
                        </SideBarTemplate>
           
                        <StartNavigationTemplate>
                            <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" Text="Next" />
                        </StartNavigationTemplate>

                         <WizardSteps>
                              <%--Choose a Machine Type--%>
                            <asp:WizardStep ID="WizardStep1" runat="server" Title="Choose a Machine Type" StepType="Start">
                                <asp:RadioButtonList ID="rblBTMMachineType" runat="server" AutoPostBack="true"
                                                    RepeatColumns="3" OnSelectedIndexChanged="rblBTMMachineType_SelectedIndexChanged">
                                    <asp:ListItem Value="oldmachines">Old Machines</asp:ListItem>
                                    <asp:ListItem Value="newmachines">Dash 4 & MSPe</asp:ListItem>

                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="ValidateControl" runat="server" 
                                                    ErrorMessage="Please Select a Punch Thickness" ControlToValidate="rblBTMPunchThick">

                                </asp:RequiredFieldValidator>
                                
                            </asp:WizardStep>

                        <%--Choose a Punch Thickness--%>
                            <asp:WizardStep ID="WizardStep2" runat="server" Title="Choose a Punch Thickness">
                                <asp:RadioButtonList ID="rblBTMPunchThick" runat="server" AutoPostBack="true"
                                            DataSourceID="sqlBTMPunchThick" DataTextField="punchThick" DataValueField="punchThick" 
                                            RepeatColumns="2" OnSelectedIndexChanged="rblBTMPunchThick_SelectedIndexChanged">

                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvFCWFastener" CssClass="ValidateControl" runat="server" 
                                                    ErrorMessage="Please Select a Punch Thickness" ControlToValidate="rblBTMPunchThick">

                                </asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="sqlBTMPunchThick" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT DISTINCT [punchThick] FROM [btmToolingData]">
                                   
                                </asp:SqlDataSource>
                            </asp:WizardStep>

                        <%--Choose a Die Thickness--%>
                        <asp:WizardStep ID="WizardStep3" runat="server" Title="Choose a Material">
                        <asp:RadioButtonList ID="rblBTMDieThick" runat="server" RepeatColumns="2" AutoPostBack="true"
                                            DataSourceID="sqlBTMDieThick" DataTextField="material" DataValueField="material" 
                                            OnSelectedIndexChanged="rblBTMDieThick_SelectedIndexChanged">

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvBTMDieThick" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Die Thickness" ControlToValidate="rblBTMDieThick">

                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="sqlBTMDieThick" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT DISTINCT [material] FROM [btmToolingData] 
                                                            WHERE ([punchThick] = @punchThick)">
                            <SelectParameters>
                                <asp:SessionParameter Name="punchThick" SessionField="btmPunchThickSelected" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </asp:WizardStep>

                        <%--Choose a Punch Diameter--%>
                        <asp:WizardStep ID="WizardStep4" runat="server" Title="Choose a Punch Diameter">
                            <asp:RadioButtonList ID="rblBTMPunchDia" runat="server" AutoPostBack="true"
                                                    DataSourceID="sqlBTMPunchDia" DataTextField="punchDia" DataValueField="punchDia" 
                                                    RepeatColumns="2" OnSelectedIndexChanged="rblBTMPunchDia_SelectedIndexChanged" >

                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="rfvBTMPunchDia" runat="server" CssClass="ValidateControl" 
                                                        ErrorMessage="Please Select a Punch Diameter" ControlToValidate="rblBTMPunchDia">

                            </asp:RequiredFieldValidator>
                            <asp:SqlDataSource ID="sqlBTMPunchDia" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                SelectCommand="SELECT [punchDia] FROM [btmToolingData] 
                                                    WHERE (([punchThick] = @punchThick) 
                                                    AND ([material] = @material))">

                                <SelectParameters>
                                    <asp:SessionParameter Name="punchThick" SessionField="btmPunchThickSelected" Type="String" />
                                    <asp:SessionParameter Name="material" SessionField="btmDieThickSelected" Type="String" />
                                </SelectParameters>

                            </asp:SqlDataSource>
                    </asp:WizardStep>

                             <%--BTM Results--%>
                    <asp:WizardStep ID="WizardStep5" runat="server" StepType="Complete" Title="Results">
                       
                        <asp:GridView ID="grdBTMResultOld" CssClass="table table-striped resultTop" runat="server" 
                                        
                                      AutoGenerateColumns="false" Width="100%" text-align="center" DataSourceID="sqlBTMResultsOld" Visible="false">
                         <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <Columns>
                    <asp:BoundField DataField="upperToolOld" HeaderText="Upper Tool" SortExpression="ITEMID" />
                    <asp:BoundField DataField="lowerTool" HeaderText="Lower Tool" SortExpression="ITEMNAME" />
                    <asp:BoundField DataField="punchAssembly" HeaderText="Punch Assembly" SortExpression="price" />
                    <asp:BoundField DataField="dieHolder" HeaderText="Die Holder" SortExpression="qty_onhand" />
                     <asp:BoundField DataField="dieAssy" HeaderText="Die Assembly" SortExpression="price" />
                    <asp:BoundField DataField="tonage" HeaderText="Tonage" SortExpression="qty_onhand" />
                </Columns>

                        
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlBTMResultsOld" runat="server"
                                           ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>"
                                           SelectCommand="SELECT [upperToolOld], [lowerTool], [punchAssembly], 
                                                                    [dieHolder], [dieAssy], [tonage] 
                                                                    FROM [btmToolingData] 
                                                                    WHERE (([punchThick] = @punchThick) 
                                                                    AND ([material] = @material) 
                                                                    AND ([punchDia] = @punchDia))">

                           <SelectParameters>
                               <asp:SessionParameter Name="punchThick" SessionField="btmPunchThickSelected" Type="String" />
                               <asp:SessionParameter Name="material" SessionField="btmDieThickSelected" Type="String" />
                               <asp:SessionParameter Name="punchDia" SessionField="btmPunchDiameterSelected" Type="String" />
                           </SelectParameters>

                        </asp:SqlDataSource>
                        
                        <asp:GridView ID="grdBTMResultNew" CssClass="table table-striped resultTop" runat="server" 
                                        
                                      AutoGenerateColumns="false" Width="100%" text-align="center" DataSourceID="sqlBTMResultsNew" Visible="false">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <Columns>
                    <asp:BoundField DataField="upperToolNew" HeaderText="Upper Tool" SortExpression="ITEMID" />
                    <asp:BoundField DataField="lowerTool" HeaderText="Lower Tool" SortExpression="ITEMNAME" />
                    <asp:BoundField DataField="punchAssembly" HeaderText="Punch Assembly" SortExpression="price" />
                    <asp:BoundField DataField="dieHolder" HeaderText="Die Holder" SortExpression="qty_onhand" />
                     <asp:BoundField DataField="dieAssy" HeaderText="Die Assembly" SortExpression="price" />
                    <asp:BoundField DataField="tonage" HeaderText="Tonage" SortExpression="qty_onhand" />
                </Columns>
                        
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlBTMResultsNew" runat="server"
                                           ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>"
                                           SelectCommand="SELECT [upperToolNew], [lowerTool], [punchAssembly], 
                                                                    [dieHolder], [dieAssy], [tonage] 
                                                                    FROM [btmToolingData] 
                                                                    WHERE (([punchThick] = @punchThick) 
                                                                    AND ([material] = @material) 
                                                                    AND ([punchDia] = @punchDia))">

                           <SelectParameters>
                               <asp:SessionParameter Name="punchThick" SessionField="btmPunchThickSelected" Type="String" />
                               <asp:SessionParameter Name="material" SessionField="btmDieThickSelected" Type="String" />
                               <asp:SessionParameter Name="punchDia" SessionField="btmPunchDiameterSelected" Type="String" />
                           </SelectParameters>

                        </asp:SqlDataSource>

                        
                        <asp:Button ID="btnBTMReset" runat="server" Text="Reset" OnClick="btnBTMReset_Click" />
                        </asp:WizardStep>

                         </WizardSteps>
                    </asp:Wizard>
                    </div>
                </div>
            </div>
</asp:Content>
