﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.PartAdminNA
{
    public partial class CurrentDetail : System.Web.UI.Page
    {


        // Database connection used to store or retrieve time sheets, also to find Partner scheduled hours.
        SqlConnection haegerTimeDB = new SqlConnection(ConfigurationManager.ConnectionStrings["HaegerTimeConnectionString"].ConnectionString);

        // Current date
        DateTime localDate = DateTime.Now;

        // ******************** Test date - DELETE FOR PRODUCTION ********************
        //DateTime localDate = new DateTime(2018, 9, 13);

        int currentYear;

        // Basic info
        string OakdaleID;
        int TrackNum;
        string currentID;
        string fName;
        string lName;
        int weekNumReq;
        DateTime dateCreated;
        string teamLead;
        string showNoShow;
        string reqYear;
        string status;

        // Monday
        Decimal regHoursMon;
        string commentMon;

        // Tuesday
        Decimal regHoursTue;
        string commentTue;

        // Wednesday
        Decimal regHoursWed;
        string commentWed;

        // Thursday
        Decimal regHoursThu;
        string commentThu;

        // Friday
        Decimal regHoursFri;
        string commentFri;

        // Saturday
        Decimal regHoursSat;
        string commentSat;

        // Sunday
        Decimal regHoursSun;
        string commentSun;

        // Vacation Hours
        Decimal vacMon;
        Decimal vacTue;
        Decimal vacWed;
        Decimal vacThu;
        Decimal vacFri;
        Decimal vacSat;
        Decimal vacSun;

        // ARP Hours
        Decimal arpMon;
        Decimal arpTue;
        Decimal arpWed;
        Decimal arpThu;
        Decimal arpFri;
        Decimal arpSat;
        Decimal arpSun;

        // Holiday Hours
        Decimal holMon;
        Decimal holTue;
        Decimal holWed;
        Decimal holThu;
        Decimal holFri;
        Decimal holSat;
        Decimal holSun;

        // Bereavement Hours
        Decimal berMon;
        Decimal berTue;
        Decimal berWed;
        Decimal berThu;
        Decimal berFri;
        Decimal berSat;
        Decimal berSun;

        // Jury Duty Hours
        Decimal jurMon;
        Decimal jurTue;
        Decimal jurWed;
        Decimal jurThu;
        Decimal jurFri;
        Decimal jurSat;
        Decimal jurSun;

        // Define variables for Reg hours and OT
        // Regular Hours
        Decimal regMon;
        Decimal regTue;
        Decimal regWed;
        Decimal regThu;
        Decimal regFri;
        Decimal regSat;
        Decimal regSun;

        // OT Hours
        Decimal otMon;
        Decimal otTue;
        Decimal otWed;
        Decimal otThu;
        Decimal otFri;
        Decimal otSat;
        Decimal otSun;

        // Double OT Hours
        Decimal dotMon;
        Decimal dotTue;
        Decimal dotWed;
        Decimal dotThu;
        Decimal dotFri;
        Decimal dotSat;
        Decimal dotSun;

        Decimal regTotal;
        Decimal vacTotal;
        Decimal arpTotal;
        Decimal holTotal;
        Decimal berTotal;
        Decimal jurTotal;
        Decimal otTotal;
        Decimal dotTotal;

        string teamLeadEmail;

        protected void Page_Load(object sender, EventArgs e)
        {

            // Identify logged in user.
            //currentID = User.Identity.GetUserId();

            // Current cultural information.
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            // Current Week number to verify existing request.
            int weekNumCurrent = ciCurr.Calendar.GetWeekOfYear(localDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            currentYear = ciCurr.Calendar.GetYear(localDate);

            haegerTimeDB.Open();

            // OakdaleTime PK associated with selected row
            string partner_OakdaleID = Session["selectedItemTimeOff"].ToString();

            // Query to get selected Partner's UserID
            string userIDQuery = "SELECT UserID FROM OakdaleTime WHERE _OakdaleID = '" + partner_OakdaleID + "'";
            SqlCommand userIDCommand = new SqlCommand(userIDQuery, haegerTimeDB);

            using (SqlDataReader uID = userIDCommand.ExecuteReader())
            {
                while (uID.Read())
                {
                    currentID = uID[0].ToString();

                }
            }


            // Retrieve first name from PartnerInfo if user is logged in.
            string currentContact = null;
            string contactNameQuery = "Select DISTINCT fName, lname, TeamLead FROM OakdalePartnerInfo WHERE UserID = '" + currentID + "'";
            SqlCommand currentContactCommand = new SqlCommand(contactNameQuery, haegerTimeDB);

            using (SqlDataReader tm = currentContactCommand.ExecuteReader())
            {
                while (tm.Read())
                {
                    fName = tm[0].ToString();
                    lName = tm[1].ToString();
                    teamLead = tm[2].ToString();
                    currentContact = fName + " " + lName;
                }
            }

            // In case no one is logged in.
            if (currentContact != null)
            {
                lblPartnerName.Text = currentContact;
            }
            else
            {
                lblPartnerName.Text = "Nobody";
            }

            string teamLeadEmailQuery = "Select Email from OakdaleTeamLeadInfo WHERE TeamLead = '" + teamLead + "'";
            SqlCommand teamLeadEmailCommand = new SqlCommand(teamLeadEmailQuery, haegerTimeDB);
            using (SqlDataReader tl = teamLeadEmailCommand.ExecuteReader())
            {
                while (tl.Read())
                {
                    teamLeadEmail = tl[0].ToString();

                }
            }



            // Find first day of week
            DateTime Firstday = FirstDateOfWeekISO8601(currentYear, weekNumCurrent);

            // Monday and Sunday of current week.
            string monday = Firstday.ToLongDateString();
            string sunday = Firstday.AddDays(6).ToLongDateString();

            // Week Number Label
            lblWeekNum.Text = weekNumCurrent.ToString();
            // Week Range Label
            lblDateRange.Text = monday + " to " + sunday;

            // Populate the dates for current week.
            lblDateMon.Text = Firstday.ToShortDateString().Trim();
            lblDateTue.Text = (Firstday.AddDays(1)).ToShortDateString().Trim();
            lblDateWed.Text = (Firstday.AddDays(2)).ToShortDateString().Trim();
            lblDateThu.Text = (Firstday.AddDays(3)).ToShortDateString().Trim();
            lblDateFri.Text = (Firstday.AddDays(4)).ToShortDateString().Trim();
            lblDateSat.Text = (Firstday.AddDays(5)).ToShortDateString().Trim();
            lblDateSun.Text = (Firstday.AddDays(6)).ToShortDateString().Trim();

            if (!IsPostBack)
            {
                // Query for OakdaleRequests to see if User has request for current week.
                string requests = "SELECT * FROM OakdaleRequests WHERE UserID = '" + currentID + "' AND WeekNum = " + weekNumCurrent + " ORDER BY DateCreated DESC";
                SqlCommand requestsCommand = new SqlCommand(requests, haegerTimeDB);
                SqlDataReader requestsReader = requestsCommand.ExecuteReader();
                DataTable rqT = new DataTable();
                rqT.Load(requestsReader);
                int rqtCount = rqT.Rows.Count;

                if (rqtCount > 0)
                {
                    lblHrRegMon.Text = rqT.Rows[0][8].ToString();
                    txtHrVacMon.Text = rqT.Rows[0][9].ToString();
                    txtHrARPMon.Text = rqT.Rows[0][10].ToString();
                    txtHrHolidayMon.Text = rqT.Rows[0][11].ToString();
                    txtHrBereaveMon.Text = rqT.Rows[0][12].ToString();
                    txtHrJuryMon.Text = rqT.Rows[0][13].ToString();
                    txtHROTMon.Text = rqT.Rows[0][14].ToString();
                    txtCommMon.Text = rqT.Rows[0][15].ToString();

                    lblHrRegTue.Text = rqT.Rows[0][16].ToString();
                    txtHrVacTue.Text = rqT.Rows[0][17].ToString();
                    txtHrARPTue.Text = rqT.Rows[0][18].ToString();
                    txtHrHolidayTue.Text = rqT.Rows[0][19].ToString();
                    txtHrBereaveTue.Text = rqT.Rows[0][20].ToString();
                    txtHrJuryTue.Text = rqT.Rows[0][21].ToString();
                    txtHROTTue.Text = rqT.Rows[0][22].ToString();
                    txtCommTue.Text = rqT.Rows[0][23].ToString();

                    lblHrRegWed.Text = rqT.Rows[0][24].ToString();
                    txtHrVacWed.Text = rqT.Rows[0][25].ToString();
                    txtHrARPWed.Text = rqT.Rows[0][26].ToString();
                    txtHrHolidayWed.Text = rqT.Rows[0][27].ToString();
                    txtHrBereaveWed.Text = rqT.Rows[0][28].ToString();
                    txtHrJuryWed.Text = rqT.Rows[0][29].ToString();
                    txtHROTWed.Text = rqT.Rows[0][30].ToString();
                    txtCommWed.Text = rqT.Rows[0][31].ToString();

                    lblHrRegThu.Text = rqT.Rows[0][32].ToString();
                    txtHrVacThu.Text = rqT.Rows[0][33].ToString();
                    txtHrARPThu.Text = rqT.Rows[0][34].ToString();
                    txtHrHolidayThu.Text = rqT.Rows[0][35].ToString();
                    txtHrBereaveThu.Text = rqT.Rows[0][36].ToString();
                    txtHrJuryThu.Text = rqT.Rows[0][37].ToString();
                    txtHROTThu.Text = rqT.Rows[0][38].ToString();
                    txtCommThu.Text = rqT.Rows[0][39].ToString();

                    lblHrRegFri.Text = rqT.Rows[0][40].ToString();
                    txtHrVacFri.Text = rqT.Rows[0][41].ToString();
                    txtHrARPFri.Text = rqT.Rows[0][42].ToString();
                    txtHrHolidayFri.Text = rqT.Rows[0][43].ToString();
                    txtHrBereaveFri.Text = rqT.Rows[0][44].ToString();
                    txtHrJuryFri.Text = rqT.Rows[0][45].ToString();
                    txtHROTFri.Text = rqT.Rows[0][46].ToString();
                    txtCommFri.Text = rqT.Rows[0][47].ToString();

                    lblHrRegSat.Text = rqT.Rows[0][48].ToString();
                    txtHrVacSat.Text = rqT.Rows[0][49].ToString();
                    txtHrARPSat.Text = rqT.Rows[0][50].ToString();
                    txtHrHolidaySat.Text = rqT.Rows[0][51].ToString();
                    txtHrBereaveSat.Text = rqT.Rows[0][52].ToString();
                    txtHrJurySat.Text = rqT.Rows[0][53].ToString();
                    txtHROTSat.Text = rqT.Rows[0][54].ToString();
                    txtCommSat.Text = rqT.Rows[0][55].ToString();

                    lblHrRegSun.Text = rqT.Rows[0][56].ToString();
                    txtHrVacSun.Text = rqT.Rows[0][57].ToString();
                    txtHrARPSun.Text = rqT.Rows[0][58].ToString();
                    txtHrHolidaySun.Text = rqT.Rows[0][59].ToString();
                    txtHrBereaveSun.Text = rqT.Rows[0][60].ToString();
                    txtHrJurySun.Text = rqT.Rows[0][61].ToString();
                    txtHROTSun.Text = rqT.Rows[0][62].ToString();
                    txtCommSun.Text = rqT.Rows[0][63].ToString();

                    status = rqT.Rows[0][64].ToString();
                    reqYear = rqT.Rows[0][65].ToString();

                    txtHRDOTMon.Text = rqT.Rows[0][74].ToString();
                    txtHRDOTTue.Text = rqT.Rows[0][75].ToString();
                    txtHRDOTWed.Text = rqT.Rows[0][76].ToString();
                    txtHRDOTThu.Text = rqT.Rows[0][77].ToString();
                    txtHRDOTFri.Text = rqT.Rows[0][78].ToString();
                    txtHRDOTSat.Text = rqT.Rows[0][79].ToString();
                    txtHRDOTSun.Text = rqT.Rows[0][80].ToString();



                }
                else
                {
                    string getNormalHours = "SELECT * FROM OakdaleTimeDefault";

                    SqlCommand getNormalHoursCommand = new SqlCommand(getNormalHours, haegerTimeDB);

                    if (IsPostBack)
                    {
                        using (SqlDataReader gNH = getNormalHoursCommand.ExecuteReader())
                        {
                            while (gNH.Read())
                            {

                                // Monday
                                regHoursMon = Convert.ToDecimal(gNH[8]);

                                // Tuesday
                                regHoursTue = Convert.ToDecimal(gNH[16]);

                                // Wednesday
                                regHoursWed = Convert.ToDecimal(gNH[24]);

                                // Thursday
                                regHoursThu = Convert.ToDecimal(gNH[32]);

                                // Friday
                                regHoursFri = Convert.ToDecimal(gNH[40]);

                                // Saturday
                                regHoursSat = 0.00m;

                                // Sunday
                                regHoursSun = 0.00m;

                                lblHrRegMon.Text = regHoursMon.ToString();
                                lblHrRegTue.Text = regHoursTue.ToString();
                                lblHrRegWed.Text = regHoursWed.ToString();
                                lblHrRegThu.Text = regHoursThu.ToString();
                                lblHrRegFri.Text = regHoursFri.ToString();
                                lblHrRegSat.Text = regHoursSat.ToString("0.00");
                                lblHrRegSun.Text = regHoursSun.ToString("0.00");

                            }
                        }
                    }
                }
            }





            haegerTimeDB.Close();


            //btnCalc_Click(sender, e);

        }

        protected void btnCalc_Click(object sender, EventArgs e)
        {
            // Define MID5 textbox varaibles.
            // Vacation Hours
            vacMon = VerifyNull(txtHrVacMon.Text, txtHrVacMon);
            vacTue = VerifyNull(txtHrVacTue.Text, txtHrVacTue);
            vacWed = VerifyNull(txtHrVacWed.Text, txtHrVacWed);
            vacThu = VerifyNull(txtHrVacThu.Text, txtHrVacThu);
            vacFri = VerifyNull(txtHrVacFri.Text, txtHrVacFri);
            vacSat = VerifyNull(txtHrVacSat.Text, txtHrVacSat);
            vacSun = VerifyNull(txtHrVacSun.Text, txtHrVacSun);


            // ARP Hours
            arpMon = VerifyNull(txtHrARPMon.Text, txtHrARPMon);
            arpTue = VerifyNull(txtHrARPTue.Text, txtHrARPTue);
            arpWed = VerifyNull(txtHrARPWed.Text, txtHrARPWed);
            arpThu = VerifyNull(txtHrARPThu.Text, txtHrARPThu);
            arpFri = VerifyNull(txtHrARPFri.Text, txtHrARPFri);
            arpSat = VerifyNull(txtHrARPSat.Text, txtHrARPSat);
            arpSun = VerifyNull(txtHrARPSun.Text, txtHrARPSun);

            // Holiday Hours
            holMon = VerifyNull(txtHrHolidayMon.Text, txtHrHolidayMon);
            holTue = VerifyNull(txtHrHolidayTue.Text, txtHrHolidayTue);
            holWed = VerifyNull(txtHrHolidayWed.Text, txtHrHolidayWed);
            holThu = VerifyNull(txtHrHolidayThu.Text, txtHrHolidayThu);
            holFri = VerifyNull(txtHrHolidayFri.Text, txtHrHolidayFri);
            holSat = VerifyNull(txtHrHolidaySat.Text, txtHrHolidaySat);
            holSun = VerifyNull(txtHrHolidaySun.Text, txtHrHolidaySun);

            // Bereavement Hours
            berMon = VerifyNull(txtHrBereaveMon.Text, txtHrBereaveMon);
            berTue = VerifyNull(txtHrBereaveTue.Text, txtHrBereaveTue);
            berWed = VerifyNull(txtHrBereaveWed.Text, txtHrBereaveWed);
            berThu = VerifyNull(txtHrBereaveThu.Text, txtHrBereaveThu);
            berFri = VerifyNull(txtHrBereaveFri.Text, txtHrBereaveFri);
            berSat = VerifyNull(txtHrBereaveSat.Text, txtHrBereaveSat);
            berSun = VerifyNull(txtHrBereaveSun.Text, txtHrBereaveSun);

            // Jury Duty Hours
            jurMon = VerifyNull(txtHrJuryMon.Text, txtHrJuryMon);
            jurTue = VerifyNull(txtHrJuryTue.Text, txtHrJuryTue);
            jurWed = VerifyNull(txtHrJuryWed.Text, txtHrJuryWed);
            jurThu = VerifyNull(txtHrJuryThu.Text, txtHrJuryThu);
            jurFri = VerifyNull(txtHrJuryFri.Text, txtHrJuryFri);
            jurSat = VerifyNull(txtHrJurySat.Text, txtHrJurySat);
            jurSun = VerifyNull(txtHrJurySun.Text, txtHrJurySun);


            // Define variables for Reg hours and OT
            // Regular Hours
            regMon = VerifyNull(lblHrRegMon.Text);
            regTue = VerifyNull(lblHrRegTue.Text);
            regWed = VerifyNull(lblHrRegWed.Text);
            regThu = VerifyNull(lblHrRegThu.Text);
            regFri = VerifyNull(lblHrRegFri.Text);
            regSat = VerifyNull(lblHrRegSat.Text);
            regSun = VerifyNull(lblHrRegSun.Text);

            // MID5 Hours = Vacation + ARP + UnpaidLv + Bereave + Volunteer <= 8
            // Calculate MID5 hours
            Decimal mid5TotalMon = vacMon + arpMon + holMon + berMon + jurMon;
            Decimal mid5TotalTue = vacTue + arpTue + holTue + berTue + jurTue;
            Decimal mid5TotalWed = vacWed + arpWed + holWed + berWed + jurWed;
            Decimal mid5TotalThu = vacThu + arpThu + holThu + berThu + jurThu;
            Decimal mid5TotalFri = vacFri + arpFri + holFri + berFri + jurFri;
            Decimal mid5TotalSat = vacSat + arpSat + holSat + berSat + jurSat;
            Decimal mid5TotalSun = vacSun + arpSun + holSun + berSun + jurSun;

            // Array for days of the week
            string[] days = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

            // Array for MID5 totals
            Decimal[] mid5Totals = new Decimal[] { mid5TotalMon, mid5TotalTue, mid5TotalWed, mid5TotalThu, mid5TotalFri };

            // Array for regular hour label names.
            Label[] regHours = new Label[] { lblHrRegMon, lblHrRegTue, lblHrRegWed, lblHrRegThu, lblHrRegFri };

            bool exit = false;

                // Loop to iterate through regular hour labels to enter 8 minus MID5 total.
                for (int i = 0; i <= mid5Totals.Length - 1; i++)
                {
                    if (mid5Totals[i] > 8)
                    {
                        string msg = "The row for " + days[i] + " adds to more than 8 hours. Please make the correction";
                        Response.Write("<script>alert('" + msg + "')</script>");
            
                        exit = true;
                        break;
                    }
                    else
                    {
                        regHours[i].Text = (8 - mid5Totals[i]).ToString("0.00");

                    }


                }
           
                if (exit == true)
                {
                    btnSubmit.Enabled = false;
                    return;
                }




            // Define variables for Reg hours and OT
            // Regular Hours
            regMon = VerifyNull(lblHrRegMon.Text);
            regTue = VerifyNull(lblHrRegTue.Text);
            regWed = VerifyNull(lblHrRegWed.Text);
            regThu = VerifyNull(lblHrRegThu.Text);
            regFri = VerifyNull(lblHrRegFri.Text);
            regSat = VerifyNull(lblHrRegSat.Text);
            regSun = VerifyNull(lblHrRegSun.Text);
            // Overtime Hours
            otMon = VerifyNull(txtHROTMon.Text, txtHROTMon);
            otTue = VerifyNull(txtHROTTue.Text, txtHROTTue);
            otWed = VerifyNull(txtHROTWed.Text, txtHROTWed);
            otThu = VerifyNull(txtHROTThu.Text, txtHROTThu);
            otFri = VerifyNull(txtHROTFri.Text, txtHROTFri);
            otSat = VerifyNull(txtHROTSat.Text, txtHROTSat);
            otSun = VerifyNull(txtHROTSun.Text, txtHROTSun);

            dotMon = VerifyNull(txtHRDOTMon.Text, txtHRDOTMon);
            dotTue = VerifyNull(txtHRDOTTue.Text, txtHRDOTTue);
            dotWed = VerifyNull(txtHRDOTWed.Text, txtHRDOTWed);
            dotThu = VerifyNull(txtHRDOTThu.Text, txtHRDOTThu);
            dotFri = VerifyNull(txtHRDOTFri.Text, txtHRDOTFri);
            dotSat = VerifyNull(txtHRDOTSat.Text, txtHRDOTSat);
            dotSun = VerifyNull(txtHRDOTSun.Text, txtHRDOTSun);


            // Array for column values to total rows.
            Decimal[] colTotalREG = new decimal[] { regMon, regTue, regWed, regThu, regFri, regSat, regSun };

            Decimal[] colTotalVAC = new decimal[] { vacMon, vacTue, vacWed, vacThu, vacFri, vacSat, vacSun };

            Decimal[] colTotalARP = new decimal[] { arpMon, arpTue, arpWed, arpThu, arpFri, arpSat, arpSun };

            Decimal[] colTotalUNL = new decimal[] { holMon, holTue, holWed, holThu, holFri, holSat, holSun };

            Decimal[] colTotalBER = new decimal[] { berMon, berTue, berWed, berThu, berFri, berSat, berSun };

            Decimal[] colTotalVOL = new decimal[] { jurMon, jurTue, jurWed, jurThu, jurFri, jurSat, jurSun };

            Decimal[] colTotalOT = new decimal[] { otMon, otTue, otWed, otThu, otFri, otSat, otSun };

            Decimal[] colTotalDOT = new decimal[] { dotMon, dotTue, dotWed, dotThu, dotFri, dotSat, dotSun };

            Decimal[][] allColumns = new decimal[][] { colTotalREG, colTotalVAC, colTotalARP, colTotalUNL, colTotalBER, colTotalVOL, colTotalOT, colTotalDOT };

            Label[] colLblTotals = new Label[] { lblTotalReg, lblTotalVac, lblTotalARP, lblTotalHoliday, lblTotalBereave, lblTotalJuryDuty, lblTotalOT, lblTotalDOT };

            // Variable for columnTotal.
            Decimal columnTotal = 0;


            // Loop to sum columns and assign to label totals
            for (int j = 0; j <= allColumns.Length - 1; j++)
            {
                for (int k = 0; k <= allColumns[j].Length - 1; k++)
                {
                    columnTotal = columnTotal + allColumns[j][k];

                }

                colLblTotals[j].Text = columnTotal.ToString("0.00");
                columnTotal = 0;
            }

            btnSubmit.Enabled = true;

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // Define MID5 textbox varaibles.
            // Vacation Hours
            vacMon = VerifyNull(txtHrVacMon.Text, txtHrVacMon);
            vacTue = VerifyNull(txtHrVacTue.Text, txtHrVacTue);
            vacWed = VerifyNull(txtHrVacWed.Text, txtHrVacWed);
            vacThu = VerifyNull(txtHrVacThu.Text, txtHrVacThu);
            vacFri = VerifyNull(txtHrVacFri.Text, txtHrVacFri);
            vacSat = VerifyNull(txtHrVacSat.Text, txtHrVacSat);
            vacSun = VerifyNull(txtHrVacSun.Text, txtHrVacSun);


            // ARP Hours
            arpMon = VerifyNull(txtHrARPMon.Text, txtHrARPMon);
            arpTue = VerifyNull(txtHrARPTue.Text, txtHrARPTue);
            arpWed = VerifyNull(txtHrARPWed.Text, txtHrARPWed);
            arpThu = VerifyNull(txtHrARPThu.Text, txtHrARPThu);
            arpFri = VerifyNull(txtHrARPFri.Text, txtHrARPFri);
            arpSat = VerifyNull(txtHrARPSat.Text, txtHrARPSat);
            arpSun = VerifyNull(txtHrARPSun.Text, txtHrARPSun);

            // Holiday Hours
            holMon = VerifyNull(txtHrHolidayMon.Text, txtHrHolidayMon);
            holTue = VerifyNull(txtHrHolidayTue.Text, txtHrHolidayTue);
            holWed = VerifyNull(txtHrHolidayWed.Text, txtHrHolidayWed);
            holThu = VerifyNull(txtHrHolidayThu.Text, txtHrHolidayThu);
            holFri = VerifyNull(txtHrHolidayFri.Text, txtHrHolidayFri);
            holSat = VerifyNull(txtHrHolidaySat.Text, txtHrHolidaySat);
            holSun = VerifyNull(txtHrHolidaySun.Text, txtHrHolidaySun);

            // Bereavement Hours
            berMon = VerifyNull(txtHrBereaveMon.Text, txtHrBereaveMon);
            berTue = VerifyNull(txtHrBereaveTue.Text, txtHrBereaveTue);
            berWed = VerifyNull(txtHrBereaveWed.Text, txtHrBereaveWed);
            berThu = VerifyNull(txtHrBereaveThu.Text, txtHrBereaveThu);
            berFri = VerifyNull(txtHrBereaveFri.Text, txtHrBereaveFri);
            berSat = VerifyNull(txtHrBereaveSat.Text, txtHrBereaveSat);
            berSun = VerifyNull(txtHrBereaveSun.Text, txtHrBereaveSun);

            // Jury Duty Hours
            jurMon = VerifyNull(txtHrJuryMon.Text, txtHrJuryMon);
            jurTue = VerifyNull(txtHrJuryTue.Text, txtHrJuryTue);
            jurWed = VerifyNull(txtHrJuryWed.Text, txtHrJuryWed);
            jurThu = VerifyNull(txtHrJuryThu.Text, txtHrJuryThu);
            jurFri = VerifyNull(txtHrJuryFri.Text, txtHrJuryFri);
            jurSat = VerifyNull(txtHrJurySat.Text, txtHrJurySat);
            jurSun = VerifyNull(txtHrJurySun.Text, txtHrJurySun);


            // Define variables for Reg hours and OT
            // Regular Hours
            regMon = VerifyNull(lblHrRegMon.Text);
            regTue = VerifyNull(lblHrRegTue.Text);
            regWed = VerifyNull(lblHrRegWed.Text);
            regThu = VerifyNull(lblHrRegThu.Text);
            regFri = VerifyNull(lblHrRegFri.Text);
            regSat = VerifyNull(lblHrRegSat.Text);
            regSun = VerifyNull(lblHrRegSun.Text);

            // MID5 Hours = Vacation + ARP + UnpaidLv + Bereave + Volunteer <= 8
            // Calculate MID5 hours
            Decimal mid5TotalMon = vacMon + arpMon + holMon + berMon + jurMon;
            Decimal mid5TotalTue = vacTue + arpTue + holTue + berTue + jurTue;
            Decimal mid5TotalWed = vacWed + arpWed + holWed + berWed + jurWed;
            Decimal mid5TotalThu = vacThu + arpThu + holThu + berThu + jurThu;
            Decimal mid5TotalFri = vacFri + arpFri + holFri + berFri + jurFri;
            Decimal mid5TotalSat = vacSat + arpSat + holSat + berSat + jurSat;
            Decimal mid5TotalSun = vacSun + arpSun + holSun + berSun + jurSun;

            // Array for days of the week
            string[] days = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

            // Array for MID5 totals
            Decimal[] mid5Totals = new Decimal[] { mid5TotalMon, mid5TotalTue, mid5TotalWed, mid5TotalThu, mid5TotalFri };

            // Array for regular hour label names.
            Label[] regHours = new Label[] { lblHrRegMon, lblHrRegTue, lblHrRegWed, lblHrRegThu, lblHrRegFri };



            // Loop to iterate through regular hour labels to enter 8 minus MID5 total.
            for (int i = 0; i <= mid5Totals.Length - 1; i++)
            {
                if (mid5Totals[i] > 8)
                {
                    string msg = "The row for " + days[i] + " adds to more than 8 hours. Please make the correction";
                    Response.Write("<script>alert('" + msg + "')</script>");
                    break;
                }
                else
                {
                    regHours[i].Text = (8 - mid5Totals[i]).ToString("0.00");


                }

            }




            // Define variables for Reg hours and OT
            // Regular Hours
            regMon = VerifyNull(lblHrRegMon.Text);
            regTue = VerifyNull(lblHrRegTue.Text);
            regWed = VerifyNull(lblHrRegWed.Text);
            regThu = VerifyNull(lblHrRegThu.Text);
            regFri = VerifyNull(lblHrRegFri.Text);
            regSat = VerifyNull(lblHrRegSat.Text);
            regSun = VerifyNull(lblHrRegSun.Text);
            // Overtime Hours
            otMon = VerifyNull(txtHROTMon.Text, txtHROTMon);
            otTue = VerifyNull(txtHROTTue.Text, txtHROTTue);
            otWed = VerifyNull(txtHROTWed.Text, txtHROTWed);
            otThu = VerifyNull(txtHROTThu.Text, txtHROTThu);
            otFri = VerifyNull(txtHROTFri.Text, txtHROTFri);
            otSat = VerifyNull(txtHROTSat.Text, txtHROTSat);
            otSun = VerifyNull(txtHROTSun.Text, txtHROTSun);

            dotMon = VerifyNull(txtHRDOTMon.Text, txtHRDOTMon);
            dotTue = VerifyNull(txtHRDOTTue.Text, txtHRDOTTue);
            dotWed = VerifyNull(txtHRDOTWed.Text, txtHRDOTWed);
            dotThu = VerifyNull(txtHRDOTThu.Text, txtHRDOTThu);
            dotFri = VerifyNull(txtHRDOTFri.Text, txtHRDOTFri);
            dotSat = VerifyNull(txtHRDOTSat.Text, txtHRDOTSat);
            dotSun = VerifyNull(txtHRDOTSun.Text, txtHRDOTSun);


            // Array for column values to total rows.
            Decimal[] colTotalREG = new decimal[] { regMon, regTue, regWed, regThu, regFri, regSat, regSun };

            Decimal[] colTotalVAC = new decimal[] { vacMon, vacTue, vacWed, vacThu, vacFri, vacSat, vacSun };

            Decimal[] colTotalARP = new decimal[] { arpMon, arpTue, arpWed, arpThu, arpFri, arpSat, arpSun };

            Decimal[] colTotalUNL = new decimal[] { holMon, holTue, holWed, holThu, holFri, holSat, holSun };

            Decimal[] colTotalBER = new decimal[] { berMon, berTue, berWed, berThu, berFri, berSat, berSun };

            Decimal[] colTotalVOL = new decimal[] { jurMon, jurTue, jurWed, jurThu, jurFri, jurSat, jurSun };

            Decimal[] colTotalOT = new decimal[] { otMon, otTue, otWed, otThu, otFri, otSat, otSun };

            Decimal[] colTotalDOT = new decimal[] { dotMon, dotTue, dotWed, dotThu, dotFri, dotSat, dotSun };

            Decimal[][] allColumns = new decimal[][] { colTotalREG, colTotalVAC, colTotalARP, colTotalUNL, colTotalBER, colTotalVOL, colTotalOT, colTotalDOT };

            Label[] colLblTotals = new Label[] { lblTotalReg, lblTotalVac, lblTotalARP, lblTotalHoliday, lblTotalBereave, lblTotalJuryDuty, lblTotalOT, lblTotalDOT };

            // Variable for columnTotal.
            Decimal columnTotal = 0;


            // Loop to sum columns and assign to label totals
            for (int j = 0; j <= allColumns.Length - 1; j++)
            {
                for (int k = 0; k <= allColumns[j].Length - 1; k++)
                {
                    columnTotal = columnTotal + allColumns[j][k];

                }

                colLblTotals[j].Text = columnTotal.ToString("0.00");
                columnTotal = 0;
            }


            // Totals
            Decimal regTotal = VerifyNull(lblTotalReg.Text);
            Decimal vacTotal = VerifyNull(lblTotalVac.Text);
            Decimal arpTotal = VerifyNull(lblTotalARP.Text);
            Decimal holTotal = VerifyNull(lblTotalHoliday.Text);
            Decimal berTotal = VerifyNull(lblTotalBereave.Text);
            Decimal jurTotal = VerifyNull(lblTotalJuryDuty.Text);
            Decimal otTotal = VerifyNull(lblTotalOT.Text);
            Decimal dotTotal = VerifyNull(lblTotalDOT.Text);

            string comMon = txtCommMon.Text;
            string comTue = txtCommTue.Text;
            string comWed = txtCommWed.Text;
            string comThu = txtCommThu.Text;
            string comFri = txtCommFri.Text;
            string comSat = txtCommSat.Text;
            string comSun = txtCommSun.Text;

            // Get current selected week
            string weekSelected = lblWeekNum.Text.Trim();

            // Check if value is correct length, if not add leading zero. Should be six (MMYYYY).
            if (weekSelected.Length < 6)
            {
                weekSelected = "0" + weekSelected;
            }

            // Break up value to determine week and year.
            int week = Int32.Parse(weekSelected);
            //int currentYear = Int32.Parse(weekSelected.Substring(2, 4));

            // Identify logged in user, current selected week, and selected week's year.
            //string currentID = User.Identity.GetUserId();
            //string weekNumCurrent = weekSelected.Substring(0, 2);

            // Query and commands to retrieve max tracking number
            string trackNumQuery = "SELECT MAX(TrackNum) FROM OakdaleRequests";

            haegerTimeDB.Open();

            SqlCommand trackNumCommand = new SqlCommand(trackNumQuery, haegerTimeDB);
            string trackNum = "";
            using (SqlDataReader tn = trackNumCommand.ExecuteReader())
            {
                while (tn.Read())
                {
                    trackNum = tn[0].ToString();

                }
            }

            // Add one to max tracking number and creating new _OakdaleID
            int nextIDnum = (Int32.Parse(trackNum)) + 1;
            string strOakdaleID = "OAKRQ" + nextIDnum.ToString().Trim();

            // INSERT INTO QUERY
            string insertRequestQuery = "INSERT INTO OakdaleRequests " +
                                        "(_OakdaleRQ_ID, UserID, UserFName, UserLName, WeekNum, DateCreated, TeamLead, " +
                                        "RegHoursMon, VacHoursMon, ArpHoursMon, HolidayHoursMon, BereaveHoursMon, JuryDutyHoursMon, OverTimeHoursMon, DoubleOTHoursMon, CommentMon, " +
                                        "RegHoursTue, VacHoursTue, ArpHoursTue, HolidayHoursTue, BereaveHoursTue, JuryDutyHoursTue, OverTimeHoursTue, DoubleOTHoursTue, CommentTue, " +
                                        "RegHoursWed, VacHoursWed, ArpHoursWed, HolidayHoursWed, BereaveHoursWed, JuryDutyHoursWed, OverTimeHoursWed, DoubleOTHoursWed, CommentWed, " +
                                        "RegHoursThu, VacHoursThu, ArpHoursThu, HolidayHoursThu, BereaveHoursThu, JuryDutyHoursThu, OverTimeHoursThu, DoubleOTHoursThu, CommentThu, " +
                                        "RegHoursFri, VacHoursFri, ArpHoursFri, HolidayHoursFri, BereaveHoursFri, JuryDutyHoursFri, OverTimeHoursFri, DoubleOTHoursFri, CommentFri, " +
                                        "RegHoursSat, VacHoursSat, ArpHoursSat, HolidayHoursSat, BereaveHoursSat, JuryDutyHoursSat, OverTimeHoursSat, DoubleOTHoursSat, CommentSat, " +
                                        "RegHoursSun, VacHoursSun, ArpHoursSun, HolidayHoursSun, BereaveHoursSun, JuryDutyHoursSun, OverTimeHoursSun, DoubleOTHoursSun, CommentSun, " +
                                        "Status, ReqYear, RegHoursTotal, VacHoursTotal, ArpHoursTotal, HolidayHoursTotal, BereaveHoursTotal, JuryDutyHoursTotal, OverTimeHoursTotal, DoubleOTHoursTotal, ShowNoShow)" +
                                        "VALUES (@strOakdaleID, @currentID, @userFName, @userLName, @weekNumCurrent, @localDate, @teamLead, " +
                                        "@monReg, @monVac, @monARP, @monHol, @monBer, @monJur, @monOT, @monDOT, @monCom, " +
                                        "@tueReg, @tueVac, @tueARP, @tueHol, @tueBer, @tueJur, @tueOT,  @tueDOT, @tueCom, " +
                                        "@wedReg, @wedVac, @wedARP, @wedHol, @wedBer, @wedJur, @wedOT,  @wedDOT, @wedCom, " +
                                        "@thuReg, @thuVac, @thuARP, @thuHol, @thuBer, @thuJur, @thuOT,  @thuDOT, @thuCom, " +
                                        "@friReg, @friVac, @friARP, @friHol, @friBer, @friJur, @friOT,  @friDOT, @friCom, " +
                                        "@satReg, @satVac, @satARP, @satHol, @satBer, @satJur, @satOT,  @satDOT, @satCom, " +
                                        "@sunReg, @sunVac, @sunARP, @sunHol, @sunBer, @sunJur, @sunOT,  @sunDOT, @sunCom, " +
                                        "@status, @currentYear, @regTotal, @vacTotal, @arpTotal, @holTotal, @berTotal, @jurTotal, @otTotal, @dotTotal, @showNoShow)";

            SqlCommand insertRequestCommand = new SqlCommand(insertRequestQuery, haegerTimeDB);
            insertRequestCommand.Parameters.AddWithValue("@strOakdaleID", strOakdaleID);
            insertRequestCommand.Parameters.AddWithValue("@currentID", currentID);
            insertRequestCommand.Parameters.AddWithValue("@userFName", fName);
            insertRequestCommand.Parameters.AddWithValue("@userLName", lName);
            insertRequestCommand.Parameters.AddWithValue("@weekNumCurrent", week);
            insertRequestCommand.Parameters.AddWithValue("@localDate", localDate);
            insertRequestCommand.Parameters.AddWithValue("@teamLead", teamLead);
            insertRequestCommand.Parameters.AddWithValue("@monReg", regMon);
            insertRequestCommand.Parameters.AddWithValue("@monVac", vacMon);
            insertRequestCommand.Parameters.AddWithValue("@monARP", arpMon);
            insertRequestCommand.Parameters.AddWithValue("@monHol", holMon);
            insertRequestCommand.Parameters.AddWithValue("@monBer", berMon);
            insertRequestCommand.Parameters.AddWithValue("@monJur", jurMon);
            insertRequestCommand.Parameters.AddWithValue("@monOT", otMon);
            insertRequestCommand.Parameters.AddWithValue("@monCom", comMon);
            insertRequestCommand.Parameters.AddWithValue("@tueReg", regTue);
            insertRequestCommand.Parameters.AddWithValue("@tueVac", vacTue);
            insertRequestCommand.Parameters.AddWithValue("@tueARP", arpTue);
            insertRequestCommand.Parameters.AddWithValue("@tueHol", holTue);
            insertRequestCommand.Parameters.AddWithValue("@tueBer", berTue);
            insertRequestCommand.Parameters.AddWithValue("@tueJur", jurTue);
            insertRequestCommand.Parameters.AddWithValue("@tueOT", otTue);
            insertRequestCommand.Parameters.AddWithValue("@tueCom", comTue);
            insertRequestCommand.Parameters.AddWithValue("@wedReg", regWed);
            insertRequestCommand.Parameters.AddWithValue("@wedVac", vacWed);
            insertRequestCommand.Parameters.AddWithValue("@wedARP", arpWed);
            insertRequestCommand.Parameters.AddWithValue("@wedHol", holWed);
            insertRequestCommand.Parameters.AddWithValue("@wedBer", berWed);
            insertRequestCommand.Parameters.AddWithValue("@wedJur", jurWed);
            insertRequestCommand.Parameters.AddWithValue("@wedOT", otWed);
            insertRequestCommand.Parameters.AddWithValue("@wedCom", comWed);
            insertRequestCommand.Parameters.AddWithValue("@thuReg", regThu);
            insertRequestCommand.Parameters.AddWithValue("@thuVac", vacThu);
            insertRequestCommand.Parameters.AddWithValue("@thuARP", arpThu);
            insertRequestCommand.Parameters.AddWithValue("@thuHol", holThu);
            insertRequestCommand.Parameters.AddWithValue("@thuBer", berThu);
            insertRequestCommand.Parameters.AddWithValue("@thuJur", jurThu);
            insertRequestCommand.Parameters.AddWithValue("@thuOT", otThu);
            insertRequestCommand.Parameters.AddWithValue("@thuCom", comThu);
            insertRequestCommand.Parameters.AddWithValue("@friReg", regFri);
            insertRequestCommand.Parameters.AddWithValue("@friVac", vacFri);
            insertRequestCommand.Parameters.AddWithValue("@friARP", arpFri);
            insertRequestCommand.Parameters.AddWithValue("@friHol", holFri);
            insertRequestCommand.Parameters.AddWithValue("@friBer", berFri);
            insertRequestCommand.Parameters.AddWithValue("@friJur", jurFri);
            insertRequestCommand.Parameters.AddWithValue("@friOT", otFri);
            insertRequestCommand.Parameters.AddWithValue("@friCom", comFri);
            insertRequestCommand.Parameters.AddWithValue("@satReg", regSat);
            insertRequestCommand.Parameters.AddWithValue("@satVac", vacSat);
            insertRequestCommand.Parameters.AddWithValue("@satARP", arpSat);
            insertRequestCommand.Parameters.AddWithValue("@satHol", holSat);
            insertRequestCommand.Parameters.AddWithValue("@satBer", berSat);
            insertRequestCommand.Parameters.AddWithValue("@satJur", jurSat);
            insertRequestCommand.Parameters.AddWithValue("@satOT", otSat);
            insertRequestCommand.Parameters.AddWithValue("@satCom", comSat);
            insertRequestCommand.Parameters.AddWithValue("@sunReg", regSun);
            insertRequestCommand.Parameters.AddWithValue("@sunVac", vacSun);
            insertRequestCommand.Parameters.AddWithValue("@sunARP", arpSun);
            insertRequestCommand.Parameters.AddWithValue("@sunHol", holSun);
            insertRequestCommand.Parameters.AddWithValue("@sunBer", berSun);
            insertRequestCommand.Parameters.AddWithValue("@sunJur", jurSun);
            insertRequestCommand.Parameters.AddWithValue("@sunOT", otSun);
            insertRequestCommand.Parameters.AddWithValue("@sunCom", comSun);
            insertRequestCommand.Parameters.AddWithValue("@status", "Approved");
            insertRequestCommand.Parameters.AddWithValue("@currentYear", currentYear);
            insertRequestCommand.Parameters.AddWithValue("@regTotal", regTotal);
            insertRequestCommand.Parameters.AddWithValue("@vacTotal", vacTotal);
            insertRequestCommand.Parameters.AddWithValue("@arpTotal", arpTotal);
            insertRequestCommand.Parameters.AddWithValue("@holTotal", holTotal);
            insertRequestCommand.Parameters.AddWithValue("@berTotal", berTotal);
            insertRequestCommand.Parameters.AddWithValue("@jurTotal", jurTotal);
            insertRequestCommand.Parameters.AddWithValue("@otTotal", otTotal);
            insertRequestCommand.Parameters.AddWithValue("@dotTotal", dotTotal);
            insertRequestCommand.Parameters.AddWithValue("@showNoShow", "Show");
            insertRequestCommand.Parameters.AddWithValue("@monDOT", dotMon);
            insertRequestCommand.Parameters.AddWithValue("@tueDOT", dotTue);
            insertRequestCommand.Parameters.AddWithValue("@wedDOT", dotWed);
            insertRequestCommand.Parameters.AddWithValue("@thuDOT", dotThu);
            insertRequestCommand.Parameters.AddWithValue("@friDOT", dotFri);
            insertRequestCommand.Parameters.AddWithValue("@satDOT", dotSat);
            insertRequestCommand.Parameters.AddWithValue("@sunDOT", dotSun);

            insertRequestCommand.ExecuteNonQuery();

            haegerTimeDB.Close();

            Response.Redirect("/PartAdminNA/TeamLeadCurrentWeek");

        }

        public Decimal VerifyNull(string textBox, TextBox txtName)
        {
            Decimal value;

            if (textBox == "" || textBox == "0.00")
            {
                value = 0.00m;
            }
            else
            {
                value = Convert.ToDecimal(textBox);
                txtName.Font.Bold = true;
            }

            return value;
        }

        public Decimal VerifyNull(string textBox)
        {
            Decimal value;

            if (textBox == "")
            {
                value = 0.00m;
            }
            else
            {
                value = Convert.ToDecimal(textBox);
            }

            return value;
        }

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            // Use first Thursday in January to get first week of the year as
            // it will never be in Week 52/53
            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            // As we're adding days to a date in Week 1,
            // we need to subtract 1 in order to get the right date for week #1
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            // Using the first Thursday as starting week ensures that we are starting in the right year
            // then we add number of weeks multiplied with days
            var result = firstThursday.AddDays(weekNum * 7);

            // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
            return result.AddDays(-3);
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartAdminNA/TeamLeadHome");
        }
    }
}