﻿<%@ Page Title="Request Summary" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RequestSummary.aspx.cs" Inherits="Haeger2018.PartAdminNA.RequestSummary" maintainScrollPositionOnPostBack = "true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container-fluid">
        <div class="row">       
            <div class="col-xs-6">
                <div class="section-title">
                    <h2>Requests for Time Off</h2> 
                    
                </div>
            </div>
             <div class="col-xs-4">
            </div>
            <div class="col-xs-2">
               <h4>Current Week: <asp:Label ID="lblDate" runat="server" Text="Current Week"></asp:Label></h4>
            </div>  
           
           
            <br />
        </div>
         <div class="row">
            <div class="col-sm-6">
                        <asp:Button CssClass="btn btn-primary" Width="220px" ID="btnBack" runat="server" Text="Back to Main" OnClick="btnBack_Click" />
                        <br />
                        <br />
<%--                        <asp:Button CssClass="btn btn-primary" Width="220px" ID="btnRequestOff" runat="server" Text="Requests for Time Off" OnClick="btnRequestOff_Click" />--%>

                    </div>
                
                    <div class="col-sm-6">
                    </div>
        </div>

        <hr />
        <br />
        <br />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="GridView1" CssClass="table table-striped table-bordered table-condensed" EmptyDataText="No Recent Request" runat="server" 
                                AutoGenerateColumns="False" DataKeyNames="column1" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" AutoGenerateEditButton="False">
                    <Columns>
                        <%--<asp:CommandField ShowDeleteButton="False" DeleteText="Approve" Visible="false"/>--%>
                        <asp:BoundField DataField="column1" HeaderText="column1" ReadOnly="True" SortExpression="column1" Visible="false"/>
                        <asp:BoundField DataField="UserFName" HeaderText="Partner" SortExpression="UserFName" />
                        <asp:BoundField DataField="WeekNum" HeaderText="Week" SortExpression="WeekNum" />
                        <asp:BoundField DataField="DateCreated" HeaderText="Date Created" SortExpression="DateCreated" />
                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                        <asp:BoundField DataField="RegHoursTotal" HeaderText="Regular Hrs" SortExpression="RegHoursTotal" />
                        <asp:BoundField DataField="OverTimeHoursTotal" HeaderText="OverTime Hrs" SortExpression="OverTimeHoursTotal" />
                        <asp:BoundField DataField="VacHoursTotal" HeaderText="Vacation Hrs" SortExpression="VacHoursTotal" />
                        <asp:BoundField DataField="ArpHoursTotal" HeaderText="ARP/Sick Hrs" SortExpression="ArpHoursTotal" />
                        <asp:BoundField DataField="HolidayHoursTotal" HeaderText="Holiday Hrs" SortExpression="HolidayHoursTotal" />
                        <asp:BoundField DataField="JuryDutyHoursTotal" HeaderText="Jury Duty Hrs" SortExpression="JuryDutyHoursTotal" />
                        <asp:BoundField DataField="BereaveHoursTotal" HeaderText="Bereavement Hrs" SortExpression="BereaveHoursTotal" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDetail" CssClass="btn btn-default" runat="server" Text="Detail" CommandName = "SELECT" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HaegerTimeConnectionString %>" 
                    SelectCommand="SELECT [_OakdaleRQ_ID] AS column1, [UserFName], [WeekNum], [DateCreated], [Status], [RegHoursTotal], [OverTimeHoursTotal], 
                                        [VacHoursTotal], [ArpHoursTotal], [HolidayHoursTotal], [JuryDutyHoursTotal], [BereaveHoursTotal] 
                                    FROM [OakdaleRequests] 
                                    WHERE (([TeamLead] = @TeamLead) AND ([Status] = @Status) 
                                    AND ([DateCreated] IN (SELECT MAX(DateCreated) FROM [OakdaleRequests] WHERE ([TeamLead] = @TeamLead) AND ([Status] = @Status) group by UserFName)))
                                    ORDER BY [DateCreated] ASC" 
                    DeleteCommand="UPDATE [OakdaleRequests] SET [Status] = @Approved WHERE [_OakdaleID] = @column1"  
                    InsertCommand="INSERT INTO [OakdaleRequests] ([_OakdaleID], [UserFName], [WeekNum], [DateCreated], [Status], [RegHoursTotal], [OverTimeHoursTotal], 
                                        [VacHoursTotal], [ArpHoursTotal], [HolidayHoursTotal], [JuryDutyHoursTotal], [BereaveHoursTotal]) 
                                    VALUES (@column1, @UserFName, @WeekNum, @DateCreated, @Status, @RegHoursTotal, @OverTimeHoursTotal, @VacHoursTotal, 
                                            @ArpHoursTotal, @HolidayHoursTotal, @JuryDutyHoursTotal, @BereaveHoursTotal)" 
                    UpdateCommand="UPDATE [OakdaleRequests] 
                                    SET [UserFName] = @UserFName, [WeekNum] = @WeekNum, [DateCreated] = @DateCreated, [Status] = @Status, 
                                        [RegHoursTotal] = @RegHoursTotal, [OverTimeHoursTotal] = @OverTimeHoursTotal, [VacHoursTotal] = @VacHoursTotal, 
                                        [ArpHoursTotal] = @ArpHoursTotal, [HolidayHoursTotal] = @HolidayHoursTotal, [JuryDutyHoursTotal] = @JuryDutyHoursTotal, 
                                        [BereaveHoursTotal] = @BereaveHoursTotal 
                                    WHERE [_OakdaleID] = @column1">
                    
                    <DeleteParameters>
                        <asp:Parameter Name="column1" Type="String" />
                        <asp:Parameter DefaultValue="Approved" Name="Approved" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="column1" Type="String" />
                        <asp:Parameter Name="UserFName" Type="String" />
                        <asp:Parameter Name="WeekNum" Type="Int32" />
                        <asp:Parameter Name="DateCreated" Type="DateTime" />
                        <asp:Parameter Name="Status" Type="String" />
                        <asp:Parameter Name="RegHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="OverTimeHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="VacHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="ArpHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="HolidayHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="JuryDutyHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="BereaveHoursTotal" Type="Decimal" />
                    </InsertParameters>
                    
                    <SelectParameters>
                        <asp:SessionParameter Name="TeamLead" SessionField="TeamLead" Type="String" />
                        <asp:Parameter DefaultValue="Pending" Name="Status" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="UserFName" Type="String" />
                        <asp:Parameter Name="WeekNum" Type="Int32" />
                        <asp:Parameter Name="DateCreated" Type="DateTime" />
                        <asp:Parameter Name="Status" Type="String" />
                        <asp:Parameter Name="RegHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="OverTimeHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="VacHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="ArpHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="HolidayHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="JuryDutyHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="BereaveHoursTotal" Type="Decimal" />
                        <asp:Parameter Name="column1" Type="String" />
                        
                    </UpdateParameters>
                </asp:SqlDataSource>


            </div>



        </div>

    </div>


</asp:Content>
