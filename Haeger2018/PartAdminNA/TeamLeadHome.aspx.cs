﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.PartAdminNA
{
    public partial class TeamLeadHome : System.Web.UI.Page
    {
        // Database connection used to identify current user.
        SqlConnection aspUsersDB = new SqlConnection(ConfigurationManager.ConnectionStrings["aspnet-Haeger2018-MembershipConnectionString"].ConnectionString);

        // Database connection used to store or retrieve time sheets, also to find Partner scheduled hours.
        SqlConnection haegerTimeDB = new SqlConnection(ConfigurationManager.ConnectionStrings["HaegerTimeConnectionString"].ConnectionString);


        DateTime localDate = DateTime.Now;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Current cultural information.
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            // Current Week number to verify existing request.
            int weekNumCurrent = ciCurr.Calendar.GetWeekOfYear(localDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            lblDate.Text = "Week " + weekNumCurrent.ToString();

            haegerTimeDB.Open();

            string currentID = User.Identity.GetUserId();
            string currentTeamLead = String.Empty;
            string teamLeadNameQuery = "SELECT Fname, LName from OakdaleTeamLeadInfo WHERE UserID = '" + currentID + "'";
            SqlCommand teamLeadNameCommand = new SqlCommand(teamLeadNameQuery, haegerTimeDB);

            

           

            using (SqlDataReader tLR = teamLeadNameCommand.ExecuteReader())
            {
                while (tLR.Read())
                {
                    currentTeamLead = tLR[0].ToString().Trim() + " " + tLR[1].ToString().Trim();
                }
            }

            string numOfPendingRequestsQuery = "SELECT * FROM OakdaleRequests WHERE TeamLead = '" + currentTeamLead +
                                            "' AND Status =  'Pending' " +
                                            "AND DateCreated IN (SELECT MAX(DateCreated) FROM OakdaleRequests WHERE Status = 'Pending' GROUP BY UserFName)";
            string numOfPending = string.Empty;
            SqlCommand numOfPendingRequestsCommand = new SqlCommand(numOfPendingRequestsQuery, haegerTimeDB);

            SqlDataReader pendingRequestsReader = numOfPendingRequestsCommand.ExecuteReader();
            DataTable prT = new DataTable();
            prT.Load(pendingRequestsReader);
            int prtCount = prT.Rows.Count;



            haegerTimeDB.Close();

            if (prtCount == 0)
            {
                numOfPending = string.Empty;
            }
            else
            {
                numOfPending = prtCount.ToString();
                btnRequestOff.Text = "Requests for Time Off  (" + numOfPending + ")";
            }


            lblPartnerName.Text = currentTeamLead;

            


        }

        protected void btnCurrentWk_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartAdminNA/TeamLeadCurrentWeek");

        }

        protected void btnRequestOff_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartAdminNA/RequestSummary");
        }
    }
}