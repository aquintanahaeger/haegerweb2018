﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.PartAdminNA
{
    public partial class RequestSummary : System.Web.UI.Page
    {
        // Database connection used to identify current user.
        SqlConnection aspUsersDB = new SqlConnection(ConfigurationManager.ConnectionStrings["aspnet-Haeger2018-MembershipConnectionString"].ConnectionString);
        // Database connection used to store or retrieve time sheets, also to find Partner scheduled hours.
        SqlConnection haegerTimeDB = new SqlConnection(ConfigurationManager.ConnectionStrings["HaegerTimeConnectionString"].ConnectionString);

        DateTime localDate = DateTime.Now;

        protected void Page_Load(object sender, EventArgs e)
        {

            haegerTimeDB.Open();

            string currentID = User.Identity.GetUserId();
            string currentTeamLead = String.Empty;
            string teamLeadNameQuery = "SELECT Fname, LName from OakdaleTeamLeadInfo WHERE UserID = '" + currentID + "'";
            SqlCommand teamLeadNameCommand = new SqlCommand(teamLeadNameQuery, haegerTimeDB);

            // Current cultural information.
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            // Current Week number to verify existing request.
            int weekNumCurrent = ciCurr.Calendar.GetWeekOfYear(localDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday);

            lblDate.Text = weekNumCurrent.ToString();

           

            using (SqlDataReader tLR = teamLeadNameCommand.ExecuteReader())
            {
                while (tLR.Read())
                {
                    currentTeamLead = tLR[0].ToString().Trim() + " " + tLR[1].ToString().Trim();
                }
            }

            

            // Team Lead
            Session["TeamLead"] = currentTeamLead;
            haegerTimeDB.Close();

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartAdminNA/TeamLeadHome");

        }



        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["selectedItemTimeOff"] = GridView1.SelectedDataKey.Values["column1"].ToString();
            Response.Redirect("/PartAdminNA/RequestDetail");
        }

    }
}