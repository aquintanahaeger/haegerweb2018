﻿<%@ Page Title="Team Lead Current Week" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TeamLeadCurrentWeek.aspx.cs" Inherits="Haeger2018.PartAdminNA.TeamLeadCurrentWeek" maintainScrollPositionOnPostBack = "true" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">

         <div class="row">       
            <div class="col-xs-6">
                <div class="section-title">
                    <h2>Week Summary</h2>
                    
                    <%--<h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>         --%>       
                </div>
            </div>
             <div class="col-xs-4">
            </div>
            <div class="col-xs-2">
               <h4>Current Week: <asp:Label ID="lblDate" runat="server" Text="Current Week"></asp:Label></h4>
            </div>      
           
           
            <br />
        </div>
        <div class="row">
            <div class="col-sm-6">
                        <asp:Button CssClass="btn btn-primary" Width="220px" ID="btnBack" runat="server" Text="Back to Main" OnClick="btnBack_Click" />
                        <br />
                        <br />
<%--                        <asp:Button CssClass="btn btn-primary" Width="220px" ID="btnRequestOff" runat="server" Text="Requests for Time Off" OnClick="btnRequestOff_Click" />--%>

                    </div>
                
                    <div class="col-sm-6">
                    </div>
        </div>

        <hr />
        <br />
        <br />
        <div class="row">
            
        </div>
         <div class="row">
            <div class="col-xs-12">
                 <%--TLCurrentWeek = Team Lead Current Week--%>
                <asp:GridView ID="grdTLCurrentWeek" CssClass="table table-striped table-bordered table-condensed" runat="server" AutoGenerateColumns="False" DataKeyNames="column1" DataSourceID="sqlTLCurrentWeek" OnSelectedIndexChanged="grdTLCurrentWeek_SelectedIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="column1" HeaderText="column1" ReadOnly="True" SortExpression="column1" Visible="false" />
                        <asp:BoundField DataField="UserID" HeaderText="UserID" SortExpression="UserID" Visible="false" />
                        <asp:BoundField DataField="UserFName" HeaderText="First" SortExpression="UserFName" />
                        <asp:BoundField DataField="UserLName" HeaderText="Last" SortExpression="UserLName" />
                        <asp:BoundField DataField="RegHoursTotal" HeaderText="Regular Hrs" SortExpression="RegHoursTotal" />
                        <asp:BoundField DataField="VacHoursTotal" HeaderText="Vacation Hrs" SortExpression="VacHoursTotal" />
                        <asp:BoundField DataField="ArpHoursTotal" HeaderText="ARP(Sick) Hrs" SortExpression="ArpHoursTotal" />
                        <asp:BoundField DataField="HolidayHoursTotal" HeaderText="Holiday Hrs" SortExpression="HolidayHoursTotal" />
                        <asp:BoundField DataField="BereaveHoursTotal" HeaderText="Bereavement Hrs" SortExpression="BereaveHoursTotal" />
                        <asp:BoundField DataField="JuryDutyHoursTotal" HeaderText="Jury Duty Hrs" SortExpression="JuryDutyHoursTotal" />
                        <asp:BoundField DataField="OverTimeHoursTotal" HeaderText="Overtime Hrs" SortExpression="OverTimeHoursTotal" />
                        <asp:BoundField DataField="DoubleOTHoursTotal" HeaderText="Double OT Hrs" SortExpression="DoubleOTHoursTotal" />
                         <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnDetail1A" CssClass="btn btn-default" runat="server" Text="Detail" CommandName = "SELECT" />
                </ItemTemplate>
            </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlTLCurrentWeek" runat="server" ConnectionString="<%$ ConnectionStrings:HaegerTimeConnectionString %>" 
                    SelectCommand="SELECT [_OakdaleID] AS column1, [UserID], [UserFName], [UserLName], [RegHoursTotal], [VacHoursTotal], 
                                            [ArpHoursTotal], [HolidayHoursTotal], [BereaveHoursTotal], [JuryDutyHoursTotal], [OverTimeHoursTotal], 
                                            [DoubleOTHoursTotal] 
                                    FROM [OakdaleTime] 
                                    WHERE ([WeekNum] = @WeekNum)">
                    <SelectParameters>
                        <asp:SessionParameter Name="WeekNum" SessionField="ssWeekNum" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
           
        </div>
         <div class="row">
            <div class="col-xs-4">
                

            </div>
            <div class="col-xs-4 text-center">
<%--                <asp:Button ID="btnReLoad" CssClass="btn btn-danger" runat="server" Text="Reload" OnClick="btnReLoad_Click" />&nbsp&nbsp&nbsp--%>
                    <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Create Excel File and Email" OnClick="btnSubmit_Click" />


            </div>
            <div class="col-xs-4">


            </div>

        </div>

    </div>




</asp:Content>



