﻿<%@ Page Title="Haeger Europe" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomerEUPage.aspx.cs" Inherits="Haeger2018.CustomerEU.CustomerEUPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1{
            color:#0085ca;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Europe Customer Page</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <div class="row">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">
                <h3>Quick Directory</h3>
                <hr />
                <a href="/ATW">Auto Tooling Wizard</a>
                <br />
                <a href="/MTW">Manual Tooling Wizard</a>
                <br />
                <a href="/BTMTW">BTM Tooling Wizard</a>
                <br />
                <a href="/MachineManuals">Machine Manuals</a>
                <br />
                <a href="/ServiceProcedures">Service Procedures</a>
                <br />
                <a href="/SalesRequest">Request Sales</a>
                <br />
                <a href="/ServiceRequest">Request Service</a>
                <br />
                <a href="/RequestRMA">Request RMA</a>
                <br />
                 <a href="/CustomTooling">Custom Tooling Quotation</a>
                <br />
                <a href="/CustomerEU/PartInformationEU">Part and Tooling Availability</a>
                <br />
                <a href="/CustomerEU/pdf/pricelists/Short-Price-List-EU-July-2018.pdf" target="_blank">Short Price List March 2018</a>
                <br />
                <a href="/pdf/toolingCatalogs/manual-tooling-catalog-021213.pdf" target="_blank">Manual Tooling Catalog</a>
                <br />
               <a href="/CustomerEU/pdf/toolingCatalogs/Auto-Tooling-Catalog-EU-July-2018.pdf" target="_blank">Auto Tooling Catalog July 2018</a>
                <br />
                <a href="/pdf/toolingCatalogs/IMSTP-Tooling-Package.pdf" target="_blank">IMSTP Tooling Package List</a>
                <br />
                <br />
                <br />
                <hr />
                


            </div>
        </div>
    </div>



</asp:Content>
