﻿<%@ Page Title="Part Info EU" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PartInformationEU.aspx.cs" Inherits="Haeger2018.CustomerEU.PartInformationEU" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Europe Part Information</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>

        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="lead text-center">Please enter <b>Part Number</b> or <b>Description</b> and click Enter. If no information is shown, the part number does not exist</p>
                <p class="text-center" style="color:red"><b>NOTE: If Quantity on hand shows 0, then lead time applies. You can also enter a partial number to get the list. <br />If Quantity is 1, it is recommended that you call in to verify the part is in stock.</b></p> 
                <br />
                <br />    
                <h4>Enter Part Number:</h4>
                <asp:TextBox ID="txtPartNumber" CssClass="form-control input-lg center-block" runat="server"></asp:TextBox>
                <asp:Button ID="btnPartNumber" CssClass="btn btn-large" runat="server" Text="Search" />
                <br />
                <br />
                <asp:GridView ID="grdPartNumEU" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="sqlPartNumEU">
                    <Columns>
                        <asp:BoundField DataField="ItemID" HeaderText="Part Number" SortExpression="ItemID" />
                        <asp:BoundField DataField="ItemName" HeaderText="Item Name" SortExpression="ItemName" />
                        <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" DataFormatString="€ {0:n}" />
                        <asp:BoundField DataField="qty_onhand" HeaderText="Quantity" SortExpression="qty_onhand"  DataFormatString="{0:F3}" />
                        <asp:BoundField DataField="LeadTime" HeaderText="Lead Time" SortExpression="LeadTime" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlPartNumEU" runat="server" ConnectionString="<%$ ConnectionStrings:HaegerSalesConnectionString-Dist-PartInfo-EU %>" SelectCommand="SELECT DISTINCT [ItemID], [ItemName], [Price], [qty_onhand], [LeadTime] FROM [haegereuropewebitems] WHERE (([packaginggroupid] = @packaginggroupid) AND ([ItemID] LIKE '%' + @ItemID + '%'))">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Show" Name="packaginggroupid" Type="String" />
                        <asp:ControlParameter ControlID="txtPartNumber" Name="ItemID" PropertyName="Text" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <h4>Enter Description:</h4>
                <asp:TextBox ID="txtDescripEU" CssClass="form-control input-lg center-block" runat="server"></asp:TextBox>
                <asp:Button ID="btnDescripEU" CssClass="btn btn-large" runat="server" Text="Search" />
                <br />
                <br />
                <asp:GridView ID="grdDescripEU" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="sqlDescripEU">
                    <Columns>
                        <asp:BoundField DataField="ItemID" HeaderText="Part Number" SortExpression="ItemID" />
                        <asp:BoundField DataField="ItemName" HeaderText="Item Name" SortExpression="ItemName" />
                        <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" DataFormatString="€ {0:n}" />
                        <asp:BoundField DataField="qty_onhand" HeaderText="Quantity On Hand" SortExpression="qty_onhand" DataFormatString="{0:F3}" />
                        <asp:BoundField DataField="LeadTime" HeaderText="LeadTime" SortExpression="LeadTime"  />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlDescripEU" runat="server" ConnectionString="<%$ ConnectionStrings:HaegerSalesConnectionString-Dist-PartInfo-EU %>" SelectCommand="SELECT DISTINCT [ItemID], [ItemName], [Price], [qty_onhand], [LeadTime] FROM [haegereuropewebitems] WHERE (([ItemName] LIKE '%' + @ItemName + '%') AND ([packaginggroupid] = @packaginggroupid))">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtDescripEU" Name="ItemName" PropertyName="Text" Type="String" />
                        <asp:Parameter DefaultValue="Show" Name="packaginggroupid" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </div>


        </div>
    </div>

    


</asp:Content>
