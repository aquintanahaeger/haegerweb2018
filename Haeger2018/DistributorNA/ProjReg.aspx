﻿<%@ Page Title="Project Registration" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjReg.aspx.cs" Inherits="Haeger2018.ProjReg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    
    <style>
     
         .form-group{
             display: inline-block;
             width: 70%;
         }
        .requiredRed
        {
            width:auto;
            text-align:right;
            color:red;
            font-size:14px;
            font-weight:bold;
        }
         .requiredRed2
        {
            width:auto;
            text-align:center;
            color:red;
            font-size:14px;
            font-weight:bold;
        }
        h7
        {
            font:normal;
            color:black;
            font-size:18px;

        }

        .red
        {
            color:red;
        }

        .rbl input[type="radio"]
        {
           margin-left: 20px;
           margin-right: 1px;
           
        }

        .rbl2 input[type=radio]
        {
            margin-left:10px;
            margin-right:1px;
        }
          span1{
            color:#0085ca;
        }
       input.form-control 
        {
          width:100%;
        }
          
       .search_categories{
  font-size: 13px;
  padding: 10px 8px 10px 14px;
  background: #fff;
  border: 1px solid #ccc;
  border-radius: 6px;
  overflow: hidden;
  position: relative;
}

.search_categories .select{
  width: 120%;
  background:url('arrow.png') no-repeat;
  background-position:80% center;
}

.search_categories .select select{
  background: transparent;
  line-height: 1;
  border: 0;
  padding: 0;
  border-radius: 0;
  width: 120%;
  position: relative;
  z-index: 10;
  font-size: 1em;
}

/* The container */
.container1 {
    display:block;
    position:center;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-size: 16px;
}

/* Hide the browser's default checkbox */
.container1 input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    
}

 .checkbox .btn, .checkbox-inline .btn {
    padding-left: 2em;
    min-width: 8em;
    }
    .checkbox label, .checkbox-inline label {
    text-align: left;
    padding-left: 0.5em;
    }
    .checkbox input[type="checkbox"]{
        float:none;
    }


/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: red;
    
}

.container1:hover {
    background-color:greenyellow;
}

/* On mouse-over, add a grey background color */
.container1:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container1 input:checked ~ .checkmark {
    background-color: #0085ca;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container1 input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container1 .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
</style>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger North America Project Registration</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <hr />
        <div class="form-horizontal text-center">
            <div class="row">
                <div class="col-xs-12">
                    <h7>Items in <h7 class="red">RED</h7> must be filled in</h7>
                </div>
            </div>
        <br />
        <br />
             
        <div class="form-group">
            <%--Company Name--%>
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="txtCompanyName" CssClass="control-label requiredRed pull-right">
                        Company Name:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:TextBox runat="server" ID="txtCompanyName" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtCompanyName"
                                            CssClass="text-danger" ErrorMessage="The company name field is required." />
                </div>  <%--End Company Name--%>
            </div> <%--End Row--%>

             <%--Country--%>
             <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="drpCountry" CssClass="control-label requiredRed pull-right">
                        Country:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:DropDownList ID="drpCountry" class="form-control" style="height:auto; width:auto;" runat="server" DataSourceID="sqlCountry" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="drpCountry_SelectedIndexChanged" DataTextField="country" DataValueField="country">
                        <asp:ListItem Text="----Make a Selection----" Value="" />
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlCountry" runat="server" ConnectionString="<%$ ConnectionStrings:ProjRegConnectionString %>" SelectCommand="SELECT DISTINCT [country] FROM [States] ORDER BY [country] DESC"></asp:SqlDataSource>

                    <asp:RequiredFieldValidator runat="server" ControlToValidate="drpCountry"
                                            CssClass="text-danger" ErrorMessage="The country field is required."  />
                </div>  <%--End Country--%>
            </div> <%--End Row--%>


            <%--Company Address--%>
             <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="txtCompanyAddress" CssClass="control-label requiredRed pull-right">
                        Address:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:TextBox runat="server" ID="txtCompanyAddress" CssClass="form-control" />

                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtCompanyAddress"
                                            CssClass="text-danger" ErrorMessage="The company address field is required."  />
                </div>  <%--End Company Address--%>
            </div> <%--End Row--%>

            
            <%--Company City--%>
             <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="txtCompanyCity" CssClass="control-label requiredRed pull-right">
                        City:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:TextBox runat="server" ID="txtCompanyCity" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtCompanyCity"
                                            CssClass="text-danger" ErrorMessage="The company city field is required."  />
                </div>  <%--End Company City--%>
            </div> <%--End Row--%>

            <%--State, Zip--%>
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">


                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                     <asp:Label runat="server"  ID="lblState" CssClass="control-label requiredRed pull-right">
                        State:
                    </asp:Label>

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <asp:DropDownList class="form-control" style="height:auto; width:auto;" ID="drpState" runat="server" AutoPostBack="True">
                        
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvState" CssClass="text-danger" runat="server" ErrorMessage="The state field is required." ControlToValidate="drpState"></asp:RequiredFieldValidator>
                
                    
                    <%--Zip--%>
                </div>
                 <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                      <asp:Label runat="server"  AssociatedControlID="txtZip" CssClass="control-label requiredRed pull-right">
                        Zip:
                    </asp:Label>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <asp:TextBox ID="txtZip" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvZip" CssClass="text-danger" runat="server" ErrorMessage="The zip field is required." ControlToValidate="txtZip"></asp:RequiredFieldValidator>
                
                </div> <%--End State, Zip--%>
            </div> <%--End Row--%>
            <br />

            

            <%--Phone, Fax--%>
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">


                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                     <asp:Label runat="server"  AssociatedControlID="txtCompanyPhone" CssClass="control-label requiredRed pull-right">
                        Company Phone:
                    </asp:Label>

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <asp:TextBox ID="txtCompanyPhone" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCompPhone" CssClass="text-danger" runat="server" ErrorMessage="The company phone field is required." ControlToValidate="txtCompanyPhone"></asp:RequiredFieldValidator>
                </div>

                 <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                      <asp:Label runat="server"  AssociatedControlID="txtCompanyFax" CssClass="control-label requiredRed pull-right">
                        Company Fax:
                    </asp:Label>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <asp:TextBox ID="txtCompanyFax" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCompFax" CssClass="text-danger" runat="server" ErrorMessage="The company fax field is required." ControlToValidate="txtCompanyFax"></asp:RequiredFieldValidator>
                </div> <%--End Phone, Fax--%>
            </div> <%--End Row--%>
            <br />
            <hr />
             <%--Contact Name--%>
             <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="txtContactName" CssClass="control-label requiredRed pull-right">
                        Contact Name:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:TextBox runat="server" ID="txtContactName" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtContactName"
                                            CssClass="text-danger" ErrorMessage="The contact name field is required."  />
                </div>  <%--End Contact Name--%>
            </div> <%--End Row--%>

             <%--Contact Phone--%>
             <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="txtContactPhone" CssClass="control-label requiredRed pull-right">
                        Contact Phone:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:TextBox runat="server" ID="txtContactPhone" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtContactPhone"
                                            CssClass="text-danger" ErrorMessage="The contact phone field is required."  />
                </div>  <%--End Contact Phone--%>
            </div> <%--End Row--%>

             <%--Contact Email--%>
             <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="txtContactEmail" CssClass="control-label requiredRed pull-right">
                        Contact Email:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:TextBox runat="server" ID="txtContactEmail" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtContactEmail"
                                            CssClass="text-danger" ErrorMessage="The contact email field is required."  />
                    <asp:RegularExpressionValidator ID="revEmail" runat="server"
                                     ErrorMessage= "* Invalid Email Format" CssClass="text-danger" ControlToValidate="txtContactEmail"
                                     SetFocusOnError="True"
                                     ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
                </div>  <%--End Contact Email--%>
            </div> <%--End Row--%>

             <%--Contact Position--%>
             <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="txtContactPosition" CssClass="control-label requiredRed pull-right">
                        Contact Position:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:TextBox runat="server" ID="txtContactPosition" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtContactPosition"
                                            CssClass="text-danger" ErrorMessage="The contact position field is required."  />
                </div>  <%--End Contact Position--%>
            </div> <%--End Row--%>

            <%--Business, Industry Type--%>
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>

                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                     <asp:Label runat="server"  AssociatedControlID="drpBusType" CssClass="control-label requiredRed pull-right" >
                        Business Type:
                    </asp:Label>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <asp:DropDownList class="form-control" style="height:auto; width:auto;" ID="drpBusType"  runat="server" DataSourceID="sqlBusType" DataTextField="busType" DataValueField="busType" AppendDataBoundItems="true" >
                        <asp:ListItem Text="-Make a Selection-" Value="" />
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlBusType" runat="server" ConnectionString="<%$ ConnectionStrings:ProjRegConnectionString %>" SelectCommand="SELECT [busType] FROM [BusinessType]"></asp:SqlDataSource>
                    <asp:RequiredFieldValidator ID="rfvBusType" CssClass="text-danger" runat="server" ErrorMessage="The business type field is required." ControlToValidate="drpBusType"></asp:RequiredFieldValidator>
                </div>

                 <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                      <asp:Label runat="server"  AssociatedControlID="drpIndType" CssClass="control-label requiredRed pull-right">
                        Industry Type:
                    </asp:Label>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <asp:DropDownList class="form-control" style="height:auto; width:auto;" ID="drpIndType"  runat="server" DataSourceID="sqlIndType" DataTextField="indType" DataValueField="indType" AppendDataBoundItems="true" >
                        <asp:ListItem Text="-Make a Selection-" Value="" />
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlIndType" runat="server" ConnectionString="<%$ ConnectionStrings:ProjRegConnectionString %>" SelectCommand="SELECT [indType] FROM [IndustryType]"></asp:SqlDataSource>
                    <asp:RequiredFieldValidator ID="rfvIndType" CssClass="text-danger" runat="server" ErrorMessage="The industry type field is required." ControlToValidate="drpIndType"></asp:RequiredFieldValidator>
                </div> <%--End Business, Industry Type--%>
            </div> <%--End Row--%>
            <br />
            
             <%--SalesPerson--%>
             <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="txtSalesPerson" CssClass="control-label requiredRed pull-right">
                        Sales Person:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:TextBox runat="server" ID="txtSalesPerson" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtSalesPerson"
                                            CssClass="text-danger" ErrorMessage="The salesperson field is required."  />
                </div>  <%--End SalesPerson--%>
            </div> <%--End Row--%>

             <%--Distributor--%>
             <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">

                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="drpDistNames" CssClass="control-label requiredRed pull-right">
                        Distributor:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:DropDownList ID="drpDistNames" class="form-control" style="height:auto; width:auto;" runat="server" AppendDataBoundItems="True" DataSourceID="sqlDistNames" DataTextField="distributorName" DataValueField="distributorName">
                                                <asp:ListItem Text="---Make a Selection---" Value="" />


                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlDistNames" runat="server" ConnectionString="<%$ ConnectionStrings:ProjRegConnectionString %>" SelectCommand="SELECT DISTINCT [distributorName] FROM [ProjRegData] ORDER BY [distributorName]"></asp:SqlDataSource>

                    <asp:RequiredFieldValidator runat="server" ControlToValidate="drpDistNames"
                                            CssClass="text-danger" ErrorMessage="The distributor field is required."  />
                </div>  <%--End Distributor--%>
            </div> <%--End Row--%>

            
            <%--PriorityMonth--%>
            <div class="row">
                <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12">


                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-10">
                     <asp:Label runat="server" style="width:auto;" AssociatedControlID="drpPriority" CssClass="control-label requiredRed pull-right" >
                        Priority Month to Close:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:DropDownList class="form-control" style="height:auto; width:250px;" ID="drpPriority"  runat="server" DataSourceID="sqlPriority" DataTextField="prMthOption" DataValueField="prMthOption" AppendDataBoundItems="True" >
                        <asp:ListItem Text="---Make a Selection---" Value="" />
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlPriority" runat="server" ConnectionString="<%$ ConnectionStrings:ProjRegConnectionString %>" SelectCommand="SELECT [prMthOption] FROM [PriorityMonth]"></asp:SqlDataSource>
                    <asp:RequiredFieldValidator ID="rfvPriority" CssClass="text-danger" runat="server" ErrorMessage="The priority month to close field is required." ControlToValidate="drpPriority"></asp:RequiredFieldValidator> 
                </div>  <%--End PriorityMonth--%>
            </div>  <%--End Row--%>

            <br />
             <%--machineType--%>
            <div class="row">
                <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12">

                </div>
                 <div class="col-md-3 col-lg-3 col-sm-3 col-xs-8">
                      <asp:Label runat="server"  AssociatedControlID="drpMachineType" CssClass="control-label requiredRed pull-right">
                        Project Machine Type:
                    </asp:Label>

                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:DropDownList class="form-control" style="height:auto; width:250px;" ID="drpMachineType"  runat="server" DataSourceID="sqlMachineType" DataTextField="machineType" DataValueField="machineType" AppendDataBoundItems="True" >
                        <asp:ListItem Text="---Make a Selection---" Value="" />
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlMachineType" runat="server" ConnectionString="<%$ ConnectionStrings:ProjRegConnectionString %>" SelectCommand="SELECT [machineType] FROM [machineNames]"></asp:SqlDataSource>
                    <asp:RequiredFieldValidator ID="rfvMachineType" CssClass="text-danger" runat="server" ErrorMessage="The machine type field is required." ControlToValidate="drpMachineType"></asp:RequiredFieldValidator>
                </div> <%--End machineType--%>
            </div> <%--End Row--%>
            <br />
            <hr />

             <%--Estimated Clinch Purchases--%>
             <div class="row">
                
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                     <asp:Label runat="server"  AssociatedControlID="drpClinchPurch" CssClass="control-label requiredRed text-nowrap pull-right">
                        Total Estimated Clinch Purchases per Year:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 form-inline">
                    <asp:DropDownList ID="drpClinchPurch" class="form-control" style="height:auto; width:250px;" AppendDataBoundItems="True"  runat="server" DataTextFormatString="{0:C0}">
                        <asp:ListItem Text="---Make a Selection---" Value="" />

                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Clinch Purchases required" ControlToValidate="drpClinchPurch"
                                            CssClass="text-danger"></asp:RequiredFieldValidator>

                </div>  <%--End Estimated Clinch Purchases--%>
            </div> <%--End Row--%>

            <br />
             <%--Estimated % From PEM--%>
             <div class="row">
                
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="drpEstPrcnt" CssClass="control-label requiredRed pull-right">
                        Estimated % purchased from PEM:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:DropDownList ID="drpEstPrcnt" class="form-control" style="height:auto; width:250px;" runat="server" AppendDataBoundItems="True" DataSourceID="sqlEstPrcnt" DataTextField="pemPercent" DataValueField="pemPercent" DataTextFormatString="{0:P0}">
                                                <asp:ListItem Text="---Make a Selection---" Value="" />


                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlEstPrcnt" runat="server" ConnectionString="<%$ ConnectionStrings:ProjRegConnectionString %>" SelectCommand="SELECT [pemPercent] FROM [PEMPercentage] ORDER BY [pemPercent]"></asp:SqlDataSource>

                    <asp:RequiredFieldValidator runat="server" ControlToValidate="drpEstPrcnt"
                                            CssClass="text-danger" ErrorMessage="The percentage field is required."  />
                </div>  <%--End Estimated % From PEM--%>
            </div> <%--End Row--%>

            <%--Competitor--%>
             <div class="row">
                
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-8">
                    <asp:Label runat="server"  AssociatedControlID="txtOtherClinch" CssClass="control-label requiredRed pull-right">
                        Other Type Clinch Used:
                    </asp:Label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <asp:TextBox runat="server" ID="txtOtherClinch" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtOtherClinch"
                                            CssClass="text-danger" ErrorMessage="The other clinch field is required."  />
                </div>  <%--End Competitor--%>
            </div> <%--End Row--%>
            <br />
            <br />

            <%--GDPR Consent Fail Reminder--%>
            <div class="row">
                 <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12 text-nowrap">
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    <span>
                        <asp:Label ID="lblConsentWrn" CssClass="pull-left" runat="server" BorderStyle="Solid" BorderColor="GreenYellow" Text="" Visible="false">
                             <i class="fa fa-arrow-down"></i> Consent must be checked 
                        </asp:Label>
                    </span>
                </div>
            </div>
            <%--END GDPR Consent Fail Reminder--%>

            <%--GDPR Consent Checkbox--%>
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 text-nowrap">
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    <label class="container1">
                         <p>I have read the 
                            <asp:HyperLink ID="hlPrivacy" runat="server" NavigateUrl="/PrivacyPolicy" Target="_blank">Privacy Policy</asp:HyperLink>
                            /
                            <asp:HyperLink ID="hlTOS" runat="server" NavigateUrl="/TOS" Target="_blank">Terms of Use</asp:HyperLink>. 
                            I consent to the collection and use of the personal data submitted on the form above.</p>
                        <input id="gdprConsentBox" runat="server" type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <%--END GDPR Consent Checkbox--%>
            <br />
            <br />

        <%--Button, Result Label, Validation Summary--%>
        <div class="form-group">
            <div class="col-xs-12">
                <asp:Button ID="btnProjRegSubmit" runat="server" Text="Submit" Width="100%" CssClass="btn btn-large btn-block btn-primary center-block" OnClick="btnProjRegSubmit_Click" />
                <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>
                <asp:ValidationSummary ID="ValidationSummaryProjReg" Width="100%" CssClass="text-danger center-block" runat="server" />

            </div>
        </div>  




        </div> <%--End FormGroup--%> 
           
    </div>




    </div>

</asp:Content>

