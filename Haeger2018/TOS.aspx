﻿<%@ Page Title="Website Terms of Service" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TOS.aspx.cs" Inherits="Haeger2018.TOS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }
        p,li
        {
            font-size:16px;
        }
        
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
         <div class="section-title">
            <h2 style="margin: 0;">Haeger Website Terms of Service - May 2018</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <hr />
        <div class="row">
           <div class="col-xs-12">
               <h3><b>Acceptance of the Terms of Use</b></h3>
               <p>
                   These terms of use are entered into by and between you and Haeger Incorporated&reg, a <span1> PennEngineering&reg</span1> Company (“<b>Company</b>” or “<b>We</b>”). 
                   The following terms and conditions, together with any documents they expressly incorporate by reference (collectively, these “<b>Terms of Use</b>”), 
                   govern your access to and use of <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl=" https://www.haeger.com/">https://www.haeger.com/</asp:HyperLink>, including any content, 
                   functionality and services offered on or through <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl=" https://www.haeger.com/">https://www.haeger.com/</asp:HyperLink> (the “<b>Website</b>”).

               </p>
               <p>
                   Please read the Terms of Use carefully before you start to use the Website. <b>By using the Website, you accept and agree to be bound and abide by these Terms of Use and our <asp:HyperLink ID="hlPrivacy" runat="server" NavigateUrl="/PrivacyPolicy" Target="_blank">Privacy Policy</asp:HyperLink>, 
                   found at <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="/PrivacyPolicy" Target="_blank">https://www.haeger.com/PrivacyPolicy</asp:HyperLink>, incorporated herein by reference.</b> 
                   If you do not want to agree to these Terms of Use or the <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="/PrivacyPolicy" Target="_blank">Privacy Policy</asp:HyperLink>, you must not access or use the Website.
               </p>

               <h3><b>Changes to the Terms of Use</b></h3>
               <p>
                   We may revise and update these Terms of Use from time to time in our sole discretion. All changes are effective immediately when we post them. Your continued use of the Website 
                   following the posting of revised Terms of Use means that you accept and agree to the changes. You are expected to check this page from time to time so you are aware of any changes, 
                   as they are binding on you.
               </p>

               <h3><b>Accessing the Website and Account Security</b></h3>
               <p>
                   We reserve the right to withdraw or amend this Website, and any service or material we provide on the Website, in our sole discretion without notice. We will not be liable if for 
                   any reason all or any part of the Website is unavailable at any time or for any period. From time to time, we may restrict access to some parts of the Website, or the entire Website, 
                   to users, including registered users.
               </p>
               <p>
                   You are responsible for:
                   <ul>
                       <li>Making all arrangements necessary for you to have access to the Website.</li>
                       <li>Ensuring that all persons who access the Website through your internet connection are aware of these Terms of Use and comply with them.</li>
                   </ul>
               </p>
               <p>
                   If you choose, or are provided with, a user name, password or any other piece of information as part of our security procedures, you must treat such information as confidential, 
                   and you must not disclose it to any other person or entity. You also acknowledge that your account is personal to you and agree not to provide any other person with access to this 
                   Website or portions of it using your user name, password or other security information. You agree to notify us immediately of any unauthorized access to or use of your user name or 
                   password or any other breach of security. You also agree to ensure that you exit from your account at the end of each session. You should use particular caution when accessing your 
                   account from a public or shared computer so that others are not able to view or record your password or other personal information.
               </p>
               <p>
                   We have the right to disable any user name, password or other identifier, whether chosen by you or provided by us, at any time in our sole discretion for any or no reason, 
                   including if, in our opinion, you have violated any provision of these Terms of Use.
               </p>

               <h3><b>Intellectual Property Rights</b></h3>
               <p>
                   The Website and its entire contents, features and functionality (including but not limited to all information, software, text, displays, images, video and audio, and the design, 
                   selection and arrangement thereof), are owned by the Company, its licensors or other providers of such material and are protected by United States and international copyright, 
                   trademark, patent, trade secret and other intellectual property or proprietary rights laws.
               </p>
               <p>
                   These Terms of Use permit you to use the Website for your personal, non-commercial use only. You must not reproduce, distribute, modify, create derivative works of, publicly display, 
                   publicly perform, republish, download, store or transmit any of the material on our Website, except as follows:
                   <ul>
                       <li>Your computer may temporarily store copies of such materials in RAM incidental to your accessing and viewing those materials.</li>
                       <li>You may store files that are automatically cached by your Web browser for display enhancement purposes.</li>
                       <li>You may print [or download] one copy of a reasonable number of pages of the Website for your own personal, non-commercial use and not for further reproduction, 
                           publication or distribution.</li>
                   </ul>
               </p>
               <p>
                   You <b><i>MUST NOT</i></b>:
                   <ul>
                       <li>Modify copies of any materials from this site.</li>
                       <li>Delete or alter any copyright, trademark or other proprietary rights notices from copies of materials from this site.</li>
                   </ul>

               </p>
               <p>
                   You <b><i>MUST NOT</i></b> access or use for any commercial purposes any part of the Website or any services or materials available through the Website.
               </p>
               <p>
                   If you print, copy, modify, download or otherwise use or provide any other person with access to any part of the Website in breach of the Terms of Use, 
                   your right to use the Website will cease immediately and you must, at our option, return or destroy any copies of the materials you have made. 
                   No right, title or interest in or to the Website or any content on the Website is transferred to you, and all rights not expressly granted are reserved by the Company. 
                   Any use of the Website not expressly permitted by these Terms of Use is a breach of these Terms of Use and may violate copyright, trademark and other laws.
               </p>

               <h3><b>Trademarks</b></h3>
               <p>
                   The Company name, the Company logo, and all related names, logos, product and service names, designs, part numbers, and slogans are trademarks of the Company or its affiliates 
                   or licensors. You must not use such marks without the prior written permission of the Company. All other names, logos, product and service names, designs and slogans on this 
                   Website are the trademarks of their respective owners and are used by permission. Nothing contained on this Website should be construed as granting, by implication, estoppel, 
                   or otherwise, any license or right to use any registered or unregistered Trademark displayed on the site without the written permission of Haeger or the third party owner of the 
                   Trademark. Any misuse of the Trademarks displayed on the site is strictly prohibited.
               </p>

                <h3><b>Prohibited Uses</b></h3>
               <p>
                   You may use the Website only for lawful purposes and in accordance with these Terms of Use. You agree not to use the Website:
                   <ul>
                       <li>In any way that violates any applicable federal, state, local or international law or regulation (including, without limitation, any laws regarding the export of data or 
                           software to and from the US or other countries).</li>
                       <li>For the purpose of exploiting, harming or attempting to exploit or harm minors in any way by exposing them to inappropriate content, asking for personally identifiable 
                           information or otherwise.</li>
                       <li>To transmit, or procure the sending of, any advertising or promotional material including any “junk mail”, “chain letter” or “spam” or any other similar solicitation.</li>
                       <li>To impersonate or attempt to impersonate the Company, a Company employee, another user or any other person or entity (including, without limitation, by using e-mail 
                           addresses associated with any of the foregoing).</li>
                       <li>To engage in any other conduct that restricts or inhibits anyone’s use or enjoyment of the Website, or which, as determined by us, may harm the Company or users of the 
                           Website or expose them to liability.</li>
                       
                   </ul>
               </p>
               <p>
                   Additionally, you agree <b><i>NOT</i></b> to:
                   <ul>
                       <li>Use the Website in any manner that could disable, overburden, damage, or impair the site or interfere with any other party’s use of the Website, including their ability to 
                           engage in real time activities through the Website.</li>
                       <li>Use any robot, spider or other automatic device, process or means to access the Website for any purpose, including monitoring or copying any of the material on the Website.</li>
                       <li>Use any manual process to monitor or copy any of the material on the Website or for any other unauthorized purpose without our prior written consent.</li>
                       <li>Use any device, software or routine that interferes with the proper working of the Website.</li>
                       <li>Introduce any viruses, Trojan horses, worms, logic bombs or other material which is malicious or technologically harmful.</li>
                       <li>Attempt to gain unauthorized access to, interfere with, damage or disrupt any parts of the Website, the server on which the Website is stored, or any server, computer or 
                           database connected to the Website.</li>
                       <li>Attack the Website via a denial-of-service attack or a distributed denial-of-service attack.</li>
                       <li>Otherwise attempt to interfere with the proper working of the Website.</li>
                   </ul>
               </p>

                <h3><b>Monitoring and Enforcement; Termination</b></h3>
                <p>
                    We have the right to:
                    <ul>
                        <li>Take appropriate legal action, including without limitation, referral to law enforcement, for any illegal or unauthorized use of the Website.</li>
                        <li>Terminate or suspend your access to all or part of the Website for [any or no reason, including without limitation,] any violation of these Terms of Use.</li>
                    </ul>
                </p>
               <p>
                   Without limiting the foregoing, we have the right to fully cooperate with any law enforcement authorities or court order requesting or directing us to disclose the identity or 
                   other information of anyone posting any materials on or through the Website. <b>YOU WAIVE AND HOLD HARMLESS THE COMPANY AND ITS AFFILIATES, LICENSEES AND SERVICE PROVIDERS FROM ANY 
                   CLAIMS RESULTING FROM ANY ACTION TAKEN BY THE COMPANY/ANY OF THE FOREGOING PARTIES DURING OR AS A RESULT OF ITS INVESTIGATIONS AND FROM ANY ACTIONS TAKEN AS A CONSEQUENCE OF 
                   INVESTIGATIONS BY EITHER THE COMPANY/SUCH PARTIES OR LAW ENFORCEMENT AUTHORITIES.</b>
               </p>
               <p>
                   We assume no liability for any action or inaction regarding transmissions, communications or content provided by any user or third party. We have no liability or responsibility 
                   to anyone for performance or nonperformance of the activities described in this section.
               </p>

                <h3><b>Reliance on Information Posted</b></h3>
               <p>
                   The information presented on or through the Website is made available solely for general information purposes. We do not warrant the accuracy, completeness or usefulness of this 
                   information. Any reliance you place on such information is strictly at your own risk. We disclaim all liability and responsibility arising from any reliance placed on such materials 
                   by you or any other visitor to the Website, or by anyone who may be informed of any of its contents.
               </p>

                <h3><b>Changes to the Website</b></h3>
               <p>
                   We may update the content on this Website from time to time, but its content is not necessarily complete or up-to-date. Any of the material on the Website may be out of date at any 
                   given time, and we are under no obligation to update such material.
               </p>

                <h3><b>Information about You and Your Visits to the Website</b></h3>
               <p>
                   All information we collect on this Website is subject to our <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="/PrivacyPolicy" Target="_blank">Privacy Policy</asp:HyperLink>. By using the Website, you consent to all actions taken by us with respect to your information in 
                   compliance with the <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="/PrivacyPolicy" Target="_blank">Privacy Policy</asp:HyperLink>.
               </p>

               <h3><b>Translation Services</b></h3>
               <p>
                   Translations on the Haeger website are prepared by third party translators and/or through third party providers, such as Google Translate. While reasonable efforts are made to provide 
                   accurate translations, portions may be incorrect. Some files, and other items cannot be translated including but not limited to download guides and technical articles, graphic features, 
                   and photos. In addition, some applications and/or services may not work as expected when translated due to language restrictions. No liability is assumed by Haeger for any errors, 
                   omissions, or ambiguities in the translations provided on this website. Any person or entity that relies on translated content does so at their own risk. Haeger shall not be liable 
                   for any losses caused by reliance on the accuracy or reliability of translated information. If you would like to report a translation error or inaccuracy, we encourage you to please 
                   contact us at <a href="mailto:webadmin@haeger.com">webadmin@haeger.com</a>.

               </p>

                <h3><b>Links to Other Websites and Content</b></h3>
               <p>
                   In the event this Website contains “hot links” permitting access to third party (non-Haeger) websites, you are advised that Haeger is not responsible for the content of such sites. 
                   If you use third party websites, you assume the risk of using them and Haeger assumes no liability with respect to such use. Links from our site to websites maintained by third parties 
                   do not constitute an endorsement by us of such third party resources or their contents. Links also do not mean that we sponsor, are affiliated or associated with, or otherwise recommend, 
                   certify or endorse the third party or the third party site, or that any such site is authorized to use any trademark, trade name or logo of Haeger. You should direct any concerns 
                   regarding any external link to its site administrator or webmaster. In the event that you use a third party website, you assume the risk of using such site and Haeger assumes no 
                   liability with respect to such use.
               </p>


                  <h3><b>Disclaimer of Warranties</b></h3>
               <p>
                   You understand that we cannot and do not guarantee or warrant that files available for downloading from the internet or the Website will be free of viruses or other destructive code. 
                   You are responsible for implementing sufficient procedures and checkpoints to satisfy your particular requirements for anti-virus protection and accuracy of data input and output, 
                   and for maintaining a means external to our site for any reconstruction of any lost data. <b>WE WILL NOT BE LIABLE FOR ANY LOSS OR DAMAGE CAUSED BY A DISTRIBUTED DENIAL-OF-SERVICE ATTACK, 
                   VIRUSES OR OTHER TECHNOLOGICALLY HARMFUL MATERIAL THAT MAY INFECT YOUR COMPUTER EQUIPMENT, COMPUTER PROGRAMS, DATA OR OTHER PROPRIETARY MATERIAL DUE TO YOUR USE OF THE WEBSITE OR ANY 
                   SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE OR TO YOUR DOWNLOADING OF ANY MATERIAL POSTED ON IT, OR ON ANY WEBSITE LINKED TO IT.</b>
               </p>
               <p>
                   <b>YOUR USE OF THE WEBSITE, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE IS AT YOUR OWN RISK. THE WEBSITE, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED 
                       THROUGH THE WEBSITE ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS, WITHOUT ANY WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. NEITHER THE COMPANY NOR ANY PERSON 
                       ASSOCIATED WITH THE COMPANY MAKES ANY WARRANTY OR REPRESENTATION WITH RESPECT TO THE COMPLETENESS, SECURITY, RELIABILITY, QUALITY, ACCURACY OR AVAILABILITY OF THE WEBSITE. 
                       WITHOUT LIMITING THE FOREGOING, NEITHER THE COMPANY NOR ANYONE ASSOCIATED WITH THE COMPANY REPRESENTS OR WARRANTS THAT THE WEBSITE, ITS CONTENT OR ANY SERVICES OR ITEMS OBTAINED 
                       THROUGH THE WEBSITE WILL BE ACCURATE, RELIABLE, ERROR-FREE OR UNINTERRUPTED, THAT DEFECTS WILL BE CORRECTED, THAT OUR SITE OR THE SERVER THAT MAKES IT AVAILABLE ARE FREE OF 
                       VIRUSES OR OTHER HARMFUL COMPONENTS OR THAT THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE WILL OTHERWISE MEET YOUR NEEDS OR EXPECTATIONS.</b>
               </p>
               <p>
                   <b>THE COMPANY HEREBY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, 
                       NON-INFRINGEMENT AND FITNESS FOR PARTICULAR PURPOSE.</b>
               </p>
               <p>
                   <b>THE FOREGOING DOES NOT AFFECT ANY WARRANTIES WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.</b>
               </p>

               <h3><b>Limitation on Liability</b></h3>
               <p>
                   <b>IN NO EVENT WILL THE COMPANY, ITS AFFILIATES OR THEIR LICENSORS, SERVICE PROVIDERS, EMPLOYEES, AGENTS, OFFICERS OR DIRECTORS BE LIABLE FOR DAMAGES OF ANY KIND, UNDER ANY LEGAL THEORY, 
                       ARISING OUT OF OR IN CONNECTION WITH YOUR USE, OR INABILITY TO USE, THE WEBSITE, ANY WEBSITES LINKED TO IT, ANY CONTENT ON THE WEBSITE OR SUCH OTHER WEBSITES OR ANY SERVICES OR 
                       ITEMS OBTAINED THROUGH THE WEBSITE OR SUCH OTHER WEBSITES, INCLUDING ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING BUT NOT LIMITED TO, 
                       PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL DISTRESS, LOSS OF REVENUE, LOSS OF PROFITS, LOSS OF BUSINESS OR ANTICIPATED SAVINGS, LOSS OF USE, LOSS OF GOODWILL, LOSS OF DATA, 
                       AND WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT OR OTHERWISE, EVEN IF FORESEEABLE.</b>
               </p>
               <p>
                   <b>THE FOREGOING DOES NOT AFFECT ANY LIABILITY WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.</b>
               </p>

               <h3><b>Governing Law and Jurisdiction</b></h3>
               <p>
                   All matters relating to the Website and these Terms of Use and any dispute or claim arising therefrom or related thereto (in each case, including non-contractual disputes or claims), 
                   shall be governed by and construed in accordance with the internal laws of the Commonwealth of Pennsylvania without giving effect to any choice or conflict of law provision or rule 
                   (whether of the Commonwealth of Pennsylvania or any other jurisdiction).
               </p>
               <p>
                   Any legal suit, action or proceeding arising out of, or related to, these Terms of Use or the Website shall be instituted exclusively in the federal courts of the United States or the 
                   courts of the Commonwealth of Pennsylvania. You waive any and all objections to the exercise of jurisdiction over you by such courts and to venue in such courts.
               </p>

               <h3><b>Arbitration</b></h3>
               <p>
                   At our sole discretion, it may require You to submit any disputes arising from the use of these Terms of Use or the Website, including disputes arising from or concerning their 
                   interpretation, violation, invalidity, non-performance, or termination, to final and binding arbitration under the Rules of Arbitration of the American Arbitration Association 
                   applying Pennsylvania law.
               </p>

               <h3><b>Waiver and Severability</b></h3>
               <p>
                   No waiver of by the Company of any term or condition set forth in these Terms of Use shall be deemed a further or continuing waiver of such term or condition or a waiver of any other 
                   term or condition, and any failure of the Company to assert a right or provision under these Terms of Use shall not constitute a waiver of such right or provision.
               </p>
               <p>
                   If any provision of these Terms of Use is held by a court or other tribunal of competent jurisdiction to be invalid, illegal or unenforceable for any reason, such provision shall be 
                   eliminated or limited to the minimum extent such that the remaining provisions of the Terms of Use will continue in full force and effect.
               </p>

                <h3><b>Entire Agreement</b></h3>
               <p>
                   The Terms of Use and our Privacy Policy constitute the sole and entire agreement between you and Haeger with respect to the Website and supersede all prior and contemporaneous 
                   understandings, agreements, representations and warranties, both written and oral, with respect to the Website.
               </p>

               <h3><b>Your Comments and Concerns</b></h3>
               <p>
                   This website is operated by Haeger. All other feedback, comments, requests for technical support and other communications relating to the Website should be directed 
                   to: <a href="mailto:webadmin@haeger.com">webadmin@haeger.com</a> or 1-800-878-4343.


               </p>

           </div>
        </div>
    </div>

</asp:Content>
