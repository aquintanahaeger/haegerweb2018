﻿<%@ Page Title="Force Chart" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FCW.aspx.cs" Inherits="Haeger2018.FCW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .ListControl
        {
            display:flex;
            align-items:center;
            justify-content:center;
            margin-left:auto;
            margin-right:auto;

        }
        .ListControl input[type=checkbox], input[type=radio]
        {
            background-color:none;
            display:inline-grid;
            align-items:center;
            width:auto;
            border: 15px solid red;
            margin-left:auto;
            margin-right:auto;
        }
        .ListControl label
        {
            
            width:auto;
            margin-left:10px;
            margin-right:10px;
            padding-left:20px;
            padding-right:10px;
            color:black;
        }
        

        .ValidateControl {
            color:black;
        }

        .machineImage
        {
            display:flex;
            align-items:center;
            justify-content:center;
            width:100%;
            height:100%;
        }

        .resultTop
        {
            color: black;
            font-size: small;

        }

           /* WIZARD */
        .stepNotCompleted
        {
            background-color: rgb(153,153,153);
            width: 15px;
            border: 1px solid rgb(153,153,153);
            margin-right: 5px;
            color: black;
            font-family: Arial;
            font-size: 12px;
            text-align: center;
        }

        .stepCompleted
        {
            background-color: #4d4d4d;
            width: 15px;
            border: 1px solid #4d4d4d;
            color: black;
            font-family: Arial;
            font-size: 12px;
            text-align: center;
        }

        .stepCurrent
        {
            background-color: #e01122;
            width: 15px;
            border: 1px solid #e01122;
            color: Black;
            font-family: Arial;
            font-size: 12px;
            font-weight: normal;
            text-align: center;
        }

        .stepBreak
        {
            width: 3px;
            background-color: Transparent;
        }

        .wizardProgress
        {
            padding-right: 10px;
            font-family: Arial;
            color: #333333;
            font-size: 12px;

        }

        .wizardTitle {
            font-family: Arial;
            font-size: 120%;
            font-weight: normal;
            color: black;
            vertical-align: middle;
            padding-right: 10px;
        }
        span1{
            color:#0085ca;
        }
    </style>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
         <div class="section-title">
                    <h2 style="margin: 0;">Haeger Force Chart Wizard</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                   
               </div>
            <br />
           
            <br />
            <div class="row">
                <div class="col-sm-3 col-sm-offset-2">
                    <asp:Label ID="lblFastener" CssClass="resultTop" runat="server" Text="Machine Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblMaterial" CssClass="resultTop" runat="server" Text="Set Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblThreadType" CssClass="resultTop" runat="server" Text="Thread Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblDashLength" CssClass="resultTop" runat="server" Text="Dash Length" Visible="false"></asp:Label>
                </div>

                 <%--Force Chart Wizard--%>
       
                <div class="col-sm-6">
                    <asp:Wizard ID="FCWizard" CssClass="ListControl" runat="server" Height="" Width="600px" DisplaySideBar="false" 
                                OnFinishButtonClick="FCWizard_FinishButtonClick" OnNextButtonClick="FCWizard_NextButtonClick" 
                                ActiveStepIndex="0" FinishCompleteButtonStyle-CssClass="hidden" StartNextButtonStyle-CssClass="hidden" StepNextButtonStyle-CssClass="hidden">
                        <HeaderTemplate>
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="wizardTitle">
                                        <%= FCWizard.ActiveStep.Title%>
                                    </td>
                                    
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <SideBarTemplate>
                        </SideBarTemplate>
           
                        <StartNavigationTemplate>
                            <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" Text="Next" Visible="false" />
                        </StartNavigationTemplate>

                         <WizardSteps>

                        <%--Choose a Fastener--%>
                            <asp:WizardStep ID="WizardStep1" runat="server" Title="Choose a Fastener" StepType="Start">
                                <asp:RadioButtonList ID="rblFCWFastener" runat="server" AutoPostBack="true"
                                            DataSourceID="sqlFCWFastener" DataTextField="ftype" DataValueField="ftype" 
                                            RepeatColumns="3" OnSelectedIndexChanged="rblFCWFastener_SelectedIndexChanged">

                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvFCWFastener" CssClass="ValidateControl" runat="server" 
                                                    ErrorMessage="Please Select a Fasteners" ControlToValidate="rblFCWFastener">

                                </asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="sqlFCWFastener" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT DISTINCT [ftype] FROM [fcForceData]">
                                   
                                </asp:SqlDataSource>
                            </asp:WizardStep>

                    <%--Choose a Material--%>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="Choose a Material">
                        <asp:RadioButtonList ID="rblFCWMaterial" runat="server" RepeatColumns="4" AutoPostBack="true"
                                            DataSourceID="sqlFCWMaterial" DataTextField="material" DataValueField="material" 
                                            OnSelectedIndexChanged="rblFCWMaterial_SelectedIndexChanged">

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvFCWMaterial" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Material" ControlToValidate="rblFCWMaterial">

                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="sqlFCWMaterial" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT DISTINCT [material] FROM [fcForceData] WHERE ([ftype] = @ftype)">
                            <SelectParameters>
                                <asp:SessionParameter Name="ftype" SessionField="fcwFastenerSelected" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </asp:WizardStep>

                             <%--Choose a Thread Type--%>
                    <asp:WizardStep ID="WizardStep3" runat="server" Title="Choose a Thread Type">
                        <asp:RadioButtonList ID="rblFCWThreadType" runat="server" AutoPostBack="true"
                                                DataSourceID="sqlFCWThreadType" DataTextField="fsize" DataValueField="fsize" 
                                                RepeatColumns="5" OnSelectedIndexChanged="rblFCWThreadType_SelectedIndexChanged" >

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvFCWThreadType" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Thread Type" ControlToValidate="rblFCWThreadType">

                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="sqlFCWThreadType" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                            SelectCommand="SELECT DISTINCT [fsize] FROM [fcForceData] WHERE (([material] = @material) AND ([ftype] = @ftype))">

                            <SelectParameters>
                                <asp:SessionParameter Name="material" SessionField="fcwMaterialSelected" Type="String" />
                                <asp:SessionParameter Name="ftype" SessionField="fcwFastenerSelected" Type="String" />
                            </SelectParameters>

                        </asp:SqlDataSource>
                        

                    </asp:WizardStep>
                    
                    <%--Choose a Dash Length--%>
                    <asp:WizardStep ID="WizardStep4" runat="server" Title="Choose a Dash Length">
                         <asp:RadioButtonList ID="rblFCWDashLength" runat="server" AutoPostBack="true"
                             DataSourceID="sqlFCWDashLength" DataTextField="dashLength" DataValueField="dashLength" 
                             RepeatDirection="Horizontal" RepeatColumns="5" 
                             OnSelectedIndexChanged="rblFCWDashLength_SelectedIndexChanged">

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvDashLength" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Dash Length" ControlToValidate="rblFCWDashLength">

                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="sqlFCWDashLength" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT DISTINCT [dashLength] FROM [fcForceData] 
                                                            WHERE (([ftype] = @ftype) 
                                                            AND ([material] = @material) 
                                                            AND ([fsize] = @fsize))">

                            <SelectParameters>
                                <asp:SessionParameter Name="ftype" SessionField="fcwFastenerSelected" Type="String" />
                                <asp:SessionParameter Name="material" SessionField="fcwMaterialSelected" Type="String" />
                                <asp:SessionParameter Name="fsize" SessionField="fcwThreadTypeSelected" Type="String" />
                            </SelectParameters>

                        </asp:SqlDataSource>
                    </asp:WizardStep>

                    <%--Results--%>
                    <asp:WizardStep ID="WizardStep5" runat="server" StepType="Complete" Title="Results">
                       
                        <asp:GridView ID="grdFCWResults" CssClass="table table-striped resultTop" runat="server" 
                                        DataSourceID="sqlFCWResults" 
                                      AutoGenerateColumns="false" Width="100%" text-align="center">
                            <Columns >
                                <asp:BoundField DataField="ftype" HeaderText="Fastener" SortExpression="ftype" />
                                <asp:BoundField DataField="material" HeaderText="Material" SortExpression="material" />
                                <asp:BoundField DataField="fsize" HeaderText="Thread Size" SortExpression="fsize" />
                                <asp:BoundField DataField="dashLength" HeaderText="Dash Length" SortExpression="dashLength" />
                                <asp:BoundField DataField="forceLB" HeaderText="Force (lb)" SortExpression="forceLB" />
                                <asp:BoundField DataField="forceKN" HeaderText="Force (kN)" SortExpression="forceKN" />
                                <asp:BoundField DataField="pushOutLB" HeaderText="Push Out (lb)" SortExpression="pushOutLB" />
                                <asp:BoundField DataField="pushOutKN" HeaderText="Push Out (kN)" SortExpression="pushOutKN" />
                                <asp:BoundField DataField="torqueOutLB" HeaderText="Torque Out (lb)" SortExpression="torqueOutLB" />
                                <asp:BoundField DataField="torqueOutNM" HeaderText="Torque Out (Nm)" SortExpression="torqueOutNM" />

                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlFCWResults" runat="server"
                                           ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>"
                                           SelectCommand="SELECT TOP 1 * FROM [fcForceData] 
                                                            WHERE (([ftype] = @ftype) 
                                                            AND ([material] = @material) 
                                                            AND ([fsize] = @fsize) 
                                                            AND ([dashLength] = @dashLength))">

                           <SelectParameters>
                               <asp:SessionParameter Name="ftype" SessionField="fcwFastenerSelected" Type="String" />
                               <asp:SessionParameter Name="material" SessionField="fcwMaterialSelected" Type="String" />
                               <asp:SessionParameter Name="fsize" SessionField="fcwThreadTypeSelected" Type="String" />
                               <asp:SessionParameter Name="dashLength" SessionField="fcwDashLengthSelected" Type="String" />
                           </SelectParameters>

                        </asp:SqlDataSource>

                        
                        <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                    </asp:WizardStep>








                </WizardSteps>
            </asp:Wizard>
            </div>
        </div>
    </div>


</asp:Content>
