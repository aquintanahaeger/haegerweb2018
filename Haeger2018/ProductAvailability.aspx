﻿<%@ Page Title="Product Search" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductAvailability.aspx.cs" Inherits="Haeger2018.ProductAvailability" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1{
            color:#0085ca;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- Container - Parts Availability -->
    <div class="section-title">
                    <h2 style="margin: 0;">Haeger Part and Tooling LookUp </h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                   
               </div>
            <br />
    <div class="container-fluid center-block">
        <div class="col-md-12 text-center">
            
        
            <p class="lead text-center">Please enter <b>Part Number</b> or <b>Description</b> and click Enter. If no information is shown, the part number does not exist</p>
            <p class="text-center" style="color:red"><b>NOTE:</b> For <b>North American</b> lead times, please contact Inside Sales by phone, <b>1-(800)-878-4343 ext. 5115</b>, OR by email, <b><a href="mailto:sales@haeger.com">sales@haeger.com</a></b></p>
            <p class="text-center" style="color:red"><b>NOTE:</b> For <b>European</b> lead times, please contact Inside Sales by phone, <b>+31 541 530 230</b> , OR by email, <b><a href="Europesales@haeger.com">Europesales@haeger.com</a></b></p> 
            <p class="text-center" style="color:#0085ca"><b>* Quantity on hand shown for North America ONLY!</b></p>
            <br />
            <br />    
            <h4>Enter Part Number:</h4>
            <asp:TextBox ID="txtProductID" CssClass="form-control input-lg center-block" Width="250px" TabIndex="1" runat="server"></asp:TextBox>
            <asp:Button ID="btnProductSearch" CssClass="btn btn-large" runat="server" Text="Search" />
            <br />
            <br />
            <asp:GridView ID="grdDisplayProduct" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataProductID" HeaderStyle-HorizontalAlign="Center">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <Columns>
                    <asp:BoundField DataField="ITEMID" HeaderText="Item ID" SortExpression="ITEMID"/>
                    <asp:BoundField DataField="ITEMNAME" HeaderText="Item Name" SortExpression="ITEMNAME" />
                   <%-- <asp:BoundField DataField="price" HeaderText="Price" SortExpression="price" DataFormatString="{0:c}" />--%>
                    <asp:BoundField DataField="qty_onhand" HeaderText="* Quantity on Hand" SortExpression="qty_onhand" DataFormatString="{0:f3}" />
                </Columns>


            </asp:GridView>
           
            <asp:SqlDataSource ID="SqlDataProductID" runat="server" 
                ConnectionString="<%$ ConnectionStrings:HaegerSalesConnectionString %>" 
                SelectCommand="SELECT [ITEMID], [ITEMNAME], [price], [qty_onhand], [leadtime] FROM [haegerwebitems] WHERE (([ITEMID] LIKE '%' + @ITEMID + '%') AND ([packaginggroupid] = @packaginggroupid))">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtProductID" Name="ITEMID" PropertyName="Text" Type="String" />
                    <asp:Parameter DefaultValue="Show" Name="packaginggroupid" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
            <br />
            <br />
            </div>
    </div>

    <div class="container-fluid">
        <div class="col-md-12 text-center">
        <h4>Enter Description:</h4>
            <asp:TextBox ID="txtProductDesc" CssClass="form-control input-lg center-block" Width="250px" TabIndex="1" runat="server"></asp:TextBox>
            <asp:Button ID="btnProductDesc" CssClass="btn btn-large" runat="server" Text="Search" />
            <br />
            <br />
            <asp:GridView ID="grdDescription" CssClass="table table-striped table-bordered table-hover" HeaderStyle-HorizontalAlign="Center" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataProdDesc">
                <Columns>
                    <asp:BoundField DataField="ITEMID" HeaderText="Item ID" SortExpression="ITEMID"  />
                    <asp:BoundField DataField="ITEMNAME" HeaderText="Item Name" SortExpression="ITEMNAME" />
                  <%-- <asp:BoundField DataField="price" HeaderText="Price" SortExpression="price" DataFormatString="{0:c}" />--%> 
                    <asp:BoundField DataField="qty_onhand" HeaderText="Quantity on Hand" SortExpression="qty_onhand" DataFormatString="{0:F3}" />
                </Columns>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataProdDesc" runat="server" ConnectionString="<%$ ConnectionStrings:HaegerSalesConnectionString %>" SelectCommand="SELECT [ITEMID], [ITEMNAME], [price], [qty_onhand], [leadtime] FROM [haegerwebitems] WHERE (([ITEMNAME] LIKE '%' + @ITEMNAME + '%') AND ([packaginggroupid] = @packaginggroupid))">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtProductDesc" Name="ITEMNAME" PropertyName="Text" Type="String" />
                    <asp:Parameter DefaultValue="Show" Name="packaginggroupid" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
    </div>
        </div>
</asp:Content>
