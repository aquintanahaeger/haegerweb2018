﻿<%@ Page Title="Dist Admin AS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DistAdminASPage.aspx.cs" Inherits="Haeger2018.DistAdminAS.DistAdminASPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }

    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Asian Distributor Administration Page</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <div class="row">
            <div class="col-xs-12">
                 <br>
              <!-- Nav pills -->
              <ul class="nav nav-pills" role="tablist" id="myTabs">
                <li id="New" class="nav-item">
                  <a class="nav-link" data-toggle="pill" href="#NewDist">New Distributor </a>
                </li>
           
                <li id="All" class="nav-item">
                  <a class="nav-link " data-toggle="pill" href="#AllDist">All Distributors</a>
                </li>
            
                <%--<li class="nav-item">
                  <a class="nav-link " data-toggle="pill" href="#menu1">Future Menu</a>
                </li>--%>
              </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" id="NewDist" class="container tab-pane active"><br>
                        <div class="row">
                            <div class="col-sm-4">
                                <h3>Distributors to be Approved:</h3>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="row">
            <div class="col-sm-12">
                <asp:GridView ID="grdDistAdminEU"  CssClass= "table table-striped table-bordered table-condensed" AutoGenerateEditButton="True" 
                    runat="server" AutoGenerateColumns="False" EmptyDataText="Currently No Distributors to Approve" DataKeyNames="Id" DataSourceID="sqlDistAdminEU" AllowSorting="False">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" Visible="false" />
                        <asp:BoundField DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName" />
                        <asp:BoundField DataField="ContactName" HeaderText="Contact Name" SortExpression="ContactName" />
                        <asp:BoundField DataField="ContactPhone" HeaderText="Contact Phone" SortExpression="ContactPhone" />
                        <asp:CheckBoxField DataField="LockoutEnabled" HeaderText="Approved" SortExpression="LockoutEnabled"></asp:CheckBoxField>
                        <asp:CheckBoxField DataField="EmailConfirmed" HeaderText="Email Confirmed" SortExpression="EmailConfirmed" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlDistAdminEU" runat="server" ConflictDetection="CompareAllValues" 
                    ConnectionString="<%$ ConnectionStrings:aspnet-Haeger2018-MembershipConnectionString-dist-admin-EU %>" 
                    DeleteCommand="DELETE FROM [AspNetUsers] WHERE [Id] = @original_Id AND (([CompanyName] = @original_CompanyName) OR ([CompanyName] IS NULL AND @original_CompanyName IS NULL)) AND (([ContactName] = @original_ContactName) OR ([ContactName] IS NULL AND @original_ContactName IS NULL)) AND (([ContactPhone] = @original_ContactPhone) OR ([ContactPhone] IS NULL AND @original_ContactPhone IS NULL)) AND [LockoutEnabled] = @original_LockoutEnabled AND [EmailConfirmed] = @original_EmailConfirmed" 
                    
                    InsertCommand="INSERT INTO [AspNetUsers] ([Id], [CompanyName], [ContactName], [ContactPhone], [LockoutEnabled], [EmailConfirmed]) VALUES (@Id, @CompanyName, @ContactName, @ContactPhone, @LockoutEnabled, @EmailConfirmed)" 
                    OldValuesParameterFormatString="original_{0}" 
                    
                    SelectCommand="SELECT AspNetUsers.Id, CompanyName, ContactName, ContactPhone, LockoutEnabled,EmailConfirmed
                                    FROM AspNetUsers
                                    LEFT JOIN AspNetUserRoles ON AspNetUsers.Id = UserId
                                    LEFT JOIN AspNetRoles ON RoleId = AspNetRoles.Id
                                    WHERE AspNetRoles.Name = 'DistributorAS'
                                    AND LockoutEnabled = 'False'"                    
                    UpdateCommand="UPDATE [AspNetUsers] SET [CompanyName] = @CompanyName, [ContactName] = @ContactName, [ContactPhone] = @ContactPhone, [LockoutEnabled] = @LockoutEnabled, [EmailConfirmed] = @EmailConfirmed WHERE [Id] = @original_Id AND (([CompanyName] = @original_CompanyName) OR ([CompanyName] IS NULL AND @original_CompanyName IS NULL)) AND (([ContactName] = @original_ContactName) OR ([ContactName] IS NULL AND @original_ContactName IS NULL)) AND (([ContactPhone] = @original_ContactPhone) OR ([ContactPhone] IS NULL AND @original_ContactPhone IS NULL)) AND [LockoutEnabled] = @original_LockoutEnabled AND [EmailConfirmed] = @original_EmailConfirmed">
                    <DeleteParameters>
                        <asp:Parameter Name="original_Id" Type="String" />
                        <asp:Parameter Name="original_CompanyName" Type="String" />
                        <asp:Parameter Name="original_ContactName" Type="String" />
                        <asp:Parameter Name="original_ContactPhone" Type="String" />
                        <asp:Parameter Name="original_LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="original_EmailConfirmed" Type="Boolean" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Id" Type="String" />
                        <asp:Parameter Name="CompanyName" Type="String" />
                        <asp:Parameter Name="ContactName" Type="String" />
                        <asp:Parameter Name="ContactPhone" Type="String" />
                        <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:Parameter DefaultValue="false" Name="LockoutEnabled" Type="Boolean" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="CompanyName" Type="String" />
                        <asp:Parameter Name="ContactName" Type="String" />
                        <asp:Parameter Name="ContactPhone" Type="String" />
                        <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                        <asp:Parameter Name="original_Id" Type="String" />
                        <asp:Parameter Name="original_CompanyName" Type="String" />
                        <asp:Parameter Name="original_ContactName" Type="String" />
                        <asp:Parameter Name="original_ContactPhone" Type="String" />
                        <asp:Parameter Name="original_LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="original_EmailConfirmed" Type="Boolean" />
                    </UpdateParameters>
                </asp:SqlDataSource>


            </div>

        </div>


                        </div>




                    </div> <%--End Tab home--%>

                    <div role="tabpanel" id="AllDist" class="container tab-pane"><br>
                        <h3>Distributor List:</h3>
                        <asp:GridView ID="grdAllDistEU" CssClass="table table-striped table-bordered table-condensed" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="sqlAllDistEU" OnSelectedIndexChanged="grdAllDistEU_SelectedIndexChanged" AllowSorting="True">
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" Visible="false" />
                                <asp:BoundField DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName" />
                                <asp:BoundField DataField="ContactName" HeaderText="Contact Name" SortExpression="ContactName" />
                                <asp:BoundField DataField="ContactPhone" HeaderText="Contact Phone" SortExpression="ContactPhone" />
                                <asp:BoundField DataField="LoginCount" HeaderText="Login Count" SortExpression="LoginCount" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="btnSelect" runat="server" Text="Detail" CommandName = "Select" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlAllDistEU" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:aspnet-Haeger2018-MembershipConnectionString-dist-admin-EU %>" 
                                SelectCommand="SELECT AspNetUsers.Id, CompanyName, ContactName, ContactPhone, LoginCount
                                    FROM AspNetUsers
                                    LEFT JOIN AspNetUserRoles ON AspNetUsers.Id = UserId
                                    LEFT JOIN AspNetRoles ON RoleId = AspNetRoles.Id
                                    WHERE AspNetRoles.Name = 'DistributorAS'">

                        </asp:SqlDataSource>

                        
                    </div> <%--End All Distributors--%>
                   

                </div> <%--End Tab Content--%>
                <asp:HiddenField ID="TabName" runat="server" />
</div>
            
</div>

        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />

            </div>


        </div>
     
    
    
    
    
    
    </div>

</asp:Content>
