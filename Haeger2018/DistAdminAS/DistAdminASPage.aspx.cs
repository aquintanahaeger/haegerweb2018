﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.DistAdminAS
{
    public partial class DistAdminASPage : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void grdAllDistEU_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["selectedItem"] = (String)grdAllDistEU.SelectedDataKey.Values["Id"];
            Response.Redirect("/DistAdminAS/DistDetailAS");
        }
    }
}