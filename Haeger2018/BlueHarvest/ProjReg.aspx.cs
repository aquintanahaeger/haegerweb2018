﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.BlueHarvest
{
    public partial class ProjReg : System.Web.UI.Page
    {
        SqlConnection projRegConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ProjRegConnectionString"].ConnectionString);
        DateTime localDate = DateTime.Now;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;
        }

        protected void btnProjRegSubmit_Click(object sender, EventArgs e)
        {
            var msg = "The consent box must be checked or we cannot process your request.";

            if (!gdprConsentBox.Checked)
                {
                lblConsentWrn.Visible = true;
                
                Response.Write("<script>alert('" + msg + "')</script>");

                return;
            }

            // *************** DATA FROM FORM ***************

            var compName = txtCompanyName.Text.Trim();
            var compAddress = txtCompanyAddress.Text.Trim();
            var compCity = txtCompanyCity.Text.Trim();

            var compState = "0";
            if (drpState.SelectedIndex != 0)
            {
                compState = drpState.SelectedItem.Text.Trim();

            }
            else
            {
                compState = "No State Selected";

            }

            var compZip = txtZip.Text.Trim();
            var phone = txtCompanyPhone.Text.Trim();
            var fax = txtCompanyFax.Text.Trim();
            var contactName = txtContactName.Text.Trim();
            var contactEmail = txtContactEmail.Text.Trim();
            var contactPosition = txtContactPosition.Text.Trim();
            var contactPhone = txtContactPhone.Text.Trim();


            var country = "0";
            if (drpCountry.SelectedIndex != 0)
            {
                country = drpCountry.SelectedItem.Text;
            }
            else
            {
                country = "No country selected";
            }

            var businessType = "0";
            if (drpBusType.SelectedIndex != 0)
            {
                businessType = drpBusType.SelectedItem.Text;
            }
            else
            {
                businessType = "No business type selected";
            }

            var industryType = "0";
            if (drpIndType.SelectedIndex != 0)
            {
                industryType = drpIndType.SelectedItem.Text;
            }
            else
            {
                industryType = "No industry type selected";
            }

            var salesPerson = txtSalesPerson.Text.Trim();

            var distributor = "0";
            if (drpDistNames.SelectedIndex != 0)
            {
                distributor = drpDistNames.SelectedItem.Text;
            }
            else
            {
                distributor = "No business type selected";
            }

            var priority = "0";
            if (drpPriority.SelectedIndex != 0)
            {
                priority = drpPriority.SelectedItem.Text;
            }
            else
            {
                priority = "No priority type selected";
            }

            var machType = "0";
            if (drpMachineType.SelectedIndex != 0)
            {
                machType = drpMachineType.SelectedItem.Text;
            }
            else
            {
                machType = "No business type selected";
            }

            var clinchPurchase = txtClinchPurch.Text.Trim();

            Decimal pemPercentage;
            if (drpEstPrcnt.SelectedIndex != 0)
            {
                pemPercentage = Convert.ToDecimal(drpEstPrcnt.SelectedItem.Value);
            }
            else
            {
                pemPercentage = 0;
            }

            var otherType = txtOtherClinch.Text.Trim();
           


            // *************** INSERT DATABASE ***************

            projRegConn.Open();

            // Query and commands to retrieve max tracking number
            string trackNumQuery = "SELECT MAX(trackNum) FROM ProjRegData";
            SqlCommand trackNumCommand = new SqlCommand(trackNumQuery, projRegConn);
            string trackNum = "";
            using (SqlDataReader tn = trackNumCommand.ExecuteReader())
            {
                while (tn.Read())
                {
                    trackNum = tn[0].ToString();
                }
            }

            // Add one to max tracking number and creating new _projID
            int nextIDnum = (Int32.Parse(trackNum)) + 1;
            string projectID = "PR" + nextIDnum.ToString();

            string insertQuery =
            "INSERT INTO [dbo].[ProjRegData] " +
            "(_projID, company, address, city, state, zipCode, country, phone, fax, contactName, " +
            "contactEmail, contactPosition, contactPhone, businessType, industryType, salesPerson, distributorName, priorityMonthToClose, " +
            "projectMachineType, clinchPurchases, percentFromPEM, typeOfOther, dateCreated, showNoShow, openClose, reasonClose, globalRegion, gdprConsent) " +
            "VALUES (@projID, @Company, @Address, @City, @State, @ZipCode, @Country, @Phone, @Fax, " +
            "@ContactName, @ContactEmail, @ContactPosition, @ContactPhone, @BusinessType, @Industry, @SalesPerson, " +
            "@Distributor, @PriorityMonthToClose, @ProjectMachineType, @ClinchPurchases, @PercentFromPEM, " +
            "@TypeOfOther, @DateCreated, @showNoShow, @openClose, @reasonClose, @globalRegion, @gdprConsent)";

            SqlCommand insertCommand = new SqlCommand(insertQuery, projRegConn);
            insertCommand.Parameters.AddWithValue("@projID", projectID);
            insertCommand.Parameters.AddWithValue("@Company", compName);
            insertCommand.Parameters.AddWithValue("@Address", compAddress);
            insertCommand.Parameters.AddWithValue("@City", compCity);
            insertCommand.Parameters.AddWithValue("@State", compState);
            insertCommand.Parameters.AddWithValue("@ZipCode", compZip);
            insertCommand.Parameters.AddWithValue("@Country", country);
            insertCommand.Parameters.AddWithValue("@Phone", phone);
            insertCommand.Parameters.AddWithValue("@Fax", fax);
            insertCommand.Parameters.AddWithValue("@ContactName", contactName);
            insertCommand.Parameters.AddWithValue("@ContactEmail", contactEmail);
            insertCommand.Parameters.AddWithValue("@ContactPosition", contactPosition);
            insertCommand.Parameters.AddWithValue("@ContactPhone", contactPhone);
            insertCommand.Parameters.AddWithValue("@BusinessType", businessType);
            insertCommand.Parameters.AddWithValue("@Industry", industryType);
            insertCommand.Parameters.AddWithValue("@SalesPerson", salesPerson);
            insertCommand.Parameters.AddWithValue("@Distributor", distributor);
            insertCommand.Parameters.AddWithValue("@PriorityMonthToClose", priority);
            insertCommand.Parameters.AddWithValue("@ProjectMachineType", machType);
            insertCommand.Parameters.AddWithValue("@ClinchPurchases", clinchPurchase);
            insertCommand.Parameters.AddWithValue("@PercentFromPEM", pemPercentage);
            insertCommand.Parameters.AddWithValue("@TypeOfOther", otherType);
            insertCommand.Parameters.AddWithValue("@showNoShow", "Show");
            insertCommand.Parameters.AddWithValue("@DateCreated", localDate);
            insertCommand.Parameters.AddWithValue("@openClose", "Open");
            insertCommand.Parameters.AddWithValue("@reasonClose", "None");
            insertCommand.Parameters.AddWithValue("@globalRegion", "NA");
            insertCommand.Parameters.AddWithValue("@gdprConsent", 1);

            insertCommand.ExecuteNonQuery();
            projRegConn.Close();

            // *************** EMAIL ***************

            var consentConfirm = "";

            if (gdprConsentBox.Checked)
            {
                consentConfirm = "Confirmed";
            }

            //Fetching Email Body Text from EmailTemplate File.
            string FilePath = @"D:/EmailTemplates/ProjReg.html";
            FileStream sourceStream = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamReader str = new StreamReader(sourceStream);
            string MailText = str.ReadToEnd();

            MailText = MailText.Replace("[currentDate]", localDate.ToString());
            MailText = MailText.Replace("[newCompName]", compName);
            MailText = MailText.Replace("[newAddress]", compAddress);
            MailText = MailText.Replace("[newCity]", compCity);
            MailText = MailText.Replace("[newState]", compState);
            MailText = MailText.Replace("[newZip]", compZip);
            MailText = MailText.Replace("[newCountry]", country);
            MailText = MailText.Replace("[newCompPhone]", phone);
            MailText = MailText.Replace("[newFax]", fax);
            MailText = MailText.Replace("[newContactName]", contactName);
            MailText = MailText.Replace("[newContactPhone]", contactPhone);
            MailText = MailText.Replace("[newContactEmail]", contactEmail);
            MailText = MailText.Replace("[newBusType]", businessType);
            MailText = MailText.Replace("[newIndType]", industryType);
            MailText = MailText.Replace("[newSalesPerson]", salesPerson);
            MailText = MailText.Replace("[newDist]", distributor);
            MailText = MailText.Replace("[newPriority]", priority);
            MailText = MailText.Replace("[newMachType]", machType);
            MailText = MailText.Replace("[newClinchPurch]", clinchPurchase);
            MailText = MailText.Replace("[newEstPercent]", pemPercentage.ToString());
            MailText = MailText.Replace("[newOtherType]", otherType);
            MailText = MailText.Replace("[newConsent]", consentConfirm);

            var subject = "Project Registration";

            // Session variable to pass email information to confirmation page.
            Session["emailConfirm"] = MailText;

            var customerEmail = contactEmail;

            try
            {
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;

                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");

                _mailmsg.From = address;

                //Add your email address to the recipients
                //_mailmsg.To.Add("ronboggs@haeger.com");
                //_mailmsg.To.Add("msplaine@haeger.com");
                //_mailmsg.To.Add("jgarcia@haeger.com");
                //_mailmsg.To.Add("jkimberlin@haeger.com");
                //_mailmsg.To.Add("kbeene@haeger.com");
                _mailmsg.CC.Add(customerEmail);
                _mailmsg.Bcc.Add("regeah811@gmail.com");
                //_mailmsg.Bcc.Add("webadmin@haeger.com");

                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                _mailmsg.Body = MailText;

                // Send the msg.
                _smtp.Send(_mailmsg);


                Response.Redirect("/SubmitSucess");

            }
            catch
            {
                // If the message failed at some point, let the user know.
                lblResult.Text = "Your message failed to send, please try again.";

            }


        }

        protected void drpCountry_SelectedIndexChanged(object sender, EventArgs e)
        {

            string countrySelected = drpCountry.SelectedItem.Text;

            if (countrySelected == "Canada")
            {
                lblState.Text = "Province/Territory:";

            }
            else
            {
                lblState.Text = "State:";
            }

            string stateQuery = "SELECT stateName FROM States WHERE country = '" + countrySelected + "' ORDER BY stateName ASC";
            SqlCommand stateList = new SqlCommand(stateQuery, projRegConn);
            projRegConn.Open();
            SqlDataReader stateSet = stateList.ExecuteReader();
            drpState.DataSource = stateSet;
            drpState.DataTextField = "stateName";
            drpState.DataValueField = "stateName";
            drpState.DataBind();
            stateSet.Close();
            projRegConn.Close();
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = gdprConsentBox.Checked;
        }
    }
}