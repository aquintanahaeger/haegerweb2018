﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Demo618.aspx.cs" Inherits="Haeger2018.BlueHarvest.Demo618" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" CssClass= "table table-striped table-bordered table-condensed" Width="400px" RowStyle-HorizontalAlign="Center">
                    <Columns>
                        <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer" />
                        <asp:BoundField DataField="UniqueMachineID" HeaderText="Unique Machine ID" SortExpression="UniqueMachineID" />
                    </Columns>
                    <RowStyle HorizontalAlign="Center" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:dAdBConnectionString %>" SelectCommand="SELECT DISTINCT [Customer], [UniqueMachineID] FROM [dATest] WHERE ([Customer] = @Customer)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Demo618" Name="Customer" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
             <div class="col-xs-4">
                 <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" CssClass= "table table-striped table-bordered table-condensed" Width="500px" RowStyle-HorizontalAlign="Center" AllowPaging="True" PageSize="1">
                     <Columns>
                         <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
                         <asp:BoundField DataField="TotalStrokeCount" HeaderText="Total Stroke Count" SortExpression="TotalStrokeCount" />
                     </Columns>
<RowStyle HorizontalAlign="Center"></RowStyle>
                 </asp:GridView>
                 <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:dAdBConnectionString %>" SelectCommand="SELECT DISTINCT [Date], [TotalStrokeCount] FROM [dATest] ORDER BY [Date] DESC"></asp:SqlDataSource>

            </div>
             <div class="col-xs-4">
                 <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource4" CssClass= "table table-striped table-bordered table-condensed" Width="500px" RowStyle-HorizontalAlign="Center" AllowPaging="True" PageSize="1">
                     <Columns>
                         <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
                         <asp:BoundField DataField="TotalHours" HeaderText="Total Hours" SortExpression="TotalHours" />
                     </Columns>
<RowStyle HorizontalAlign="Center"></RowStyle>
                 </asp:GridView>
                 <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:dAdBConnectionString %>" SelectCommand="SELECT DISTINCT [Date], [TotalHours] FROM [dATest] WHERE ([Customer] = @Customer) ORDER BY [Date] DESC">
                     <SelectParameters>
                         <asp:Parameter DefaultValue="Demo618" Name="Customer" Type="String" />
                     </SelectParameters>
                 </asp:SqlDataSource>

            </div>

        </div>
        <br />
        <br />
        <br />
        <br />
        <div class="row">
            <div class="col-xs-6">
                 <asp:Chart ID="Chart1" runat="server" DataSourceID="SqlDataSource1" Width="600px">
                     <Titles>
        <asp:Title Font="Times New Roman, 16pt, style=Bold, Italic" Name="Title2" 
            Text="Total Stroke Count">
        </asp:Title>
    </Titles>
        <Series>
            <asp:Series Name="Series1" XValueMember="Date" YValueMembers="TotalStrokeCount"></asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
        </ChartAreas>
    </asp:Chart>



    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:dAdBConnectionString %>" SelectCommand="SELECT DISTINCT [Date], [TotalStrokeCount] FROM [dATest] WHERE ([Customer] = @Customer) ORDER BY [Date]">
        <SelectParameters>
            <asp:Parameter DefaultValue="Demo618" Name="Customer" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>



            </div>
            
             <div class="col-xs-6">
                 <asp:Chart ID="Chart2" runat="server" DataSourceID="SqlDataSource5" Width="600px">
                      <Titles>
        <asp:Title Font="Times New Roman, 16pt, style=Bold, Italic" Name="Title1" 
            Text="Total Hours">
        </asp:Title>
    </Titles>
                     <Series>
                         <asp:Series Name="Series1" ChartType="Line" XValueMember="Date" YValueMembers="TotalHours"></asp:Series>
                     </Series>
                     <ChartAreas>
                         <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                 </asp:Chart>
                 <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:dAdBConnectionString %>" SelectCommand="SELECT DISTINCT [Date], [TotalHours] FROM [dATest] WHERE ([Customer] = @Customer)">
                     <SelectParameters>
                         <asp:Parameter DefaultValue="Demo618" Name="Customer" Type="String" />
                     </SelectParameters>
                 </asp:SqlDataSource>


            </div>


        </div>



    </div>



   


</asp:Content>
