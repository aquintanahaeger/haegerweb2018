﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.PartnerNA
{
    public partial class RequestTimeOff : System.Web.UI.Page
    {
        
        // Database connection used to store or retrieve time sheets, also to find Partner scheduled hours.
        SqlConnection haegerTimeDB = new SqlConnection(ConfigurationManager.ConnectionStrings["HaegerTimeConnectionString"].ConnectionString);

        // Current date
        DateTime localDate = DateTime.Now;

        // ******************** Test date - DELETE FOR PRODUCTION ********************
        //DateTime localDate = new DateTime(2025, 7, 16);

        // Basic info
        string OakdaleID;
        int TrackNum;
        string currentID;
        string fName;
        string lName;
        int weekNumReq;
        DateTime dateCreated;
        string teamLead;
        string showNoShow;
        string reqYear;
        string status;

        // Monday
        Decimal regHoursMon;
        string commentMon;

        // Tuesday
        Decimal regHoursTue;
        string commentTue;

        // Wednesday
        Decimal regHoursWed;
        string commentWed;

        // Thursday
        Decimal regHoursThu;
        string commentThu;

        // Friday
        Decimal regHoursFri;
        string commentFri;



        // Vacation Hours
        Decimal vacMon;
        Decimal vacTue;
        Decimal vacWed;
        Decimal vacThu;
        Decimal vacFri;
        Decimal vacSat = 0.00m;
        Decimal vacSun = 0.00m;

        // ARP Hours
        Decimal arpMon;
        Decimal arpTue;
        Decimal arpWed;
        Decimal arpThu;
        Decimal arpFri;
        Decimal arpSat = 0.00m;
        Decimal arpSun = 0.00m;

        // Holiday Hours
        Decimal holMon;
        Decimal holTue;
        Decimal holWed;
        Decimal holThu;
        Decimal holFri;
        Decimal holSat = 0.00m;
        Decimal holSun = 0.00m;

        // Bereavement Hours
        Decimal berMon;
        Decimal berTue;
        Decimal berWed;
        Decimal berThu;
        Decimal berFri;
        Decimal berSat = 0.00m;
        Decimal berSun = 0.00m;

        // Jury Duty Hours
        Decimal jurMon;
        Decimal jurTue;
        Decimal jurWed;
        Decimal jurThu;
        Decimal jurFri;
        Decimal jurSat = 0.00m;
        Decimal jurSun = 0.00m;

        // Define variables for Reg hours and OT
        // Regular Hours
        Decimal regMon;
        Decimal regTue;
        Decimal regWed;
        Decimal regThu;
        Decimal regFri;
        Decimal regSat = 0.00m;
        Decimal regSun = 0.00m;

        // OT Hours
        Decimal otMon = 0.00m;
        Decimal otTue = 0.00m;
        Decimal otWed = 0.00m;
        Decimal otThu = 0.00m;
        Decimal otFri = 0.00m;
        Decimal otSat = 0.00m;
        Decimal otSun = 0.00m;

        // Double OT Hours
        Decimal dotMon = 0.00m;
        Decimal dotTue = 0.00m;
        Decimal dotWed = 0.00m;
        Decimal dotThu = 0.00m;
        Decimal dotFri = 0.00m;
        Decimal dotSat = 0.00m;
        Decimal dotSun = 0.00m;

        string teamLeadEmail;


        protected void Page_Load(object sender, EventArgs e)
        {
            // Check the User Agent
            var agent = Request.UserAgent.ToLower();

            if (Request.Browser.IsMobileDevice)
            {
                Response.Redirect("/NoMobile");
            }


            // Identify logged in user.
            currentID = User.Identity.GetUserId();

            // Current cultural information.
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            // Current Week number to verify existing request.
            int weekNumCurrent = ciCurr.Calendar.GetWeekOfYear(localDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            haegerTimeDB.Open();

            // Retrieve first name from PartnerInfo if user is logged in.
            string currentContact = null;
            string contactNameQuery = "Select DISTINCT fName, lname, TeamLead FROM OakdalePartnerInfo WHERE UserID = '" + currentID + "'";
            SqlCommand currentContactCommand = new SqlCommand(contactNameQuery, haegerTimeDB);

            using (SqlDataReader tm = currentContactCommand.ExecuteReader())
            {
                while (tm.Read())
                {
                    currentContact = tm[0].ToString();
                    fName = tm[0].ToString();
                    lName = tm[1].ToString();
                    teamLead = tm[2].ToString();
                }
            }


            // In case no one is logged in.
            if (currentContact != null)
            {
                lblPartnerName.Text = currentContact;
            }
            else
            {
                Response.Redirect("/Default");
            }

            string teamLeadEmailQuery = "Select Email from OakdaleTeamLeadInfo WHERE TeamLead = '" + teamLead + "'";
            SqlCommand teamLeadEmailCommand = new SqlCommand(teamLeadEmailQuery, haegerTimeDB);
            using (SqlDataReader tl = teamLeadEmailCommand.ExecuteReader())
            {
                while (tl.Read())
                {
                    teamLeadEmail = tl[0].ToString();
                    
                }
            }

            // **************** Populate week range dropdown menu ****************
            // Find Monday of current week.
            //DateTime firstMondayCurrentWK = localDate.AddDays(-(int)localDate.DayOfWeek + 1);


            DateTime firstMondayCurrentWK = FirstDateOfWeekISO8601(localDate.Year, weekNumCurrent);

            // Find the Monday two weeks from now.
            DateTime firstDayRequestEligible = firstMondayCurrentWK.AddDays(14);


            // ********** LOOP **********
            // Loop will execute 52 times 
            for (int i = 0; i <= 52; i++)
            {
                int daysToAdd = 7 * i;
                DateTime monday = firstDayRequestEligible.AddDays(daysToAdd);
                // Find week number for a given Monday.
                int weekNum = ciCurr.Calendar.GetWeekOfYear(monday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                int year = monday.Year;

                switch (year)
                {

                    case 2020:
                        if (weekNum == 53)
                        {
                            weekNum = 53;
                        }

                        break;
                    default:
                        if (weekNum == 53)
                        {
                            weekNum = 1;
                        }

                        break;

                }

                // Find Sunday of that week - add 6 days to Monday.
                DateTime sunday = monday.AddDays(6);
                DateTime thursdayOfWeek = monday.AddDays(3);

                // Create date ranges.
                // Strings for date display.
                string displayWeek = weekNum.ToString();
                string displayMonday = monday.ToLongDateString();
                string displaySunday = sunday.ToLongDateString();
                string dateRange = "Week " + displayWeek + ": " + displayMonday + " to " + displaySunday;

                string dateValue = displayWeek.ToString() + thursdayOfWeek.Year;

                // Add dateRange to drop
                if (!IsPostBack)
                {
                    drpWeekRange.Items.Add(new ListItem(dateRange, dateValue));
                }
                // Start loop over.


            }

            haegerTimeDB.Close();



        }


        protected void btnCalc_Click(object sender, EventArgs e)
        {
            // Define MID5 textbox varaibles.
            // Vacation Hours
            vacMon = VerifyNull(txtHrVacMon.Text);
            vacTue = VerifyNull(txtHrVacTue.Text);
            vacWed = VerifyNull(txtHrVacWed.Text);
            vacThu = VerifyNull(txtHrVacThu.Text);
            vacFri = VerifyNull(txtHrVacFri.Text);

            // ARP Hours
            arpMon = VerifyNull(txtHrARPMon.Text);
            arpTue = VerifyNull(txtHrARPTue.Text);
            arpWed = VerifyNull(txtHrARPWed.Text);
            arpThu = VerifyNull(txtHrARPThu.Text);
            arpFri = VerifyNull(txtHrARPFri.Text);

            // Holiday Hours
            holMon = VerifyNull(txtHrHolidayMon.Text);
            holTue = VerifyNull(txtHrHolidayTue.Text);
            holWed = VerifyNull(txtHrHolidayWed.Text);
            holThu = VerifyNull(txtHrHolidayThu.Text);
            holFri = VerifyNull(txtHrHolidayFri.Text);

            // Bereavement Hours
            berMon = VerifyNull(txtHrBereaveMon.Text);
            berTue = VerifyNull(txtHrBereaveTue.Text);
            berWed = VerifyNull(txtHrBereaveWed.Text);
            berThu = VerifyNull(txtHrBereaveThu.Text);
            berFri = VerifyNull(txtHrBereaveFri.Text);

            // Jury Duty Hours
            jurMon = VerifyNull(txtHrJuryMon.Text);
            jurTue = VerifyNull(txtHrJuryTue.Text);
            jurWed = VerifyNull(txtHrJuryWed.Text);
            jurThu = VerifyNull(txtHrJuryThu.Text);
            jurFri = VerifyNull(txtHrJuryFri.Text);


            // Define variables for Reg hours and OT
            // Regular Hours
            regMon = VerifyNull(lblHrRegMon.Text);
            regTue = VerifyNull(lblHrRegTue.Text);
            regWed = VerifyNull(lblHrRegWed.Text);
            regThu = VerifyNull(lblHrRegThu.Text);
            regFri = VerifyNull(lblHrRegFri.Text);

            // MID5 Hours = Vacation + ARP + UnpaidLv + Bereave + Volunteer <= 8
            // Calculate MID5 hours
            Decimal mid5TotalMon = vacMon + arpMon + holMon + berMon + jurMon;
            Decimal mid5TotalTue = vacTue + arpTue + holTue + berTue + jurTue;
            Decimal mid5TotalWed = vacWed + arpWed + holWed + berWed + jurWed;
            Decimal mid5TotalThu = vacThu + arpThu + holThu + berThu + jurThu;
            Decimal mid5TotalFri = vacFri + arpFri + holFri + berFri + jurFri;
            //Decimal mid5TotalSat = vacSat + arpSat + unlSat + berSat + volSat;
            //Decimal mid5TotalSun = vacSun + arpSun + unlSun + berSun + volSun;

            // Array for days of the week
            string[] days = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

            // Array for MID5 totals
            Decimal[] mid5Totals = new Decimal[] { mid5TotalMon, mid5TotalTue, mid5TotalWed, mid5TotalThu, mid5TotalFri };

            // Array for regular hour label names.
            Label[] regHours = new Label[] { lblHrRegMon, lblHrRegTue, lblHrRegWed, lblHrRegThu, lblHrRegFri };

            bool exit = false;

            // Loop to iterate through regular hour labels to enter 8 minus MID5 total.
            for (int i = 0; i <= mid5Totals.Length - 1; i++)
            {
                if (mid5Totals[i] > 8)
                {
                    string msg = "The row for " + days[i] + " adds to more than 8 hours. Please make the correction";
                    Response.Write("<script>alert('" + msg + "')</script>");

                    exit = true;
                    break;
                }
                else
                {
                    regHours[i].Text = (8 - mid5Totals[i]).ToString("0.00");

                }


            }

            if (exit == true)
            {
                btnSubmit.Enabled = false;
                return;
            }


            // Define variables for Reg hours and OT
            // Regular Hours
            regMon = VerifyNull(lblHrRegMon.Text);
            regTue = VerifyNull(lblHrRegTue.Text);
            regWed = VerifyNull(lblHrRegWed.Text);
            regThu = VerifyNull(lblHrRegThu.Text);
            regFri = VerifyNull(lblHrRegFri.Text);
            //regSat = VerifyNull(lblHrRegSat.Text);
            //regSun = VerifyNull(lblHrRegSun.Text);
            // Volunteer Hours
            //otMon = VerifyNull(txtHROTMon.Text);
            //otTue = VerifyNull(txtHROTTue.Text);
            //otWed = VerifyNull(txtHROTWed.Text);
            //otThu = VerifyNull(txtHROTThu.Text);
            //otFri = VerifyNull(txtHROTFri.Text);
            //otSat = VerifyNull(txtHROTSat.Text);
            //otSun = VerifyNull(txtHROTSun.Text);


            // Array for column values to total rows.
            Decimal[] colTotalREG = new decimal[] { regMon, regTue, regWed, regThu, regFri };

            Decimal[] colTotalVAC = new decimal[] { vacMon, vacTue, vacWed, vacThu, vacFri };

            Decimal[] colTotalARP = new decimal[] { arpMon, arpTue, arpWed, arpThu, arpFri };

            Decimal[] colTotalUNL = new decimal[] { holMon, holTue, holWed, holThu, holFri };

            Decimal[] colTotalBER = new decimal[] { berMon, berTue, berWed, berThu, berFri };

            Decimal[] colTotalVOL = new decimal[] { jurMon, jurTue, jurWed, jurThu, jurFri };

            //Decimal[] colTotalOT = new decimal[] { otMon, otTue, otWed, otThu, otFri };

            Decimal[][] allColumns = new decimal[][] { colTotalREG, colTotalVAC, colTotalARP, colTotalUNL, colTotalBER, colTotalVOL };

            Label[] colLblTotals = new Label[] { lblTotalReg, lblTotalVac, lblTotalARP, lblTotalHoliday, lblTotalBereave, lblTotalJuryDuty };

            // Variable for columnTotal.
            Decimal columnTotal = 0;


            // Loop to sum columns and assign to label totals
            for (int j = 0; j <= allColumns.Length - 1; j++)
            {
                for (int k = 0; k <= allColumns[j].Length - 1; k++)
                {
                    columnTotal = columnTotal + allColumns[j][k];

                }

                colLblTotals[j].Text = columnTotal.ToString("0.00");
                columnTotal = 0;
            }

            btnSubmit.Enabled = true;
        }
        public Decimal VerifyNull(string textBox)
        {
            Decimal value;

            if (textBox == "")
            {
                value = 0;
            }
            else
            {
                value = Convert.ToDecimal(textBox);
            }

            return value;
        }

        protected void drpWeekRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            drpWeekRange.Items.Remove(drpWeekRange.Items.FindByValue("011900"));

            string selectedWeek = drpWeekRange.SelectedItem.Value;

            if (selectedWeek.Length < 6)
            {
                selectedWeek = "0" + selectedWeek;
            }

            int week = Int32.Parse(selectedWeek.Substring(0, 2));


            int currentYear = Int32.Parse(selectedWeek.Substring(2, 4));


            DateTime Firstday = FirstDateOfWeekISO8601(currentYear, week);

            // Populate the dates for current week.
            lblDateMon.Text = Firstday.ToShortDateString().Trim();
            lblDateTue.Text = (Firstday.AddDays(1)).ToShortDateString().Trim();
            lblDateWed.Text = (Firstday.AddDays(2)).ToShortDateString().Trim();
            lblDateThu.Text = (Firstday.AddDays(3)).ToShortDateString().Trim();
            lblDateFri.Text = (Firstday.AddDays(4)).ToShortDateString().Trim();
            //lblDateSat.Text = (Firstday.AddDays(5)).ToShortDateString().Trim();
            //lblDateSun.Text = (Firstday.AddDays(6)).ToShortDateString().Trim();

            ClearTextBoxes();
            // Access database to identify current user and retrieve contact name.
            // In production version, will be fName from database.
            haegerTimeDB.Open();
            string requests = "SELECT * FROM OakdaleRequests WHERE UserID = '" + currentID + "' AND WeekNum = " + week + " ORDER BY DateCreated DESC";
            SqlCommand requestsCommand = new SqlCommand(requests, haegerTimeDB);
            SqlDataReader requestsReader = requestsCommand.ExecuteReader();
            DataTable rqT = new DataTable();
            rqT.Load(requestsReader);
            int rqtCount = rqT.Rows.Count;

            if (rqtCount > 0)
            {
                lblHrRegMon.Text = rqT.Rows[0][8].ToString();
                txtHrVacMon.Text = rqT.Rows[0][9].ToString();
                txtHrARPMon.Text = rqT.Rows[0][10].ToString();
                txtHrHolidayMon.Text = rqT.Rows[0][11].ToString();
                txtHrBereaveMon.Text = rqT.Rows[0][12].ToString();
                txtHrJuryMon.Text = rqT.Rows[0][13].ToString();
                //txtHROTMon.Text = rqT.Rows[0][14].ToString();
                txtCommMon.Text = rqT.Rows[0][15].ToString();

                lblHrRegTue.Text = rqT.Rows[0][16].ToString();
                txtHrVacTue.Text = rqT.Rows[0][17].ToString();
                txtHrARPTue.Text = rqT.Rows[0][18].ToString();
                txtHrHolidayTue.Text = rqT.Rows[0][19].ToString();
                txtHrBereaveTue.Text = rqT.Rows[0][20].ToString();
                txtHrJuryTue.Text = rqT.Rows[0][21].ToString();
                //txtHROTTue.Text = rqT.Rows[0][22].ToString();
                txtCommTue.Text = rqT.Rows[0][23].ToString();

                lblHrRegWed.Text = rqT.Rows[0][24].ToString();
                txtHrVacWed.Text = rqT.Rows[0][25].ToString();
                txtHrARPWed.Text = rqT.Rows[0][26].ToString();
                txtHrHolidayWed.Text = rqT.Rows[0][27].ToString();
                txtHrBereaveWed.Text = rqT.Rows[0][28].ToString();
                txtHrJuryWed.Text = rqT.Rows[0][29].ToString();
                //txtHROTWed.Text = rqT.Rows[0][30].ToString();
                txtCommWed.Text = rqT.Rows[0][31].ToString();

                lblHrRegThu.Text = rqT.Rows[0][32].ToString();
                txtHrVacThu.Text = rqT.Rows[0][33].ToString();
                txtHrARPThu.Text = rqT.Rows[0][34].ToString();
                txtHrHolidayThu.Text = rqT.Rows[0][35].ToString();
                txtHrBereaveThu.Text = rqT.Rows[0][36].ToString();
                txtHrJuryThu.Text = rqT.Rows[0][37].ToString();
                //txtHROTThu.Text = rqT.Rows[0][38].ToString();
                txtCommThu.Text = rqT.Rows[0][39].ToString();

                lblHrRegFri.Text = rqT.Rows[0][40].ToString();
                txtHrVacFri.Text = rqT.Rows[0][41].ToString();
                txtHrARPFri.Text = rqT.Rows[0][42].ToString();
                txtHrHolidayFri.Text = rqT.Rows[0][43].ToString();
                txtHrBereaveFri.Text = rqT.Rows[0][44].ToString();
                txtHrJuryFri.Text = rqT.Rows[0][45].ToString();
                //txtHROTFri.Text = rqT.Rows[0][46].ToString();
                txtCommFri.Text = rqT.Rows[0][47].ToString();

                //lblHrRegSat.Text = rqT.Rows[0][48].ToString();
                //txtHrVacSat.Text = rqT.Rows[0][49].ToString();
                //txtHrARPSat.Text = rqT.Rows[0][50].ToString();
                //txtHrHolidaySat.Text = rqT.Rows[0][51].ToString();
                //txtHrBereaveSat.Text = rqT.Rows[0][52].ToString();
                //txtHrJurySat.Text = rqT.Rows[0][53].ToString();
                //txtHROTSat.Text = rqT.Rows[0][54].ToString();
                //txtCommSat.Text = rqT.Rows[0][55].ToString();

                //lblHrRegSun.Text = rqT.Rows[0][56].ToString();
                //txtHrVacSun.Text = rqT.Rows[0][57].ToString();
                //txtHrARPSun.Text = rqT.Rows[0][58].ToString();
                //txtHrHolidaySun.Text = rqT.Rows[0][59].ToString();
                //txtHrBereaveSun.Text = rqT.Rows[0][60].ToString();
                //txtHrJurySun.Text = rqT.Rows[0][61].ToString();
                //txtHROTSun.Text = rqT.Rows[0][62].ToString();
                //txtCommSun.Text = rqT.Rows[0][63].ToString();



            }
            else
            {
                string getNormalHours = "SELECT * FROM OakdaleTimeDefault";

                SqlCommand getNormalHoursCommand = new SqlCommand(getNormalHours, haegerTimeDB);

                if (IsPostBack)
                {
                    using (SqlDataReader gNH = getNormalHoursCommand.ExecuteReader())
                    {
                        while (gNH.Read())
                        {

                            // Monday
                            regHoursMon = Convert.ToDecimal(gNH[8]);

                            // Tuesday
                            regHoursTue = Convert.ToDecimal(gNH[16]);

                            // Wednesday
                            regHoursWed = Convert.ToDecimal(gNH[24]);

                            // Thursday
                            regHoursThu = Convert.ToDecimal(gNH[32]);

                            // Friday
                            regHoursFri = Convert.ToDecimal(gNH[40]);

                            lblHrRegMon.Text = regHoursMon.ToString();
                            lblHrRegTue.Text = regHoursTue.ToString();
                            lblHrRegWed.Text = regHoursWed.ToString();
                            lblHrRegThu.Text = regHoursThu.ToString();
                            lblHrRegFri.Text = regHoursFri.ToString();

                        }
                    }
                }
            }




            haegerTimeDB.Close();
            //btnCalc_Click(sender, e);

        }


        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            // Use first Thursday in January to get first week of the year as
            // it will never be in Week 52/53
            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            // As we're adding days to a date in Week 1,
            // we need to subtract 1 in order to get the right date for week #1
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            // Using the first Thursday as starting week ensures that we are starting in the right year
            // then we add number of weeks multiplied with days
            var result = firstThursday.AddDays(weekNum * 7);

            // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
            return result.AddDays(-3);
        }

        private void ClearTextBoxes()
        {
            // Clear Vacation Column
            txtHrVacMon.Text = string.Empty;
            txtHrVacTue.Text = "";
            txtHrVacWed.Text = "";
            txtHrVacThu.Text = "";
            txtHrVacFri.Text = "";
            //txtHrVacSat.Text = "";
            //txtHrVacSun.Text = "";
            // Clear ARP Column
            txtHrARPMon.Text = "";
            txtHrARPTue.Text = "";
            txtHrARPWed.Text = "";
            txtHrARPThu.Text = "";
            txtHrARPFri.Text = "";
            //txtHrARPSat.Text = "";
            //txtHrARPSun.Text = "";
            // Clear Unpaid Leave Column
            txtHrHolidayMon.Text = "";
            txtHrHolidayTue.Text = "";
            txtHrHolidayWed.Text = "";
            txtHrHolidayThu.Text = "";
            txtHrHolidayFri.Text = "";
            //txtHrHolidaySat.Text = "";
            //txtHrHolidaySun.Text = "";
            // Clear Bereavement Column
            txtHrBereaveMon.Text = "";
            txtHrBereaveTue.Text = "";
            txtHrBereaveWed.Text = "";
            txtHrBereaveThu.Text = "";
            txtHrBereaveFri.Text = "";
            //txtHrBereaveSat.Text = "";
            //txtHrBereaveSun.Text = "";
            // Clear Volunteer Column
            txtHrJuryMon.Text = "";
            txtHrJuryTue.Text = "";
            txtHrJuryWed.Text = "";
            txtHrJuryThu.Text = "";
            txtHrJuryFri.Text = "";
            //txtHrJurySat.Text = "";
            //txtHrJurySun.Text = "";
            // Clear OT Column
            //txtHROTMon.Text = "";
            //txtHROTTue.Text = "";
            //txtHROTWed.Text = "";
            //txtHROTThu.Text = "";
            //txtHROTFri.Text = "";
            //txtHROTSat.Text = "";
            //txtHROTSun.Text = "";
            // Clear Comments Column
            txtCommMon.Text = "";
            txtCommTue.Text = "";
            txtCommWed.Text = "";
            txtCommThu.Text = "";
            txtCommFri.Text = "";
            //txtCommSat.Text = "";
            //txtCommSun.Text = "";

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            btnCalc_Click(sender, e);

            // Totals
            Decimal regTotal = VerifyNull(lblTotalReg.Text);
            Decimal vacTotal = VerifyNull(lblTotalVac.Text);
            Decimal arpTotal = VerifyNull(lblTotalARP.Text);
            Decimal holTotal = VerifyNull(lblTotalHoliday.Text);
            Decimal berTotal = VerifyNull(lblTotalBereave.Text);
            Decimal jurTotal = VerifyNull(lblTotalJuryDuty.Text);
            Decimal otTotal = 0.00m;
            Decimal dotTotal = 0.00m;

            string comMon = txtCommMon.Text;
            string comTue = txtCommTue.Text;
            string comWed = txtCommWed.Text;
            string comThu = txtCommThu.Text;
            string comFri = txtCommFri.Text;
            string comSat = String.Empty;
            string comSun = String.Empty;

            // Get current selected week
            string weekSelected = drpWeekRange.SelectedItem.Value;

            // Check if value is correct length, if not add leading zero. Should be six (MMYYYY).
            if (weekSelected.Length < 6)
            {
                weekSelected = "0" + weekSelected;
            }

            // Break up value to determine week and year.
            int week = Int32.Parse(weekSelected.Substring(0, 2));
            int currentYear = Int32.Parse(weekSelected.Substring(2, 4));

            // Identify logged in user, current selected week, and selected week's year.
            //string currentID = User.Identity.GetUserId();
            string weekNumCurrent = weekSelected.Substring(0, 2);

            // Query and commands to retrieve max tracking number
            string trackNumQuery = "SELECT MAX(TrackNum) FROM OakdaleRequests";

            haegerTimeDB.Open();

            SqlCommand trackNumCommand = new SqlCommand(trackNumQuery, haegerTimeDB);
            string trackNum = "";
            using (SqlDataReader tn = trackNumCommand.ExecuteReader())
            {
                while (tn.Read())
                {
                    trackNum = tn[0].ToString();

                }
            }

            // Add one to max tracking number and creating new _OakdaleID
            int nextIDnum = (Int32.Parse(trackNum)) + 1;
            string strOakdaleID = "OAKRQ" + nextIDnum.ToString().Trim();

            // INSERT INTO QUERY
            string insertRequestQuery = "INSERT INTO OakdaleRequests " +
                                        "(_OakdaleRQ_ID, UserID, UserFName, UserLName, WeekNum, DateCreated, TeamLead, " +
                                        "RegHoursMon, VacHoursMon, ArpHoursMon, HolidayHoursMon, BereaveHoursMon, JuryDutyHoursMon, OverTimeHoursMon, DoubleOTHoursMon, CommentMon, " +
                                        "RegHoursTue, VacHoursTue, ArpHoursTue, HolidayHoursTue, BereaveHoursTue, JuryDutyHoursTue, OverTimeHoursTue, DoubleOTHoursTue, CommentTue, " +
                                        "RegHoursWed, VacHoursWed, ArpHoursWed, HolidayHoursWed, BereaveHoursWed, JuryDutyHoursWed, OverTimeHoursWed, DoubleOTHoursWed, CommentWed, " +
                                        "RegHoursThu, VacHoursThu, ArpHoursThu, HolidayHoursThu, BereaveHoursThu, JuryDutyHoursThu, OverTimeHoursThu, DoubleOTHoursThu, CommentThu, " +
                                        "RegHoursFri, VacHoursFri, ArpHoursFri, HolidayHoursFri, BereaveHoursFri, JuryDutyHoursFri, OverTimeHoursFri, DoubleOTHoursFri, CommentFri, " +
                                        "RegHoursSat, VacHoursSat, ArpHoursSat, HolidayHoursSat, BereaveHoursSat, JuryDutyHoursSat, OverTimeHoursSat, DoubleOTHoursSat, CommentSat, " +
                                        "RegHoursSun, VacHoursSun, ArpHoursSun, HolidayHoursSun, BereaveHoursSun, JuryDutyHoursSun, OverTimeHoursSun, DoubleOTHoursSun, CommentSun, " +
                                        "Status, ReqYear, RegHoursTotal, VacHoursTotal, ArpHoursTotal, HolidayHoursTotal, BereaveHoursTotal, JuryDutyHoursTotal, OverTimeHoursTotal, DoubleOTHoursTotal, ShowNoShow)" +
                                        "VALUES (@strOakdaleID, @currentID, @userFName, @userLName, @weekNumCurrent, @localDate, @teamLead, " +
                                        "@monReg, @monVac, @monARP, @monHol, @monBer, @monJur, @monOT, @monDOT, @monCom, " +
                                        "@tueReg, @tueVac, @tueARP, @tueHol, @tueBer, @tueJur, @tueOT,  @tueDOT, @tueCom, " +
                                        "@wedReg, @wedVac, @wedARP, @wedHol, @wedBer, @wedJur, @wedOT,  @wedDOT, @wedCom, " +
                                        "@thuReg, @thuVac, @thuARP, @thuHol, @thuBer, @thuJur, @thuOT,  @thuDOT, @thuCom, " +
                                        "@friReg, @friVac, @friARP, @friHol, @friBer, @friJur, @friOT,  @friDOT, @friCom, " +
                                        "@satReg, @satVac, @satARP, @satHol, @satBer, @satJur, @satOT,  @satDOT, @satCom, " +
                                        "@sunReg, @sunVac, @sunARP, @sunHol, @sunBer, @sunJur, @sunOT,  @sunDOT, @sunCom, " +
                                        "@status, @currentYear, @regTotal, @vacTotal, @arpTotal, @holTotal, @berTotal, @jurTotal, @otTotal, @dotTotal, @showNoShow)";

            SqlCommand insertRequestCommand = new SqlCommand(insertRequestQuery, haegerTimeDB);
            insertRequestCommand.Parameters.AddWithValue("@strOakdaleID", strOakdaleID);
            insertRequestCommand.Parameters.AddWithValue("@currentID", currentID);
            insertRequestCommand.Parameters.AddWithValue("@userFName", fName);
            insertRequestCommand.Parameters.AddWithValue("@userLName", lName);
            insertRequestCommand.Parameters.AddWithValue("@weekNumCurrent", week);
            insertRequestCommand.Parameters.AddWithValue("@localDate", localDate);
            insertRequestCommand.Parameters.AddWithValue("@teamLead", teamLead);
            insertRequestCommand.Parameters.AddWithValue("@monReg", regMon);
            insertRequestCommand.Parameters.AddWithValue("@monVac", vacMon);
            insertRequestCommand.Parameters.AddWithValue("@monARP", arpMon);
            insertRequestCommand.Parameters.AddWithValue("@monHol", holMon);
            insertRequestCommand.Parameters.AddWithValue("@monBer", berMon);
            insertRequestCommand.Parameters.AddWithValue("@monJur", jurMon);
            insertRequestCommand.Parameters.AddWithValue("@monOT", otMon);
            insertRequestCommand.Parameters.AddWithValue("@monCom", comMon);
            insertRequestCommand.Parameters.AddWithValue("@tueReg", regTue);
            insertRequestCommand.Parameters.AddWithValue("@tueVac", vacTue);
            insertRequestCommand.Parameters.AddWithValue("@tueARP", arpTue);
            insertRequestCommand.Parameters.AddWithValue("@tueHol", holTue);
            insertRequestCommand.Parameters.AddWithValue("@tueBer", berTue);
            insertRequestCommand.Parameters.AddWithValue("@tueJur", jurTue);
            insertRequestCommand.Parameters.AddWithValue("@tueOT", otTue);
            insertRequestCommand.Parameters.AddWithValue("@tueCom", comTue);
            insertRequestCommand.Parameters.AddWithValue("@wedReg", regWed);
            insertRequestCommand.Parameters.AddWithValue("@wedVac", vacWed);
            insertRequestCommand.Parameters.AddWithValue("@wedARP", arpWed);
            insertRequestCommand.Parameters.AddWithValue("@wedHol", holWed);
            insertRequestCommand.Parameters.AddWithValue("@wedBer", berWed);
            insertRequestCommand.Parameters.AddWithValue("@wedJur", jurWed);
            insertRequestCommand.Parameters.AddWithValue("@wedOT", otWed);
            insertRequestCommand.Parameters.AddWithValue("@wedCom", comWed);
            insertRequestCommand.Parameters.AddWithValue("@thuReg", regThu);
            insertRequestCommand.Parameters.AddWithValue("@thuVac", vacThu);
            insertRequestCommand.Parameters.AddWithValue("@thuARP", arpThu);
            insertRequestCommand.Parameters.AddWithValue("@thuHol", holThu);
            insertRequestCommand.Parameters.AddWithValue("@thuBer", berThu);
            insertRequestCommand.Parameters.AddWithValue("@thuJur", jurThu);
            insertRequestCommand.Parameters.AddWithValue("@thuOT", otThu);
            insertRequestCommand.Parameters.AddWithValue("@thuCom", comThu);
            insertRequestCommand.Parameters.AddWithValue("@friReg", regFri);
            insertRequestCommand.Parameters.AddWithValue("@friVac", vacFri);
            insertRequestCommand.Parameters.AddWithValue("@friARP", arpFri);
            insertRequestCommand.Parameters.AddWithValue("@friHol", holFri);
            insertRequestCommand.Parameters.AddWithValue("@friBer", berFri);
            insertRequestCommand.Parameters.AddWithValue("@friJur", jurFri);
            insertRequestCommand.Parameters.AddWithValue("@friOT", otFri);
            insertRequestCommand.Parameters.AddWithValue("@friCom", comFri);
            insertRequestCommand.Parameters.AddWithValue("@satReg", regSat);
            insertRequestCommand.Parameters.AddWithValue("@satVac", vacSat);
            insertRequestCommand.Parameters.AddWithValue("@satARP", arpSat);
            insertRequestCommand.Parameters.AddWithValue("@satHol", holSat);
            insertRequestCommand.Parameters.AddWithValue("@satBer", berSat);
            insertRequestCommand.Parameters.AddWithValue("@satJur", jurSat);
            insertRequestCommand.Parameters.AddWithValue("@satOT", otSat);
            insertRequestCommand.Parameters.AddWithValue("@satCom", comSat);
            insertRequestCommand.Parameters.AddWithValue("@sunReg", regSun);
            insertRequestCommand.Parameters.AddWithValue("@sunVac", vacSun);
            insertRequestCommand.Parameters.AddWithValue("@sunARP", arpSun);
            insertRequestCommand.Parameters.AddWithValue("@sunHol", holSun);
            insertRequestCommand.Parameters.AddWithValue("@sunBer", berSun);
            insertRequestCommand.Parameters.AddWithValue("@sunJur", jurSun);
            insertRequestCommand.Parameters.AddWithValue("@sunOT", otSun);
            insertRequestCommand.Parameters.AddWithValue("@sunCom", comSun);
            insertRequestCommand.Parameters.AddWithValue("@status", "Pending");
            insertRequestCommand.Parameters.AddWithValue("@currentYear", currentYear);
            insertRequestCommand.Parameters.AddWithValue("@regTotal", regTotal);
            insertRequestCommand.Parameters.AddWithValue("@vacTotal", vacTotal);
            insertRequestCommand.Parameters.AddWithValue("@arpTotal", arpTotal);
            insertRequestCommand.Parameters.AddWithValue("@holTotal", holTotal);
            insertRequestCommand.Parameters.AddWithValue("@berTotal", berTotal);
            insertRequestCommand.Parameters.AddWithValue("@jurTotal", jurTotal);
            insertRequestCommand.Parameters.AddWithValue("@otTotal", otTotal);
            insertRequestCommand.Parameters.AddWithValue("@dotTotal", dotTotal);
            insertRequestCommand.Parameters.AddWithValue("@showNoShow", "Show");
            insertRequestCommand.Parameters.AddWithValue("@monDOT", dotMon);
            insertRequestCommand.Parameters.AddWithValue("@tueDOT", dotTue);
            insertRequestCommand.Parameters.AddWithValue("@wedDOT", dotWed);
            insertRequestCommand.Parameters.AddWithValue("@thuDOT", dotThu);
            insertRequestCommand.Parameters.AddWithValue("@friDOT", dotFri);
            insertRequestCommand.Parameters.AddWithValue("@satDOT", dotSat);
            insertRequestCommand.Parameters.AddWithValue("@sunDOT", dotSun);

            insertRequestCommand.ExecuteNonQuery();

            haegerTimeDB.Close();


            string subject = "Time off Requested - " + localDate.ToString();
            string body = fName + " " + lName + " has requested time off.";

            try
            {

                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");

                _mailmsg.From = address;

                // Add your email address to the recipients
                _mailmsg.To.Add(teamLeadEmail);
                _mailmsg.Bcc.Add("webadmin@haeger.com");
                _mailmsg.Bcc.Add("regeah811@gmail.com");

                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                _mailmsg.Body = body;
                //Attachment attachment;
                //attachment = new Attachment("C:\\ExcelTest\\TestSheet1.xlsx");
                //_mailmsg.Attachments.Add(attachment);


                // Configure an SmtpClient to send the mail.
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;

                //_smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                //_smtp.PickupDirectoryLocation = @targetDirectory;

                // Send the msg.
                _smtp.Send(_mailmsg);

                // Display some feedback to the user to let them know it was sent.
                //lblResult.Text = "Your message was sent!";


                Response.Redirect("/PartnerNA/PartnerNAHome");

            }
            catch (Exception)
            {


            }

        } // END Submit

        //protected void btnStartOver_Click(object sender, EventArgs e)
        //{
        //    ClearTextBoxes();
        //    Response.Redirect(Request.RawUrl);

        //}
    }
}