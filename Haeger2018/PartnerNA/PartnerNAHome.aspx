﻿<%@ Page Title="Partner Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PartnerNAHome.aspx.cs" Inherits="Haeger2018.PartnerNA.PartnerNAHome" maintainScrollPositionOnPostBack = "true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <style>
        th,td
        {
            text-align:left;
            vertical-align:central;
            align-content:flex-start;
            height:auto;
            padding-right: 10px;
            padding-left:10px;
           
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container-fluid text-center">
        <div class="row">       
            <div class="col-xs-6">
                <div class="section-title">
                    <h2>Welcome, <asp:Label ID="lblPartnerName" runat="server" Text="Partner Name"></asp:Label></h2> 
                    <h3>Partner Home Page</h3>
                    <%--<h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>         --%> 
                    <hr />
                </div>
            </div>
             <div class="col-xs-6">
                 
                
                <br />
                <h4><asp:Label ID="lblDate" runat="server" Text="Current Week"></asp:Label></h4>
            </div>
           
           
            <br />
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-6">
                        <asp:Button CssClass="btn btn-primary" ID="btnCurrentWk" Width="220px" runat="server" Text="Edit Current Week" OnClick="btnCurrentWk_Click" />
                    
                        <br />
                        <br />
                        <asp:Button CssClass="btn btn-primary" ID="btnRequestOff" Width="220px" runat="server" Text="Request Time Off" OnClick="btnRequestOff_Click" />
                         <br />
                        <br />
                        <asp:Button CssClass="btn btn-primary" ID="brnPreviousRequests" Width="220px" runat="server" Text="Previous Requests" OnClick="brnPreviousRequests_Click" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-sm-6">
<%--                        <asp:Button CssClass="btn btn-primary" ID="btnPreviousRequests" runat="server" Text="Previous Requests" />--%>

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div>
            <div class="col-sm-4">


            </div>
            <div class="col-sm-4">


            </div>

        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HaegerTimeConnectionString %>" SelectCommand="SELECT * FROM [OakdaleTime]"></asp:SqlDataSource>
    </asp:Content>
