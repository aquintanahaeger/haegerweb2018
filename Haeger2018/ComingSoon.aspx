﻿<%@ Page Title="Coming Soon" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ComingSoon.aspx.cs" Inherits="Haeger2018.ComingSoon" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="row">
            <div class="span4"></div>
            <div class="span4"><img class="img-responsive col-centered" src="images/coming-soon.jpg" height="350" width="1000"/></div>
            <div class="span4"></div>
        </div>
    </div>

</asp:Content>
