﻿<%@ Page Title="Service Coordinator" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ServiceCoordinatorHome.aspx.cs" Inherits="Haeger2018.ServiceNACoordinator.ServiceCoordinatorHome" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <style>
         span1{
            color:#0085ca;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container-fluid text-center">
        <div class="row">       
            <div class="col-xs-6">
                <div class="section-title">
                    <%--<h2>Welcome, <asp:Label ID="lblPartnerName" runat="server" Text="Partner Name"></asp:Label></h2> --%>
                    <h3>Service Coordinator Home Page</h3>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>          
                    <hr />
                </div>
            </div>
             <div class="col-xs-6">
                 
                
                <br />
               <%-- <h4><asp:Label ID="lblDate" runat="server" Text="Current Week"></asp:Label></h4>--%>
            </div>
           
           
            <br />
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-6">
                        <asp:Button CssClass="btn btn-primary" Width="220px" ID="btnServiceApprove" runat="server" Text="Awaiting Approval" OnClick="btnServiceApprove_Click" />
                        <br />
                        <br />
                        <asp:Button CssClass="btn btn-primary" Width="220px" ID="btnAllTechs" runat="server" Text="Service Technicians" OnClick="btnAllTechs_Click" />

                    </div>
                
                    <div class="col-sm-6">
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-sm-6">
<%--                        <asp:Button CssClass="btn btn-primary" ID="btnPreviousRequests" runat="server" Text="Previous Requests" />--%>

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div>
            <div class="col-sm-4">


            </div>
            <div class="col-sm-4">


            </div>

        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>

    </asp:Content>
