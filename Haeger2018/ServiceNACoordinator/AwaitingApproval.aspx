﻿<%@ Page Title="Awaiting Approval" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AwaitingApproval.aspx.cs" Inherits="Haeger2018.ServiceNACoordinator.AwaitingApproval" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1{
            color:#0085ca;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Service Administration Page</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <hr />
        <br />
        <asp:Button ID="btnBackServCoor" CssClass="btn btn-primary" Width="220px" runat="server" Text="Back to Main" OnClick="btnBackServCoor_Click"/>
        <br />
        <br />
        <asp:Button ID="btnAllServTech" CssClass="btn btn-primary" Width="220px" runat="server" Text="Service Technicians" OnClick="btnAllServTech_Click"  />
        
        <div class="row">
            <div class="col-sm-4">
                <h3>Service Technicians to be Approved:</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <asp:GridView ID="grdAwaitingApproval"  CssClass= "table table-striped table-bordered table-condensed" AutoGenerateEditButton="True" 
                    runat="server" AutoGenerateColumns="False" EmptyDataText="Currently No Service Technicians to Approve" DataKeyNames="Id" DataSourceID="sqlAwaitingApproval">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" Visible="false" />
                        <asp:BoundField DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName" />
                        <asp:BoundField DataField="ContactName" HeaderText="Contact Name" SortExpression="ContactName" />
                        <asp:BoundField DataField="ContactPhone" HeaderText="Contact Phone" SortExpression="ContactPhone" />
                        <asp:CheckBoxField DataField="LockoutEnabled" HeaderText="Approved" SortExpression="LockoutEnabled"></asp:CheckBoxField>
                        <asp:CheckBoxField DataField="EmailConfirmed" HeaderText="Email Confirmed" SortExpression="EmailConfirmed" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlAwaitingApproval" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:aspnet-Haeger2018-MembershipConnectionString-dist-admin-NA %>" 
                    DeleteCommand="DELETE FROM [AspNetUsers] WHERE [Id] = @original_Id AND (([CompanyName] = @original_CompanyName) OR ([CompanyName] IS NULL AND @original_CompanyName IS NULL)) AND (([ContactName] = @original_ContactName) OR ([ContactName] IS NULL AND @original_ContactName IS NULL)) AND (([ContactPhone] = @original_ContactPhone) OR ([ContactPhone] IS NULL AND @original_ContactPhone IS NULL)) AND [LockoutEnabled] = @original_LockoutEnabled AND [EmailConfirmed] = @original_EmailConfirmed" 
                    InsertCommand="INSERT INTO [AspNetUsers] ([Id], [CompanyName], [ContactName], [ContactPhone], [LockoutEnabled], [EmailConfirmed]) VALUES (@Id, @CompanyName, @ContactName, @ContactPhone, @LockoutEnabled, @EmailConfirmed)" 
                    OldValuesParameterFormatString="original_{0}" 
                    SelectCommand="SELECT AspNetUsers.Id, CompanyName, ContactName, ContactPhone, LockoutEnabled,EmailConfirmed
                                    FROM AspNetUsers
                                    LEFT JOIN AspNetUserRoles ON AspNetUsers.Id = UserId
                                    LEFT JOIN AspNetRoles ON RoleId = AspNetRoles.Id
                                    WHERE AspNetRoles.Name = 'ServiceNA'
                                    AND LockoutEnabled = 'False'"    
                    UpdateCommand="UPDATE [AspNetUsers] 
                                    SET [CompanyName] = @CompanyName, [ContactName] = @ContactName, [ContactPhone] = @ContactPhone, [LockoutEnabled] = @LockoutEnabled, 
                                        [EmailConfirmed] = @EmailConfirmed 
                                    WHERE [Id] = @original_Id AND (([CompanyName] = @original_CompanyName) OR ([CompanyName] IS NULL AND @original_CompanyName IS NULL)) 
                                    AND (([ContactName] = @original_ContactName) OR ([ContactName] IS NULL AND @original_ContactName IS NULL)) 
                                    AND (([ContactPhone] = @original_ContactPhone) OR ([ContactPhone] IS NULL AND @original_ContactPhone IS NULL)) 
                                    AND [LockoutEnabled] = @original_LockoutEnabled AND [EmailConfirmed] = @original_EmailConfirmed">
                    <DeleteParameters>
                        <asp:Parameter Name="original_Id" Type="String" />
                        <asp:Parameter Name="original_CompanyName" Type="String" />
                        <asp:Parameter Name="original_ContactName" Type="String" />
                        <asp:Parameter Name="original_ContactPhone" Type="String" />
                        <asp:Parameter Name="original_LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="original_EmailConfirmed" Type="Boolean" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Id" Type="String" />
                        <asp:Parameter Name="CompanyName" Type="String" />
                        <asp:Parameter Name="ContactName" Type="String" />
                        <asp:Parameter Name="ContactPhone" Type="String" />
                        <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:Parameter DefaultValue="False" Name="LockoutEnabled" Type="Boolean" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="CompanyName" Type="String" />
                        <asp:Parameter Name="ContactName" Type="String" />
                        <asp:Parameter Name="ContactPhone" Type="String" />
                        <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                        <asp:Parameter Name="original_Id" Type="String" />
                        <asp:Parameter Name="original_CompanyName" Type="String" />
                        <asp:Parameter Name="original_ContactName" Type="String" />
                        <asp:Parameter Name="original_ContactPhone" Type="String" />
                        <asp:Parameter Name="original_LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="original_EmailConfirmed" Type="Boolean" />
                    </UpdateParameters>
                </asp:SqlDataSource>


            </div>

        </div>
    
    
    
    
    
    </div>

</asp:Content>
