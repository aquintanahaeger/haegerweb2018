﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.ServiceNACoordinator
{
    public partial class AwaitingApproval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAllServTech_Click(object sender, EventArgs e)
        {
            Response.Redirect("/ServiceNACoordinator/AllServiceTechs");
        }

        protected void btnBackServCoor_Click(object sender, EventArgs e)
        {
            Response.Redirect("/ServiceNACoordinator/ServiceCoordinatorHome");
        }
    }
}