﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018
{
    public partial class MachineManualsType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMachineType.Text = Session["machineName"] + " Manuals";
        }

        protected void btnGetInfo_Click(object sender, EventArgs e)
        {
            var fileLocation = drpManuals2.SelectedItem.Value;

            Response.Redirect(fileLocation);
        }

        protected void getAdobeReader_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("https://get.adobe.com/reader/");
        }
    }
}



