﻿<%@ Page Title="Manual Tooling" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MTW.aspx.cs" Inherits="Haeger2018.MTW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <style type="text/css">
        .ListControl
        {
            display:flex;
            align-items:center;
            justify-content:center;
            margin-left:auto;
            margin-right:auto;

        }
        .ListControl input[type=checkbox], input[type=radio]
        {
            background-color:none;
            display:inline-grid;
            align-items:center;
            width:auto;
            border: 15px solid red;
            margin-left:auto;
            margin-right:auto;
        }
        .ListControl label
        {
            
            width:auto;
            margin-left:10px;
            margin-right:10px;
            padding-left:20px;
            padding-right:10px;
            color:black;
        }
        

        .ValidateControl {
            color:black;
        }

        .machineImage
        {
            display:flex;
            align-items:center;
            justify-content:center;
            width:100%;
            height:100%;
        }

        .resultTop
        {
            color: black;
            font-size: small;

        }

           /* WIZARD */
        .stepNotCompleted
        {
            background-color: rgb(153,153,153);
            width: 15px;
            border: 1px solid rgb(153,153,153);
            margin-right: 5px;
            color: black;
            font-family: Arial;
            font-size: 12px;
            text-align: center;
        }

        .stepCompleted
        {
            background-color: #4d4d4d;
            width: 15px;
            border: 1px solid #4d4d4d;
            color: black;
            font-family: Arial;
            font-size: 12px;
            text-align: center;
        }

        .stepCurrent
        {
            background-color: #e01122;
            width: 15px;
            border: 1px solid #e01122;
            color: Black;
            font-family: Arial;
            font-size: 12px;
            font-weight: normal;
            text-align: center;
        }

        .stepBreak
        {
            width: 3px;
            background-color: Transparent;
        }

        .wizardProgress
        {
            padding-right: 10px;
            font-family: Arial;
            color: #333333;
            font-size: 12px;

        }

        .wizardTitle {
            font-family: Arial;
            font-size: 120%;
            font-weight: normal;
            color: black;
            vertical-align: middle;
            padding-right: 10px;
        }
         span1{
            color:#0085ca;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
         <div class="section-title">
                    <h2 style="margin: 0;">Haeger Manual Tooling Wizard</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                   
               </div>
            <br />
      
   

            <div class="row">
                <div class="col-sm-2">
                    <asp:Image ID="imgMachineSelected" CssClass="machineImage" runat="server" Visible="false" />


                    </div>
                <div class="col-sm-3">
                    <asp:Label ID="lblMachineType" CssClass="resultTop" runat="server" Text="Machine Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblSetType" CssClass="resultTop" runat="server" Text="Set Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblFastener" CssClass="resultTop" runat="server" Text="Fastener" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblThreadType" CssClass="resultTop" runat="server" Text="Thread Type" Visible="false"></asp:Label>
                    <br />
                    <asp:Label ID="lblDashLength" CssClass="resultTop" runat="server" Text="Dash Length" Visible="false"></asp:Label>
                </div>
     <%--Manual Tooling Wizard--%>
       
                <div class="col-sm-6">
                    <asp:Wizard ID="Wizard1" CssClass="ListControl" runat="server" Height="" Width="600px" DisplaySideBar="false" 
                                OnFinishButtonClick="Wizard1_FinishButtonClick" OnNextButtonClick="Wizard1_NextButtonClick" 
                                ActiveStepIndex="0" StartNextButtonStyle-CssClass="hidden" StepNextButtonStyle-CssClass="hidden" FinishCompleteButtonStyle-CssClass="hidden">
                        <HeaderTemplate>
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="wizardTitle">
                                        <%= Wizard1.ActiveStep.Title%>
                                    </td>
                                   
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <SideBarTemplate>
                        </SideBarTemplate>
           
                        <StartNavigationTemplate>
                            <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" Text="Next" Visible="false" />
                        </StartNavigationTemplate>

                         <WizardSteps>

                        <%--Choose a Machine--%>
                            <asp:WizardStep ID="WizardStep1" runat="server" Title="Choose a Machine" StepType="Start">
                                <asp:RadioButtonList ID="rblMachineSelection" CssClass="notranslate" runat="server" AutoPostBack="true"
                                            DataSourceID="sqlMachineSelection" DataTextField="machineType" DataValueField="imageName" 
                                            RepeatColumns="2" OnSelectedIndexChanged="rblMachineSelection_SelectedIndexChanged">

                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvMachineSelection" CssClass="ValidateControl" runat="server" 
                                                    ErrorMessage="Please Select a Machine" ControlToValidate="rblMachineSelection">

                                </asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="sqlMachineSelection" runat="server" ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT DISTINCT [machineType], [imageName]  
                                                            FROM [machineNameSetTypes] 
                                                            WHERE (([setType] = @setType))">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="MANUAL" Name="setType" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:WizardStep>

                        <%--Choose a Fastener--%>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="Choose a Fastener">
                        <asp:RadioButtonList ID="rblFastener" runat="server" RepeatColumns="4" AutoPostBack="true"
                                            DataSourceID="sqlFastener" DataTextField="ftype" DataValueField="ftype" 
                                            OnSelectedIndexChanged="rblFastener_SelectedIndexChanged">

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvFastener" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Fastener" ControlToValidate="rblFastener">

                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="sqlFastener" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT DISTINCT [ftype] FROM [twMainDBManual] 
                                                            WHERE (([machineType] = @machineType) 
                                                                    AND ([setType] = @setType))">
                            <SelectParameters>
                                <asp:SessionParameter Name="machineType" SessionField="mtwMachineSelected" Type="String" />
                                <asp:Parameter DefaultValue="MANUAL" Name="setType" />                            
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </asp:WizardStep>

                    <%--Choose a Thread Type--%>
                    <asp:WizardStep ID="WizardStep3" runat="server" Title="Choose a Thread Type">
                        <asp:RadioButtonList ID="rblThreadType" runat="server" AutoPostBack="true"
                                                DataSourceID="sqlThreadType" DataTextField="fsize" DataValueField="fsize" 
                                                RepeatColumns="5" OnSelectedIndexChanged="rblThreadType_SelectedIndexChanged" >

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvThreadType" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Thread Type" ControlToValidate="rblThreadType">

                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="sqlThreadType" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                            SelectCommand="SELECT DISTINCT [fsize] FROM [twMainDBManual] 
                                            WHERE (([machineType] = @machineType) 
                                                    AND ([setType] = @setType)
                                                    AND ([ftype] = @ftype))">

                            <SelectParameters>
                                <asp:SessionParameter Name="machineType" SessionField="mtwMachineSelected" Type="String" />
                                <asp:Parameter DefaultValue="MANUAL" Name="setType" />                            
                                <asp:SessionParameter Name="ftype" SessionField="mtwFastenerSelected" Type="String" />
                            </SelectParameters>

                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="sqlDashLength" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                                            SelectCommand="SELECT [dashMin], [dashMax] FROM [twMainDBManual] 
                                                            WHERE (([machineType] = @machineType) 
                                                                    AND ([setType] = @setType)
                                                                    AND ([ftype] = @ftype)
                                                                    AND ([fsize] = @fsize))">

                            <SelectParameters>
                                <asp:SessionParameter Name="machineType" SessionField="mtwMachineSelected" Type="String" />
                                <asp:Parameter DefaultValue="MANUAL" Name="setType" />                            
                                <asp:SessionParameter Name="ftype" SessionField="mtwFastenerSelected" Type="String" />
                                <asp:SessionParameter Name="fsize" SessionField="mtwThreadTypeSelected" Type="String" />
                            </SelectParameters>

                        </asp:SqlDataSource>

                    </asp:WizardStep>
                    
                    <%--Choose a Dash Length--%>
                    <asp:WizardStep ID="WizardStep4" runat="server" Title="Choose a Dash Length">
                         <asp:RadioButtonList ID="rblDashLength" runat="server" AutoPostBack="true"
                             RepeatDirection="Horizontal" RepeatColumns="5" 
                             OnSelectedIndexChanged="rblDashLength_SelectedIndexChanged">

                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvDashLength" runat="server" CssClass="ValidateControl" 
                                                    ErrorMessage="Please Select a Dash Length" ControlToValidate="rblDashLength">

                        </asp:RequiredFieldValidator>
                        

                    </asp:WizardStep>

                              <%--Results--%>
                    <asp:WizardStep ID="WizardStep5" runat="server" StepType="Complete" Title="Results">
                       
                        <asp:GridView ID="grdResults1" CssClass="table table-striped resultTop" runat="server" DataSourceID="sqlResults1" 
                                      AutoGenerateColumns="false" Width="100%" text-align="center">
                            <Columns >
                                <asp:BoundField DataField="lowerTool" HeaderText="Lower Tool" />
                                <asp:BoundField DataField="upperTool" HeaderText="Upper Tool" />

                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlResults1" runat="server"
                                           ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>"
                                           SelectCommand="SELECT TOP 1 [lowerTool], [upperTool]
                                                           FROM [twMainDBManual]
                                                           WHERE (([machineType] = @machineType)
                                                                   AND ([setType] = @setType)
                                                                   AND ([ftype] = @ftype)
                                                                   AND ([fsize] = @fsize)
                                                                   AND ([dashMin] &lt;= @dashMin)
                                                                   AND ([dashMax] &gt;= @dashMax))">

                           <SelectParameters>
                               <asp:SessionParameter Name="machineType" SessionField="mtwMachineSelected" Type="String" />
                                <asp:Parameter DefaultValue="MANUAL" Name="setType" />                            
                               <asp:SessionParameter Name="ftype" SessionField="mtwFastenerSelected" Type="String" />
                               <asp:SessionParameter Name="fsize" SessionField="mtwThreadTypeSelected" Type="String" />
                               <asp:SessionParameter Name="dashMin" SessionField="mtwDashLengthSelected" Type="Int32" />
                               <asp:SessionParameter Name="dashMax" SessionField="mtwDashLengthSelected" Type="Int32" />
                           </SelectParameters>

                        </asp:SqlDataSource>

                        
                        <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                    </asp:WizardStep>



                </WizardSteps>
            </asp:Wizard>
            </div>
        </div>
    </div>



</asp:Content>
