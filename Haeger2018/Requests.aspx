﻿<%@ Page Title="Requests" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Requests.aspx.cs" Inherits="Haeger2018.Requests" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1{
            color:#0085ca;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="section-title">
                    <h2 style="margin: 0;">Haeger Requests</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                   
               </div>
            <br />
      
        <div class="row">
            <div class="col-sm-4 col-sm-push-2">
                <a class="tileLink" href="CustomTooling.aspx" target="_blank">
                    <div class="tile nosize dark-blue animated zoomIn animation-delay-3">
                        <h2><i class="fa fa-comments"></i>Custom Tooling Quote</h2>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-sm-push-2">
                <a class="tileLink" href="RequestRMA.aspx" target="_blank">
                    <div class="tile nosize dark-red animated zoomIn animation-delay-3">
                        <h2><i class="fa fa-comments"></i> Request RMA</h2>
                    </div>
                </a>
            </div>
          
        </div>
        <br />
        <div class="row">
            <div class="col-sm-4 col-sm-push-2">
                <a class="tileLink" href="SalesRequest.aspx" target="_blank">
                    <div class="tile nosize dark-green animated zoomIn animation-delay-3">
                        <h2><i class="fa fa-comments"></i>Sales Visit</h2>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-sm-push-2">
                <a class="tileLink" href="ServiceRequest.aspx" target="_blank">
                    <div class="tile nosize dark-purple animated zoomIn animation-delay-3">
                        
                        <h2><i class="fa fa-wrench"></i> Service Visit</h2>                  

                    </div>
                </a>
            </div>
          
        </div>
        <br />
        <br />
        <br />
</asp:Content>
