﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018
{
    public partial class SalesRequest : System.Web.UI.Page
    {
        SqlConnection salesRqConn = new SqlConnection(ConfigurationManager.ConnectionStrings["OptInConsentConnectionString"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;

        }

        protected void btnSlsRSubmit_Click(object sender, EventArgs e)
        {
            DateTime localDate = DateTime.Now;
            string date = localDate.ToLongDateString();

            var msg = "Without your consent we cannot process your request.";

            // First check to warn user 
            if (!gdprConsentBox.Checked)
            {
                lblConsentWrn.Visible = true;

                Response.Write("<script>alert('" + msg + "')</script>");

                return;
            }

            // *************** RFQ Form Variables ***************
            string companyName = txtSlsRCompany.Text.Trim();
            string firstName = txtSlsRFirstName.Text.Trim();
            string lastName = txtSlsLastName.Text.Trim();
            string phone = txtSlsRPhone.Text.Trim();
            string zipCode = txtSlsRZip.Text.Trim();
            string country = txtSlsRCountry.Text.Trim();
            string email = txtSlsREmail.Text.Trim();
            string contactAddress = txtSlsRAddress.Text.Trim();
            string city = txtSlsRCity.Text.Trim();
            string comments = txtSlsRComments.Text.Trim();

            // *************** INSERT DATABASE ***************

            salesRqConn.Open();

            // Query and commands to retrieve max tracking number
            string trackNumQuery = "SELECT MAX(trackingNum) FROM SalesRqConsent";
            SqlCommand trackNumCommand = new SqlCommand(trackNumQuery, salesRqConn);
            string trackNum = "";
            using (SqlDataReader tn = trackNumCommand.ExecuteReader())
            {
                while (tn.Read())
                {
                    trackNum = tn[0].ToString();
                }
            }

            // Add one to max tracking number and creating new _projID
            int nextIDnum = (Int32.Parse(trackNum)) + 1;
            string salesRqID = "salesRq" + nextIDnum.ToString();

            string insertQuery =
            "INSERT INTO [dbo].[salesRqConsent] " +
            "(_salesRqID, companyName, firstName, lastName, phone, zipCode, country, gdprConsent, dateCreated, email, address, city) " +
            "VALUES (@salesRqID, @companyName, @firstName, @lastName, @phone, @zipCode, @country, @gdprConsent, @dateCreated, @email, @address, @city)";

            SqlCommand insertCommand = new SqlCommand(insertQuery, salesRqConn);
            insertCommand.Parameters.AddWithValue("@salesRqID", salesRqID);
            insertCommand.Parameters.AddWithValue("@companyName", companyName);
            insertCommand.Parameters.AddWithValue("@firstName", firstName);
            insertCommand.Parameters.AddWithValue("@lastName", lastName);
            insertCommand.Parameters.AddWithValue("@phone", phone);
            insertCommand.Parameters.AddWithValue("@zipCode", zipCode);
            insertCommand.Parameters.AddWithValue("@country", country);
            insertCommand.Parameters.AddWithValue("@gdprConsent", 1);
            insertCommand.Parameters.AddWithValue("@dateCreated", localDate);
            insertCommand.Parameters.AddWithValue("@email", email);
            insertCommand.Parameters.AddWithValue("@address", contactAddress);
            insertCommand.Parameters.AddWithValue("@city", city);

            insertCommand.ExecuteNonQuery();
            salesRqConn.Close();



            // *************** EMAIL ***************

            var consentConfirm = "";

            if (gdprConsentBox.Checked)
            {
                consentConfirm = "Confirmed";
            }


            //Fetching Email Body Text from EmailTemplate File.
            string FilePath = @"D:/EmailTemplates/SalesRequestTemplate.html";
            FileStream sourceStream = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamReader str = new StreamReader(sourceStream);
            string MailText = str.ReadToEnd();
            str.Close();

            //Repalce variables
            MailText = MailText.Replace("[currentDate]", date);
            MailText = MailText.Replace("[newCompany]", txtSlsRCompany.Text.Trim());
            MailText = MailText.Replace("[newFirstName]", txtSlsRFirstName.Text.Trim());
            MailText = MailText.Replace("[newLastName]", txtSlsLastName.Text.Trim());
            MailText = MailText.Replace("[newPhone]", txtSlsRPhone.Text.Trim());
            MailText = MailText.Replace("[newEmailAddress]", txtSlsREmail.Text.Trim());
            MailText = MailText.Replace("[newState]", txtSlsRState.Text.Trim());
            MailText = MailText.Replace("[newAddress]", contactAddress);
            MailText = MailText.Replace("[newCity]", city);
            MailText = MailText.Replace("[newZipCode]", txtSlsRZip.Text.Trim());
            MailText = MailText.Replace("[newCountry]", txtSlsRCountry.Text.Trim());
            MailText = MailText.Replace("[newSalesInquiry]", rblSlsInquiry.SelectedItem.Text);
            MailText = MailText.Replace("[newOther]", txtSlsInqOther.Text.Trim());
            MailText = MailText.Replace("[newConsent]", consentConfirm);
            MailText = MailText.Replace("[newComments]", comments);

            // Session variable to pass email information to confirmation page.
            Session["emailConfirm"] = MailText;

            string subject = "Sales Request: " + txtSlsRCompany.Text + " - " + rblSlsInquiry.SelectedItem.Text;

            var customerEmail = txtSlsREmail.Text.Trim();

            try
            {

                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");

                _mailmsg.From = address;

                // Add your email address to the recipientss
                _mailmsg.To.Add("sales@haeger.com");
                _mailmsg.To.Add("ronboggs@haeger.com");
                _mailmsg.To.Add("msplaine@haeger.com");
                _mailmsg.To.Add("jgarcia@haeger.com");
                _mailmsg.To.Add("jkimberlin@haeger.com");
                _mailmsg.To.Add("kbeene@haeger.com");
                _mailmsg.CC.Add(customerEmail);
                _mailmsg.Bcc.Add("regeah811@gmail.com");
                _mailmsg.Bcc.Add("webadmin@haeger.com");

                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                _mailmsg.Body = MailText;

                // Configure an SmtpClient to send the mail.
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;


                //_smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                //_smtp.PickupDirectoryLocation = @targetDirectory;

                // Send the msg.
                _smtp.Send(_mailmsg);

                clearControls();

                Response.Redirect("/SubmitSucess.aspx");
            }
            catch
            {
                // If the message failed at some point, let the user know.
                lblResult.Text = "Your message failed to send, please try again.";

            }



        }
        private void clearControls()
        {

            txtSlsRCompany.Text = "";
            txtSlsRFirstName.Text = "";
            txtSlsLastName.Text = "";
            txtSlsRPhone.Text = "";
            txtSlsREmail.Text = "";
            txtSlsRState.Text = "";
            txtSlsRZip.Text = "";
            txtSlsRCountry.Text = "";
            rblSlsInquiry.ClearSelection();
            txtSlsInqOther.Text = "";



        }
    }
}