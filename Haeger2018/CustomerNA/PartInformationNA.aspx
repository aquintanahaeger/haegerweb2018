﻿<%@ Page Title="Part Info NA" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PartInformationNA.aspx.cs" Inherits="Haeger2018.CustomerNA.PartInformationNA" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="section-title">
            <h2 style="margin: 0;">Haeger North American Part Information</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>

        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="lead text-center">Please enter <b>Part Number</b> or <b>Description</b> and click Enter. If no information is shown, the part number does not exist</p>
                <p class="text-center" style="color:red"><b>NOTE: If Quantity on hand shows 0, then lead time applies. You can also enter a partial number to get the list. <br />If Quantity is 1, it is recommended that you call in to verify the part is in stock.</b></p> 
                <br />
                <br />    
                <h4>Enter Part Number:</h4>
                <asp:TextBox ID="txtPartNumberNA" CssClass="form-control input-lg center-block" runat="server"></asp:TextBox>
                <asp:Button ID="btnPartNumberNA" CssClass="btn btn-large" runat="server" Text="Search" />
                <br />
                <br />
                <asp:GridView ID="grdPartNumNA" CssClass="table table-striped table-bordered table-hover"  runat="server" AutoGenerateColumns="False" DataSourceID="sqlPartNumNA">
                    <Columns>
                        <asp:BoundField DataField="ITEMID" HeaderText="Part Number" SortExpression="ITEMID" />
                        <asp:BoundField DataField="ITEMNAME" HeaderText="Item Name" SortExpression="ITEMNAME" />
                        <asp:BoundField DataField="price" HeaderText="Price" SortExpression="price" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="qty_onhand" HeaderText="Quantity on Hand" SortExpression="qty_onhand" DataFormatString="{0:F3}" />
                        <asp:BoundField DataField="leadtime" HeaderText="Lead Time" SortExpression="leadtime" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlPartNumNA" runat="server" ConnectionString="<%$ ConnectionStrings:HaegerSalesConnectionString-Dist-PartInfo-NA %>" 
                    SelectCommand="SELECT DISTINCT [ITEMID], [ITEMNAME], [price], [qty_onhand], [leadtime] 
                    FROM [haegerwebitems] WHERE (([packaginggroupid] = @packaginggroupid) AND ([ITEMID] LIKE '%' + @ITEMID + '%'))">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Show" Name="packaginggroupid" Type="String" />
                        <asp:ControlParameter ControlID="txtPartNumberNA" Name="ITEMID" PropertyName="Text" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <h4>Enter Description:</h4>
                <asp:TextBox ID="txtDescripNA" CssClass="form-control input-lg center-block" runat="server"></asp:TextBox>
                <asp:Button ID="btnDescripNA" CssClass="btn btn-large" runat="server" Text="Search" />
                <br />
                <br />
                <asp:GridView ID="grdDescripNA" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="sqlDescripNA">
                    <Columns>
                        <asp:BoundField DataField="ITEMID" HeaderText="Part Number" SortExpression="ITEMID" />
                        <asp:BoundField DataField="ITEMNAME" HeaderText="Item Name" SortExpression="ITEMNAME" />
                        <asp:BoundField DataField="price" HeaderText="Price" SortExpression="price" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="qty_onhand" HeaderText="Quantity on Hand" SortExpression="qty_onhand" DataFormatString="{0:F3}" />
                        <asp:BoundField DataField="leadtime" HeaderText="Lead Time" SortExpression="leadtime"  />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlDescripNA" runat="server" ConnectionString="<%$ ConnectionStrings:HaegerSalesConnectionString-Dist-PartInfo-NA %>" SelectCommand="SELECT DISTINCT [ITEMID], [ITEMNAME], [price], [qty_onhand], [leadtime] FROM [haegerwebitems] WHERE (([ITEMNAME] LIKE '%' + @ITEMNAME + '%') AND ([packaginggroupid] = @packaginggroupid))">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtDescripNA" Name="ITEMNAME" PropertyName="Text" Type="String" />
                        <asp:Parameter DefaultValue="Show" Name="packaginggroupid" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </div>


        </div>
    </div>

    


</asp:Content>
