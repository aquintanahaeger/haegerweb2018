﻿<%@ Page Title="All Dist" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DistAllNA.aspx.cs" Inherits="Haeger2018.DistAdminNA.DistAllNA" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }

    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger All North American Distributors</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <br />
        <asp:Button ID="btnDistApp" CssClass="btn btn-primary" runat="server" Text="Back to Main" OnClick="btnDistApp_Click" />
        <hr />

            <h3>Distributor List:</h3>
                        <asp:GridView ID="grdAllDistNA" CssClass="table table-striped table-bordered table-condensed" runat="server" 
                                    AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="sqlAllDistNA" 
                                    OnSelectedIndexChanged="grdAllDistNA_SelectedIndexChanged" AllowSorting="True">
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" Visible="false" />
                                <asp:BoundField DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName" />
                                <asp:BoundField DataField="ContactName" HeaderText="Contact Name" SortExpression="ContactName" />
                                <asp:BoundField DataField="ContactPhone" HeaderText="Contact Phone" SortExpression="ContactPhone" />
                                <asp:BoundField DataField="LoginCount" HeaderText="Login Count" SortExpression="LoginCount" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="btnSelect" runat="server" Text="Detail" CommandName = "Select" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:SqlDataSource ID="sqlAllDistNA" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:aspnet-Haeger2018-MembershipConnectionString-dist-admin-NA %>" 
                             SelectCommand="SELECT AspNetUsers.Id, CompanyName, ContactName, ContactPhone, LoginCount
                                    FROM AspNetUsers
                                    LEFT JOIN AspNetUserRoles ON AspNetUsers.Id = UserId
                                    LEFT JOIN AspNetRoles ON RoleId = AspNetRoles.Id
                                    WHERE AspNetRoles.Name = 'DistributorNA'">

                        </asp:SqlDataSource>

        </div>
    

</asp:Content>
