$(document).ready(function () {
    $('#map').usmap({
        'stateSpecificStyles': {
            'AK': { fill: '#080808' }
        },
        'stateSpecificHoverStyles': {
            'AK': { fill: '#ff0' },
            'HI': { fill: '#ff0' }
        },

        'mouseoverState': {
            'HI': function (event, data) {
                //return false;
            }
        },


        'click': function (event, data) {
           
            $('[id$=HiddenField1]')
                .val(data.name)
                .stop()
            $('[id$=txtUserZip]').val("");
            __doPostBack('[id$=anchorId]', '');
            


        }
    });


});