﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Haeger2018.Models;

namespace Haeger2018.Logic
{
    internal class RoleActions
    {
        internal void AddUserAndRole()
        {
            // Access the application context and create result variables.
            Models.ApplicationDbContext context = new ApplicationDbContext();
            IdentityResult IdRoleResult;
            IdentityResult IdUserResult;

            // Create a RoleStore object by using the ApplicationDbContext object. 
            // The RoleStore is only allowed to contain IdentityRole objects.
            var roleStore = new RoleStore<IdentityRole>(context);

            // Create a RoleManager object that is only allowed to contain IdentityRole objects.
            // When creating the RoleManager object, you pass in (as a parameter) a new RoleStore object. 
            var roleMgr = new RoleManager<IdentityRole>(roleStore);

            // Then, you create the "canEdit" role if it doesn't already exist.
            if (!roleMgr.RoleExists("RootAdmin"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "RootAdmin" });
            }
            if (!roleMgr.RoleExists("Admin"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "Admin" });
            }
            if (!roleMgr.RoleExists("AdminAS"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "AdminAS" });
            }
            if (!roleMgr.RoleExists("AdminEU"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "AdminEU" });
            }
            if (!roleMgr.RoleExists("AdminNA"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "AdminNA" });
            }
            if (!roleMgr.RoleExists("DistAdminAS"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "DistAdminAS" });
            }
            if (!roleMgr.RoleExists("CustAdminAS"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "CustAdminAS" });
            }
            if (!roleMgr.RoleExists("PartAdminAS"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "PartAdminAS" });
            }
            if (!roleMgr.RoleExists("DistAdminEU"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "DistAdminEU" });
            }
            if (!roleMgr.RoleExists("CustAdminEU"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "CustAdminEU" });
            }
            if (!roleMgr.RoleExists("PartAdminEU"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "PartAdminEU" });
            }
            if (!roleMgr.RoleExists("DistAdminNA"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "DistAdminNA" });
            }
            if (!roleMgr.RoleExists("CustAdminNA"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "CustAdminNA" });
            }
            if (!roleMgr.RoleExists("PartAdminNA"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "PartAdminNA" });
            }
            if (!roleMgr.RoleExists("CustomerAS"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "CustomerAS" });
            }
            if (!roleMgr.RoleExists("CustomerEU"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "CustomerEU" });
            }
            if (!roleMgr.RoleExists("CustomerNA"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "CustomerNA" });
            }
            if (!roleMgr.RoleExists("DistributorAS"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "DistributorAS" });
            }
            if (!roleMgr.RoleExists("DistributorEU"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "DistributorEU" });
            }
            if (!roleMgr.RoleExists("DistributorNA"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "DistributorNA" });
            }
            if (!roleMgr.RoleExists("PartnerAS"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "PartnerrAS" });
            }
            if (!roleMgr.RoleExists("PartnerEU"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "PartnerEU" });
            }
            if (!roleMgr.RoleExists("PartnerNA"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "PartnerNA" });
            }
            if (!roleMgr.RoleExists("PartOutSalesNA"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "PartOutSalesNA" });
            }
            if (!roleMgr.RoleExists("PartOutSalesEU"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "PartOutSalesEU" });
            }
            if (!roleMgr.RoleExists("PartOutSalesAS"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "PartOutSalesAS" });
            }
            if (!roleMgr.RoleExists("ServiceAS"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "ServiceAS" });
            }
            if (!roleMgr.RoleExists("ServiceEU"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "ServiceEU" });
            }
            if (!roleMgr.RoleExists("ServiceNA"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "ServiceNA" });
            }
            if (!roleMgr.RoleExists("ServiceNACoordinator"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "ServiceNACoordinator" });
            }


            // Create a UserManager object based on the UserStore object and the ApplicationDbContext  
            // object. Note that you can create new objects and use them as parameters in
            // a single line of code, rather than using multiple lines of code, as you did
            // for the RoleManager object.
            var userMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var rootAdmin = new ApplicationUser
            {
                UserName = "jloya@haeger.com",
                Email = "jloya@haeger.com"
            };
            IdUserResult = userMgr.Create(rootAdmin, "Lnikon!007");

            var appAdmin = new ApplicationUser
            {
                UserName = "rboggs@haeger.com",
                Email = "rboggs@haeger.com"
            };
            IdUserResult = userMgr.Create(appAdmin, "PW4Haeger!");

            



            // If the new "canEdit" user was successfully created, 
            // add the "canEdit" user to the "canEdit" role. 
            if (!userMgr.IsInRole(userMgr.FindByEmail("jloya@haeger.com").Id, "RootAdmin"))
            {
                IdUserResult = userMgr.AddToRole(userMgr.FindByEmail("jloya@haeger.com").Id, "RootAdmin");
            }

            if (!userMgr.IsInRole(userMgr.FindByEmail("rboggs@haeger.com").Id, "Admin"))
            {
                IdUserResult = userMgr.AddToRole(userMgr.FindByEmail("rboggs@haeger.com").Id, "Admin");
            }



        }



    }
}