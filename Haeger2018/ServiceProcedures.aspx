﻿<%@ Page Title="Service Procedures" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ServiceProcedures.aspx.cs" 
    Inherits="Haeger2018.ServiceProcedures" MaintainScrollPositionOnPostback="true" 


 %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
         span1{
            color:#0085ca;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="section-title">
                    <h2 style="margin: 0;">Haeger Service Procedures</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                   
               </div>
            <br />
      
        <div class="col-md-12 text-center">
           
            <div>
                <asp:DropDownList ID="drpServiceProc" class="btn btn-default dropdown-toggle notranslate" runat="server" AppendDataBoundItems="true" DataSourceID="sqlServProcMachine" DataTextField="machineType" DataValueField="machineType">
                <asp:ListItem Text="----Make a Selection----" Value="" />
                </asp:DropDownList>
                <asp:SqlDataSource ID="sqlServProcMachine" runat="server" ConnectionString="<%$ ConnectionStrings:ServiceProceduresConnectionString2 %>" SelectCommand="SELECT DISTINCT [machineType] FROM [serviceBulletins$]"></asp:SqlDataSource>
                <asp:Button ID="btnSPGetInfo" CssClass="btn btn-default" runat="server" Text="Get Info" OnClick="btnSPGetInfo_Click"  />
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
