﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Haeger2018._Default" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="Head" runat="server">
    <style>
        .card
        {
            position:relative ;
            height: 250px;
            width: 325px;
            
            }
     
        .card:hover .imageRed 
        {
          outline: solid red;
        }
        .card:hover .imagePurple 
        {
          outline: solid purple;
        }
        .card:hover .imageGreen
        {
          outline: solid green
        }
        .card:hover .imageBlue
        {
          outline: solid #0085ca
        }

      
    </style>

</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <!-- Machine Carousel-->
  <section>
        <div id="page-carousel" class="carousel carousel-mind margin-bottom-50 slide" data-ride="carousel" data-interval="false">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#page-carousel" data-slide-to="0" class="active"></li>
               <%-- <li data-target="#page-carousel" data-slide-to="1" class=""></li>--%>
                
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
 
            <div class="item active" style="background-image: url(/images/industrialBackground.png); background-size: cover; background-position: center center;">                
                    <div class="container">
                        <div class="row margin-top-20">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12 col-sm-6">
                                        <div class="carousel-caption">
                                            <div class="carousel-text">
                                                <h1 class="animated fadeInDownBig">Haeger<br />
                                                    Insertion<br />
                                                    Machines</h1>
                                                <h3 class="animated fadeInUpBig animation-delay-7">Always the most cost effective solution for your challenge!</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 text-center  hidden-sm hidden-xs">
                                <div class="carousel-text">
                                    <a href="Products.aspx" class="grow">
                                        <img src="/images/824.wt-4e.png" class="animated bounceInDown animation-delay-7" alt="Image"/></a>
                                    <h4 class="animated bounceInDown animation-delay-12 margin-top-minus-75 notranslate"><span class="boxed">824 Window Touch 4<span class="haegerE">e</span></span></h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center  hidden-sm hidden-xs">
                                <div class="carousel-text">
                                    <a href="Products.aspx" class="grow">
                                        <img src="/images/824.ot.4e.png" class="animated bounceInDown animation-delay-11" alt="Image"></a>
                                    <h4 class="animated bounceInDown animation-delay-16 margin-top-minus-75 notranslate"><span class="boxed"/>824 One Touch 4<span class="haegerE">e</span></span></h4>
                                </div>
                            </div>
                            <div class="col-md-3 text-center  hidden-sm hidden-xs">
                                <div class="carousel-text">
                                    <a href="Products.aspx" class="grow">
                                        <img src="/images/824.ot.4e.lite.png" class="animated bounceInDown animation-delay-9" alt="Image"/></a>
                                    <h4 class="animated bounceInDown animation-delay-14 margin-top-minus-75 notranslate"><span class="boxed">824 One Touch 4<span class="haegerE">e</span> LITE</span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            
                
        <div class="item" style="background-image: url(/images/FabtechMexicoBoothGraphics3.jpg); background-size:initial; background-position:center;">
            <div class="container content">
                <a href="https://www.pemnet.com/" target="_blank">
               <%-- <img  src="/images/FabtechMexicoBoothGraphics.jpg"  />--%>
                    </a>
               </div>
        </div>
</div>
        <!-- Controls -->
        <a class="left carousel-control hidden" href="#page-carousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control hidden" href="#page-carousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
        </div>
    </section>
   <!--End carousel -->
    
      <!-- Buttons -->
     <section>
   <div class="container content">
        <div class="row col-centered">
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <a class="hover" href="Products.aspx" target="_blank">
                    <div class="card dark-red animated zoomIn animated delay-3" style="min-height:230px; max-height:230px; min-width:328px; margin-bottom:20px;">
                        <img class="img-responsive imageRed" src="images/haegerlineup.jpg" style="min-height:230px; max-height:230px; min-width:328px" />
                        </div>
                </a>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <a class="hover" href="Wizards.aspx" target="_blank">
                    <div class="card dark-red animated zoomIn animated delay-3" style="min-height:230px; max-height:230px; min-width:328px; margin-bottom:20px;">
                        <img class="img-responsive imagePurple" src="images/toolingButtonImage.jpg" style="min-height:230px; max-height:230px; min-width:328px" />

                    </div>
                </a>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <a class="hover" href="Documents.aspx" target="_blank">
                    <div class="card animated zoomIn animated delay-3" style="min-height:230px; max-height:230px; min-width:328px; margin-bottom:20px;">
                        <img class="img-responsive imageGreen" src="images/manuals.jpg" style="min-height:230px; max-height:230px; min-width:328px" />

                    </div>
                </a>
            </div>
        
        
            <div class="col-lg-4 col-lg-push-4 col-md-6 col-sm-12 col-xs-12">
                <a class="hover" href="https://www.pemnet.com/fastening-products/pem-self-clinching-fasteners-new/" target="_blank">
                    <div class="card animated zoomIn animated delay-3" style="min-height:230px; max-height:230px; min-width:328px; margin-bottom:20px;">
                        <img class="img-responsive imageBlue" src="images/PEMFastenerPhoto328x230.jpg" style="min-height:230px; max-height:230px; min-width:328px" />

                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-lg-push-4 col-md-6 col-md-push-3 col-sm-12 col-xs-12">
                <a class="btn btn-primary btn-block animated bounceInRight animation-delay-5 margin-bottom-20" style="max-width:328px" href="ContactUs.aspx" target="_blank">
                        <div class="row row-vertical-align row-no-pad">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-1">
                                <i class="fa fa-phone-square pull-right fa-2x"></i>
                            </div>
                            <div class="col-md-6">
                                <div style="margin-left: 10px; text-align: left;">
                                    Request Information Now<br />
                                    USA 1-800-878-4343 <br />
                                    Europe +31-541-530-230 <br />
                                    China +86-21-5695-4988
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                    </a>

                    <a class="btn btn-black btn-block animated bounceInRight animation-delay-7 margin-bottom-10" style="max-width:328px"  href="https://itunes.apple.com/us/app/haeger-wizard/id625478700?mt=8&uo=4" target="_itunes">
                        <div class="row row-vertical-align row-no-pad" >
                            <div class="col-md-6">
                                <span style="font-size: 18px;">Haeger Wizard</span>
                            </div>
                            <div class="col-md-5">
                                <img src="/images/appstore.png" />
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </a>
                
                    <a class="btn btn-black btn-block animated bounceInRight animation-delay-9" style="max-width:328px" href="https://play.google.com/store/apps/details?id=edu.haeger.haegerwizard&hl=en" target="_itunes">
                        <div class="row row-vertical-align row-no-pad">
                            <div class="col-md-6">
                                <span style="font-size: 18px;">Haeger Wizard</span>
                            </div>
                            <div class="col-md-5">
                                <img src="/images/googleplay.png" />
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </a>

            </div>
        </div>
    </div>
</section>


   
</asp:Content>
