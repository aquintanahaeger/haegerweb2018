﻿<%@ Page Title="Dist AS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DistributorASPage.aspx.cs" Inherits="Haeger2018.DistributorAS.DistributorASPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
     <style>
        span1{
            color:#0085ca;
        }

    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Distributor Central - Asia</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
         <br />
        <div class="row">
            <div class="col-sm-4">
                 <h3>Useful Pages</h3>
                <hr />
                <%-- <asp:Button ID="btnMarketing" CssClass="btn btn-primary" runat="server" Text="Marketing Materials" OnClick="btnMarketing_Click" />
                &nbsp&nbsp--%>
                <asp:Button ID="btnStdProp" CssClass="btn btn-primary" runat="server" Text="Standard Proposals" OnClick="btnStdProp_Click" />
            </div>
            <div class="col-sm-4">
                <h3>Useful Forms</h3>
                <hr />
               <%-- <a href="/CustomerEU/PartInformationEU">Part and Tooling Availability</a>
                <br />
                 <a href="/SalesRequest">Request Sales</a>
                <br />
                <a href="/ServiceRequest">Request Service</a>
                <br />
                <a href="/RequestRMA">Request RMA</a>
                <br />
                 <a href="/CustomTooling">Custom Tooling Quotation</a>
                <br />--%>
            </div>
            <div class="col-sm-4">
                <h3>Quick Directory</h3>
                <hr />
                <a href="/ATW">Auto Tooling Wizard</a>
                <br />
                <a href="/MTW">Manual Tooling Wizard</a>
                <br />
                <a href="/BTMTW">BTM Tooling Wizard</a>
                <br />

                <h3>Machine Manuals</h3>
                <hr />
                <a href="/MachineManuals">Haeger Machine Manuals</a>
                <br />
                <a href="/PEMSERTER/PemserterManuals">PEMSERTER&reg; Machine Manuals</a>
                <br />
                <hr />


                <a href="/ServiceProcedures">Service Procedures</a>
                <br />
                
                <%--<a href="/DistributorEU/pdf/priceList/haeger-price-list-march-2018.pdf">Complete Price List March 2018</a>
                <br />
                <a href="/DistributorEU/pdf/priceList/haeger-short-price-list-march-2018.pdf">Short Price List March 2018</a>
                <br />--%>
                <a href="/CustomerEU/pdf/pricelists/Short-Price-List-EU-July-2018.pdf" target="_blank">Short Price List March 2018</a>
                <br />
                <a href="/pdf/toolingCatalogs/manual-tooling-catalog-021213.pdf" target="_blank">Manual Tooling Catalog</a>
                <br />
              
                <a href="/DistributorNA/pdf/toolingCatalogs/IMSTP-Tooling-Package.pdf" target="_blank">IMSTP Tooling Package List</a>
                <br />
                <br />
                <br />
                <hr />
                


            </div>
        </div>
    </div>






</asp:Content>
