﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018
{
    public partial class CustomTooling : System.Web.UI.Page
    {
        SqlConnection custToolConn = new SqlConnection(ConfigurationManager.ConnectionStrings["OptInConsentConnectionString"].ConnectionString);
        DateTime localDate = DateTime.Now;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;

            drpCTMachineType.Items.Add(new ListItem("Other", "-1"));

            lblFTypeList.Text = "( " + "<img src='Images/PEMlogo.png' height='30' width='30' />" + ", PSM, Hank, Kalei, Strux, Captive, Kerb, Konus, etc. - 25 to 300 samples may be required):";
        }

        public void UploadFiles()
        {
            DateTime localDate = DateTime.Now;

            string userName = txtName.Text;
            string date = localDate.ToLongDateString();

            string newDir = "D:\\Emails";
            Session["newDirectory"] = newDir;
            MakeDirectoryIfExists(newDir);

            if (FileUpload1.HasFile)
            {
                string fileExt1 =
                   System.IO.Path.GetExtension(FileUpload1.FileName);

                try
                {
                    FileUpload1.SaveAs(newDir + "\\" +
                       FileUpload1.FileName);
                    Session["newDirectory1"] = newDir + "\\" +
                       FileUpload1.FileName;
                    Label1.Text = "File name: " +
                        FileUpload1.PostedFile.FileName + "<br>" +
                        FileUpload1.PostedFile.ContentLength + " kb<br>" +
                        "Content type: " +
                        FileUpload1.PostedFile.ContentType + " kb<br>" +
                        "Upload Successful";
                }
                catch (Exception ex)
                {
                    Label1.Text = "ERROR: " + ex.Message.ToString();
                }
            }

            if (FileUpload2.HasFile)
            {
                string fileExt2 =
                   System.IO.Path.GetExtension(FileUpload2.FileName);

                try
                {
                    FileUpload2.SaveAs(newDir + "\\" +
                       FileUpload2.FileName);
                    Session["newDirectory2"] = newDir + "\\" +
                      FileUpload2.FileName;
                    Label2.Text = "File name: " +
                        FileUpload2.PostedFile.FileName + "<br>" +
                        FileUpload2.PostedFile.ContentLength + " kb<br>" +
                        "Content type: " +
                        FileUpload2.PostedFile.ContentType + " kb<br>" +
                        "Upload Successful";
                }
                catch (Exception ex)
                {
                    Label2.Text = "ERROR: " + ex.Message.ToString();
                }
            }

            if (FileUpload3.HasFile)
            {
                string fileExt3 =
                   System.IO.Path.GetExtension(FileUpload3.FileName);

                try
                {
                    FileUpload3.SaveAs(newDir + "\\" +
                         FileUpload3.FileName);
                    Session["newDirectory3"] = newDir + "\\" +
                      FileUpload3.FileName;
                    Label3.Text = "File name: " +
                        FileUpload3.PostedFile.FileName + "<br>" +
                        FileUpload3.PostedFile.ContentLength + " kb<br>" +
                        "Content type: " +
                        FileUpload3.PostedFile.ContentType + " kb<br>" +
                        "Upload Successful";
                }
                catch (Exception ex)
                {
                    Label3.Text = "ERROR: " + ex.Message.ToString();
                }
            }

            if (FileUpload4.HasFile)
            {
                string fileExt4 =
                   System.IO.Path.GetExtension(FileUpload4.FileName);

                try
                {
                    FileUpload4.SaveAs(newDir + "\\" +
                      FileUpload4.FileName);
                    Session["newDirectory4"] = newDir + "\\" +
                      FileUpload4.FileName;
                    Label4.Text = "File name: " +
                        FileUpload4.PostedFile.FileName + "<br>" +
                        FileUpload4.PostedFile.ContentLength + " kb<br>" +
                        "Content type: " +
                        FileUpload4.PostedFile.ContentType + " kb<br>" +
                        "Upload Successful";
                }
                catch (Exception ex)
                {
                    Label4.Text = "ERROR: " + ex.Message.ToString();
                }
            }
        } // End Button1_Click

        private void MakeDirectoryIfExists(string NewDirectory)
        {
            try
            {
                // Check if directory exists
                if (!Directory.Exists(NewDirectory))
                {
                    // Create the directory.
                    Directory.CreateDirectory(NewDirectory);
                }
            }
            catch (IOException _ex)
            {
                Response.Write(_ex.Message);
            }

        } // End MakeDirectoryIfExists

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DateTime localDate = DateTime.Now;
            string date = localDate.ToLongDateString();

            var msg = "The consent box must be checked or we cannot process your request.";

            if (!gdprConsentBox.Checked)
            {
                lblConsentWrn.Visible = true;

                Response.Write("<script>alert('" + msg + "')</script>");

                return;
            }


            // *************** RFQ Form Variables ***************
            string name = txtName.Text.Trim();
            string distributor = txtDistributor.Text.Trim();
            string salesPerson = txtSalesPerson.Text.Trim();
            string email = txtEmail.Text.Trim();
            string customer = txtCustomer.Text.Trim();
            string contactName = txtContactName.Text.Trim();
            string machineType = drpCTMachineType.SelectedItem.Text;
            
            

            // *************** INSERT DATABASE ***************

            custToolConn.Open();

            // Query and commands to retrieve max tracking number
            string trackNumQuery = "SELECT MAX(trackNum) FROM CustToolingConsent";
            SqlCommand trackNumCommand = new SqlCommand(trackNumQuery, custToolConn);
            string trackNum = "";
            using (SqlDataReader tn = trackNumCommand.ExecuteReader())
            {
                while (tn.Read())
                {
                    trackNum = tn[0].ToString();
                }
            }

            // Add one to max tracking number and creating new _projID
            int nextIDnum = (Int32.Parse(trackNum)) + 1;
            string rfqID = "rfq" + nextIDnum.ToString();

            string insertQuery =
            "INSERT INTO [dbo].[CustToolingConsent] " +
            "(_rfqID, name, distributor, salesPerson, email, customer, contactName, machineType, gdprConsent, dateCreated) " +
            "VALUES (@rfqID, @name, @distributor, @salesPerson, @email, @customer, @contactName, @machineType, @gdprConsent, @dateCreated)";

            SqlCommand insertCommand = new SqlCommand(insertQuery, custToolConn);
            insertCommand.Parameters.AddWithValue("@rfqID", rfqID);
            insertCommand.Parameters.AddWithValue("@name", name);
            insertCommand.Parameters.AddWithValue("@distributor", distributor);
            insertCommand.Parameters.AddWithValue("@salesPerson", salesPerson);
            insertCommand.Parameters.AddWithValue("@email", email);
            insertCommand.Parameters.AddWithValue("@customer", customer);
            insertCommand.Parameters.AddWithValue("@contactName", contactName);
            insertCommand.Parameters.AddWithValue("@machineType", machineType);
            insertCommand.Parameters.AddWithValue("@gdprConsent", 1);
            insertCommand.Parameters.AddWithValue("@dateCreated", localDate);
           
            insertCommand.ExecuteNonQuery();
            custToolConn.Close();


            // *************** EMAIL ***************

            var consentConfirm = "";
            var customerEmail = txtEmail.Text.Trim();
            var customerEmail2 = txtEmail2.Text.Trim();
            var customerEmail3 = txtEmail3.Text.Trim();

            if (gdprConsentBox.Checked)
            {
                consentConfirm = "Confirmed";
            }

            //Fetching Email Body Text from EmailTemplate File.
            string FilePath = @"D:/EmailTemplates/CustomToolingEmail.html";
            FileStream sourceStream = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamReader str = new StreamReader(sourceStream);
            string MailText = str.ReadToEnd();
            str.Close();

            //Repalce variables
            MailText = MailText.Replace("[currentDate]", date);
            MailText = MailText.Replace("[newName]", txtName.Text.Trim());
            MailText = MailText.Replace("[newDistributor]", txtDistributor.Text.Trim());
            MailText = MailText.Replace("[newSalesperson]", txtSalesPerson.Text.Trim());
            MailText = MailText.Replace("[newEmail]", customerEmail);

            if (!String.IsNullOrEmpty(customerEmail2))
            {
                MailText = MailText.Replace("[newEmail2]", customerEmail2);
            }
            else
            {
                MailText = MailText.Replace("[newEmail2]","");
            }

            if (!String.IsNullOrEmpty(customerEmail3))
            {
                MailText = MailText.Replace("[newEmail3]", customerEmail3);

            }
            else
            {
                MailText = MailText.Replace("[newEmail3]", "");
            }

            // Replace Enviroment.Newline with <br> for multiline textboxes.

            string strDescTools = txtDescTools.Text.Replace(Environment.NewLine, "<br>");
            string strDescFast = txtDescripFast.Text.Replace(Environment.NewLine, "<br>");
            string strDescPart = txtPartDescr.Text.Replace(Environment.NewLine, "<br>");
            string strDescPartMat = txtPartMaterial.Text.Replace(Environment.NewLine, "<br>");
            

            MailText = MailText.Replace("[newCustomer]", txtCustomer.Text.Trim());
            MailText = MailText.Replace("[newContactName]", txtContactName.Text.Trim());
            MailText = MailText.Replace("[newPhone]", txtPhone.Text.Trim());
            MailText = MailText.Replace("[newExt]", txtExt.Text.Trim());
            MailText = MailText.Replace("[newDescription]", strDescTools.Trim());

            MailText = MailText.Replace("[newOrderNumber]", txtOrigOrderNum.Text.Trim());
            MailText = MailText.Replace("[newModel]", drpCTMachineType.SelectedItem.Text);
            MailText = MailText.Replace("[newModelOther]", txtOtherMachine.Text.Trim());
            MailText = MailText.Replace("[newConsent]", consentConfirm);

            if (rblInstalledQ.SelectedItem != null)
            {
                MailText = MailText.Replace("[newInstallYesNo]", rblInstalledQ.SelectedItem.Text);
            }
            else
            {
                MailText = MailText.Replace("[newInstallYesNo]", "Install not selected");
            }

            if (rblRobotReadyQ != null)
            {
                MailText = MailText.Replace("[newRobotReadyYesNo]", rblRobotReadyQ.SelectedItem.Text);

            }
            else
            {
                MailText = MailText.Replace("[newRobotReadyYesNo]", "Robot Ready not Selected");

            }

            if (rblMASSize.SelectedItem != null)
            {
                MailText = MailText.Replace("[newMASSize]", rblMASSize.SelectedItem.Text);

            }
            else
            {
                MailText = MailText.Replace("[newMASSize]", "No MAS Selected.");

            }

            MailText = MailText.Replace("[newFastManu]", txtFastManu.Text.Trim());
            MailText = MailText.Replace("[newFastPartNum]", txtFastNum.Text.Trim());
            MailText = MailText.Replace("[newFastDescrip]", strDescFast.Trim());
            MailText = MailText.Replace("[newPartDescription]", strDescPart.Trim());
            MailText = MailText.Replace("[newPartMaterial]", strDescPartMat.Trim());

            // Session variable to pass email information to confirmation page.
            Session["emailConfirm"] = MailText;


            string subject = txtContactName.Text + " - " + drpCTMachineType.SelectedItem.Text;

            
            try
            {
                UploadFiles();
                var targetDirectory = Session["newDirectory"].ToString();
                var emailStorage = @targetDirectory;
                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");

                _mailmsg.From = address;

                // Add your email address to the recipients
                //_mailmsg.To.Add("jloya@haeger.com");
                _mailmsg.To.Add("customtooling@haeger.com");
                _mailmsg.CC.Add(customerEmail);

                if (!String.IsNullOrEmpty(customerEmail2))
                {
                    _mailmsg.CC.Add(customerEmail2);
                }

                if (!String.IsNullOrEmpty(customerEmail3))
                {
                    _mailmsg.CC.Add(customerEmail3);

                }

                
                _mailmsg.Bcc.Add("regeah811@gmail.com");

                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                _mailmsg.Body = MailText;


                // Attach file
                Attachment data1, data2, data3, data4;

                // Declare file location
                if (!string.IsNullOrEmpty((string)Session["newDirectory1"]))
                {
                    var fileToAttach1 = Session["newDirectory1"].ToString();
                    data1 = new Attachment(fileToAttach1);
                    _mailmsg.Attachments.Add(data1);
                }

                if (!string.IsNullOrEmpty((string)Session["newDirectory2"]))
                {
                    var fileToAttach2 = Session["newDirectory2"].ToString();
                    data2 = new Attachment(fileToAttach2);
                    _mailmsg.Attachments.Add(data2);
                }

                if (!string.IsNullOrEmpty((string)Session["newDirectory3"]))
                {
                    var fileToAttach3 = Session["newDirectory3"].ToString();
                    data3 = new Attachment(fileToAttach3);
                    _mailmsg.Attachments.Add(data3);
                }

                if (!string.IsNullOrEmpty((string)Session["newDirectory4"]))
                {
                    var fileToAttach4 = Session["newDirectory4"].ToString();
                    data4 = new Attachment(fileToAttach4);
                    _mailmsg.Attachments.Add(data4);
                }

                // Configure an SmtpClient to send the mail.
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;

                //_smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                //_smtp.PickupDirectoryLocation = @targetDirectory;

                // Send the msg.
                _smtp.Send(_mailmsg);

                // Display some feedback to the user to let them know it was sent.
                //lblResult.Text = "Your message was sent!";

                clearControls();

                Response.Redirect("/SubmitSucess");

            }
            catch (Exception)
            {
                // If the message failed at some point, let the user know.
                lblResult.Text = "Your message failed to send, please try again.";

            }



        }

        private void clearControls()
        {
            txtName.Text = "";
            txtDistributor.Text = "";
            txtSalesPerson.Text = "";
            txtEmail.Text = "";
            txtCustomer.Text = "";
            txtContactName.Text = "";
            txtPhone.Text = "";
            txtDescTools.Text = "";
            rblInstalledQ.ClearSelection();
            rblRobotReadyQ.ClearSelection();
            txtOrigOrderNum.Text = "";
            drpCTMachineType.SelectedIndex = 0;
            txtOtherMachine.Text = "";
            rblMASSize.ClearSelection();
            txtFastManu.Text = "";
            txtFastNum.Text = "";
            txtDescripFast.Text = "";
            txtPartDescr.Text = "";
            txtPartMaterial.Text = "";
            Label1.Text = "";
            Label2.Text = "";
            Label3.Text = "";
            Label4.Text = "";



        }
    }
}