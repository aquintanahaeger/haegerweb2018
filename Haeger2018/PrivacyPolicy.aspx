﻿<%@ Page Title="Privacy Policy" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.cs" Inherits="Haeger2018.PrivacyPolicy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }
        p,li
        {
            font-size:16px;
        }
        
            
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Privacy Policy - May 2018</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <h3><b>Introduction</b></h3>
                <p>
                    Haeger Incorporated&reg, a <span1> PennEngineering&reg</span1> Company (“<b>Company</b>” or “<b>We</b>”) respect your privacy and are committed to protecting it through our 
                    compliance with this policy.
                    
                </p>
                <p>
                    This policy describes the types of information we may collect from you or that you may provide when you visit the website <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl=" https://www.haeger.com/">https://www.haeger.com/</asp:HyperLink>  
                    (our “<b>Website</b>”) and our practices for collecting, using, maintaining, protecting, and disclosing that information.
                </p>
                <p>
                    This policy applies to information we collect:
                    <ul>
                        <li>On this Website.</li>
                        <li>In email, text, and other electronic messages between you and this Website.</li>
                        <li>When you interact with our advertising and applications on third-party websites and services, if those applications or advertising include links to this policy.</li>
                    </ul>
                </p>
                <p>
                    It <b><i>does not apply</i></b> to information collected by:
                    <ul>
                        <li>us offline or through any other means, including on any other website operated by Company or any third party (including our affiliates and subsidiaries); or</li>
                        <li>Any third party (including our affiliates and subsidiaries), including through any application or content (including advertising) that may link to or be accessible 
                            from or on the Website.</li>
                    </ul>
                </p>
                <p>
                    Please read this policy carefully to understand our policies and practices regarding your information and how we will treat it. 
                    If you do not agree with our policies and practices, your choice is not to use our Website. By accessing or using this Website, you agree to this privacy policy. 
                    This policy may change from time to time (see <a href="#privacyChanges" style="text-decoration:none;font-style:italic;">Changes to Our Privacy Policy</a>). Your continued use of this Website after we make changes is deemed to be acceptance of those changes, 
                    so please check the policy periodically for updates.
                </p>

                <h3><b>Children Under the Age of 13</b></h3>
                <p>
                    Our Website is not intended for children under 13 years of age. No one under age 13 may provide any information to or on the Website. 
                    We do not knowingly collect personal information from children under 13. If you are under 13, do not use or provide any information on this Website or on or through any of its features. 
                    If we learn we have collected or received personal information from a child under 13 without verification of parental consent, we will delete that information. 
                    If you believe we might have any information from or about a child under 13, please contact please contact Customer Care at <a href="mailto:webadmin@haeger.com">webadmin@haeger.com</a>.
                </p>

                <h3><b>Information We Collect About You and How We Collect It</b></h3>
                <p>
                    We collect several types of information from and about users of our Website, including information:
                    <ul>
                        <li>by which you may be personally identified, such as name, title, company name, postal address, e-mail address, telephone number, and fax number (“<b>personal information</b>“);</li>
                        <li>about your internet connection, the equipment you use to access our Website and usage details.</li>
                    </ul>
                    We collect this information:
                    <ul>
                        <li>Directly from you when you provide it to us.</li>
                        <li>Automatically as you navigate through the site. Information collected automatically may include usage details, IP addresses, and information collected through cookies.</li>
                    </ul>
                </p>

                <h3><b>Information You Provide to Us.</b></h3>
                <p>
                    The information we collect on or through our Website may include:
                    <ul>
                        <li>Information that you provide by filling in forms on our Website. This includes information provided at the time of registering to use our Website or requesting further services. 
                            We may also ask you for information when you report a problem with our Website.</li>
                        <li>Records and copies of your correspondence (including email addresses), if you contact us.</li>
                        <li>Details of transactions you carry out through our Website and of the fulfillment of your orders. You may be required to provide financial information before placing an order 
                            through our Website, such as VAT number or bank account number.</li>
                        <li>Your search queries on the Website.</li>
                        <li>Newsletter signup.</li>
                    </ul>
                </p>
                <h3><b>Information We Collect Through Automatic Data Collection Technologies.</b></h3>
                <p>
                    As you navigate through and interact with our Website, we may use automatic data collection technologies to collect certain information about your equipment, 
                    browsing actions, and patterns, including:
                    <ul>
                        <li>Details of your visits to our Website, including traffic data, location data, logs, and other communication data and the resources that you access and use on the Website.</li>
                        <li>Information about your computer and internet connection, including your IP address, operating system, and browser type.</li>
                    </ul>
                </p>
                <p>
                    The information we collect automatically may include personal information or we may maintain it or associate it with personal information we collect in other ways or receive from 
                    third parties. It helps us to improve our Website and to deliver a better and more personalized service, including by enabling us to:
                    <ul>
                        <li>Estimate our audience size and usage patterns.</li>
                        <li>Store information about your preferences, allowing us to customize our Website according to your individual interests.</li>
                        <li>Speed up your searches.</li>
                        <li>Recognize you when you return to our Website.</li>
                    </ul>
                </p>

                <p>
                    The technologies we use for this automatic data collection may include:
                    <ul>
                        <li><b>Cookies (or browser cookies)</b>. A cookie is a small file placed on the hard drive of your computer. You may refuse to accept browser cookies by activating the appropriate setting on your browser. However, if you select this setting you may be unable to access certain parts of our Website. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you direct your browser to our Website.</li>
                        <li><b>Flash Cookies</b>. Certain features of our Website may use local stored objects (or Flash cookies) to collect and store information about your preferences and navigation to, 
                            from, and on our Website. Flash cookies are not managed by the same browser settings as are used for browser cookies. 
                            For information about managing your privacy and security settings for Flash cookies, see <a href="#privacyChoices" style="text-decoration:none;font-style:italic;">Choices About How We Use and Disclose Your Information</a>.</li>
                    </ul>
                </p>

                <h3><b>How We Use Your Information</b></h3>
                <p>
                    <ul>
                        <li>To present our Website and its contents to you.</li>
                        <li>To provide you with information, products, or services that you request from us.</li>
                        <li>To fulfill any other purpose for which you provide it.</li>
                        <li>To carry out our obligations and enforce our rights arising from any contracts entered into between you and us, including for billing and collection.</li>
                        <li>To notify you about changes to our Website or any products or services we offer or provide though it.</li>
                        <li>In any other way we may describe when you provide the information.</li>
                        <li>For any other purpose with your consent.</li>
                    </ul>
                </p>
                    
                <p>
                    We may also use your information to contact you about goods and services that may be of interest to you. If you do not want us to use this information in this way, 
                    please check “unsubscribe” on any of our mailings or click <asp:LinkButton ID="lbtnRemove" runat="server" OnClick="lbtnRemove_Click">here</asp:LinkButton> to send “REMOVE” email. 
                    For more information, see <a href="#privacyChoices" style="text-decoration:none;font-style:italic;">Choices About How We Use and Disclose Your Information</a>.
                </p>

                <h3><b>Equipment</b></h3>
                <p>
                    Haeger employees have access to equipment (both fixed workstations and laptops) that allows them to access your personal data either remotely or at the office. 
                    Haeger uses a secure network.

                </p>

                <h3><b>Disclosure of Your Information</b></h3>
                <p>
                    We may disclose aggregated information about our users, and information that does not identify any individual, without restriction.<br />
                    We may disclose personal information that we collect or you provide as described in this privacy policy:
                    <ul>
                        <li>To our subsidiaries and affiliates.</li>
                        <li>To contractors, service providers, and other third parties we use to support our business and who are bound by contractual obligations to keep personal information confidential and use it only for the purposes for which we disclose it to them.</li>
                        <li>To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all of Haeger's assets, whether as a going concern or as part of bankruptcy, liquidation, or similar proceeding, in which personal information held by Haeger about our Website users is among the assets transferred.</li>
                        <li>To fulfill the purpose for which you provide it.</li>
                        <li>For any other purpose disclosed by us when you provide the information.</li>
                        <li>With your consent.</li>
                    </ul>
                </p>
                
                <p>
                    We may also disclose your personal information:
                    <ul>
                        <li>To comply with any court order, law, or legal process, including responding to any government or regulatory request.</li>
                        <li>To comply with other agreements, including for billing and collection purposes.</li>
                        <li>If we believe disclosure is necessary or appropriate to protect the rights, property, or safety of Haeger, our customers, or others.</li>
                    </ul>
                </p>
                <a name="privacyChoices"></a>
                <h3><b>Choices about How We Use and Disclose Your Information</b></h3>
                <p>
                    We strive to provide you with choices regarding the personal information you provide to us. We have created mechanisms to provide you with the following control over your information:
                    <ul>
                        <li>Tracking Technologies and Advertising. You can set your browser to refuse all or some browser cookies, or to alert you when cookies are being sent. 
                            To learn how you can manage your Flash cookie settings, visit the Flash player settings page on Adobe’s website. If you disable or refuse cookies, 
                            please note that some parts of this site may then be inaccessible or not function properly.</li>
                        <li>Promotional Offers from the Company. If you do not wish to have your contact information used by the Company to promote our own or third parties’ products or services, 
                            you can opt out by checking the relevant box located on the form on which we collect your data (the order/registration form) or by through your “My Account” page. 
                            At any time, if you wish to opt out of our mailing list, you may do so by selecting the “Unsubscribe” option or by sending us an email stating your request 
                            to <a href="mailto:webadmin@haeger.com">webadmin@haeger.com</a>. If we have sent you a promotional email, you may send us a return email asking to be omitted from future email distributions. 
                            This opt out does not apply to information provided to the Company as a result of a product purchase, warranty registration, product service experience, or other transactions.</li>
                        <li>Do Not Track Signals. We do not track your information over time and across third party websites to provide targeted advertising and therefore do not respond to Do Not Track (DNT) signals.</li>
                    </ul>
                </p>

                <h3><b>Transferring Personal Data from the EU to the US</b></h3>
                <p>
                    We are headquartered in the United States. Information we collect from you will be processed in the United States. The United States has not sought nor received a finding of “adequacy” 
                    from the European Union under Article 45 of the GDPR. We collect and transfer to the U.S. personal data only: with your consent; to perform a contract with you; or to fulfill a 
                    compelling legitimate in a manner that does not outweigh your rights and freedoms. We endeavor to apply suitable safeguards to protect the privacy and security of your personal data 
                    and to use it only consistent with your relationship with us and the practices described in this Privacy Policy. We also minimize the risk to your rights and freedoms by not collecting 
                    or storing sensitive information about you.
                </p>

                <h3><b>Accessing and Correcting Your Information</b></h3>
                <p>
                    You can update your personal information by sending us an email at <a href="mailto:webadmin@haeger.com">webadmin@haeger.com</a> to request access to, correct, or delete any personal information that you have provided to us. 
                    We may not accommodate a request to change information if we believe the change would violate any law or legal requirement or cause the information to be incorrect.
                </p>

                <h3><b>Data Security</b></h3>
                <p>
                    We have implemented measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration, and disclosure. All information 
                    you provide to us is stored on our secure servers behind firewalls. Any payment transactions will be encrypted.<br />
                    The safety and security of your information also depends on you. Where we have given you (or where you have chosen) a password for access to certain parts of our Website, 
                    you are responsible for keeping this password confidential. We ask you not to share your password with anyone.<br />
                    Unfortunately, the transmission of information via the internet is not completely secure. Although we do our best to protect your personal information, we cannot guarantee the 
                    security of your personal information transmitted to our Website. Any transmission of personal information is at your own risk. We are not responsible for circumvention of any 
                    privacy settings or security measures contained on the Website.
                </p>
                <a name="privacyChanges"></a>
                <h3><b>Changes to Our Privacy Policy</b></h3>
                <p>
                    It is our policy to post any changes we make to our privacy policy on this page. If we make material changes to how we treat our users’ personal information, we will notify you by 
                    email to the primary email address specified in your account. The date the privacy policy was last revised is identified at the top of the page. You are responsible for ensuring 
                    we have an up-to-date active and deliverable email address for you, and for periodically visiting our Website and this privacy policy to check for any changes.
                </p>

                <h3><b>Contact Information</b></h3>
                <p>
                    To ask questions or comment about this privacy policy and our privacy practices, contact us at:
                    <ul style="list-style-type:none">
                        <li>Customer Care Department</li>
                        <li>Haeger</li>
                        <li>811 Wakefield Drive</li>
                        <li>Oakdale, CA 95361</li>
                        <li></li>
                        <li><a href="mailto:webadmin@haeger.com">webadmin@haeger.com</a></li>

                    </ul>

                </p>

            </div>
            

        </div>




    </div>

</asp:Content>
