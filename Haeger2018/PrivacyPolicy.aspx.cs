﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018
{
    public partial class PrivacyPolicy : System.Web.UI.Page
    {
        DateTime localDate = DateTime.Now;
        

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbtnRemove_Click(object sender, EventArgs e)
        {
            string date = localDate.ToLongDateString();
            // *************** RFQ Form Variables ***************
            string name = "";

            if (User.Identity.GetUserName() != "")
            {
                name = User.Identity.GetUserName().ToString();
            }
            else
            {
                string FilePathError = @"D:/EmailTemplates/RemoveRequestError.html";
                FileStream sourceStreamError = new FileStream(FilePathError, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                StreamReader strError = new StreamReader(sourceStreamError);
                string MailTextError = strError.ReadToEnd();
                // ********** Repalce variables **********
                MailTextError = MailTextError.Replace("[currentDate]", date);
                MailTextError = MailTextError.Replace("[error]", "");


                // Session variable to pass email information to confirmation page.
                Session["SuccessMessage"] = "An error has occured!";
                Session["emailConfirm"] = MailTextError;
                Response.Redirect("/SubmitSuccess.aspx");
            }

            

            //Fetching Email Body Text from EmailTemplate File.
            string FilePath = @"D:/EmailTemplates/RemoveRequest.html";
            FileStream sourceStream = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamReader str = new StreamReader(sourceStream);
            string MailText = str.ReadToEnd();
            // ********** Repalce variables **********
            MailText = MailText.Replace("[currentDate]", date);
            MailText = MailText.Replace("[removedUser]", name);

            // Session variable to pass email information to confirmation page.
            Session["SuccessMessage"] = "Your Email has been sent";
            Session["emailConfirm"] = MailText;

            string subject = "REMOVE";

            try
            {
                // Create the msg object to be sent
                MailMessage _mailmsg = new MailMessage();

                //Make TRUE because our body text is html
                _mailmsg.IsBodyHtml = true;

                // Configure the address we are sending the mail from
                MailAddress address = new MailAddress("webadmin@haeger.com");
                _mailmsg.From = address;

                //Add your email address to the recipients
                _mailmsg.To.Add("webadmin@haeger.com");
                //_mailmsg.CC.Add(name);
                
                //Set Subject
                _mailmsg.Subject = subject;

                //Set Body Text of Email
                _mailmsg.Body = MailText;

                // Configure an SmtpClient to send the mail.
                SmtpClient _smtp = new SmtpClient("smtp-mail.outlook.com");
                _smtp.Port = 587;
                _smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                _smtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("webadmin@haeger.com", "Crazycat75");
                _smtp.EnableSsl = true;
                _smtp.Credentials = credentials;

                // Send the msg.
                _smtp.Send(_mailmsg);

            }
            catch (Exception ex)
            {


                //Fetching Email Body Text from EmailTemplate File.
                string FilePathError = @"D:/EmailTemplates/RemoveRequestError.html";
                FileStream sourceStreamError = new FileStream(FilePathError, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                StreamReader strError = new StreamReader(sourceStreamError);
                string MailTextError = strError.ReadToEnd();
                // ********** Repalce variables **********
                MailTextError = MailTextError.Replace("[currentDate]", date);
                MailTextError = MailTextError.Replace("[error]", ex.ToString());


                // Session variable to pass email information to confirmation page.
                Session["SuccessMessage"] = "An error has occured1";
                Session["emailConfirm"] = MailTextError;
               

            }

            Response.Redirect("/SubmitSucess.aspx");

        }
    }
}