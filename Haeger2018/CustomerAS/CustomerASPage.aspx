﻿<%@ Page Title="Haeger Asia" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomerASPage.aspx.cs" Inherits="Haeger2018.DistributorEU.DistributorEUPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1{
            color:#0085ca;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Asia Customer Page</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <div class="row">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">
                <h3>Quick Directory</h3>
                <hr />
                <a href="/ATW">Auto Tooling Wizard</a>
                <br />
                <a href="/MTW">Manual Tooling Wizard</a>
                <br />
                <a href="/BTMTW">BTM Tooling Wizard</a>
                <br />
                <a href="/MachineManuals">Machine Manuals</a>
                <br />
                <a href="/ServiceProcedures">Service Procedures</a>
                <br />
                <a href="/SalesRequest">Request Sales</a>
                <br />
                <a href="/ServiceRequest">Request Service</a>
                <br />
                <a href="/RequestRMA">Request RMA</a>
                <br />
                 <a href="/CustomTooling">Custom Tooling Quotation</a>
                <br />
                <a href="/ProductAvailability">Part and Tooling Availability</a>
                <br />
                <a href="/pdf/toolingCatalogs/manual-tooling-021213.pdf" target="_blank">Manual Tooling Catalog</a>
                <br />
                
                <a href="/pdf/toolingCatalogs/824-OT-3-WITH-5-DIA-VAC-TIPS-Auto-Tooling-Charts-Oct2014.pdf">
                    824OT-3 w/ 1/2" Dia.Vacuum Tips Automatic Tooling Catalog
                </a>
                <br />
                <a href="/pdf/toolingCatalogs/IMSTP-Tooling-Package.pdf" target="_blank">IMSTP Tooling Package List</a>
                <br />
                <br />
                <br />
                <hr />
                


            </div>
        </div>
    </div>



</asp:Content>
