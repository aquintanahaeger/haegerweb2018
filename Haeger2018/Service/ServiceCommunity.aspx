﻿<%@ Page Title="Service Community" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ServiceCommunity.aspx.cs" Inherits="Haeger2018.Service.ServiceCommunity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
     <style>
        span1{
            color:#0085ca;
        }

        .distLinks{
            font-weight:bold;
            font-size:18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger Service Community</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <br />
        
        <br />
        <div class="row">
            <div class="col-sm-4">
                 <h3>Useful Pages</h3>
                <hr />
                 <asp:Button ID="btnInstallChk" CssClass="btn btn-primary" runat="server" Text="Install Checklists" OnClick="btnInstallChk_Click" Width="220px"/>
                &nbsp&nbsp
               <%-- <asp:Button ID="btnStdProp" CssClass="btn btn-primary" runat="server" Text="Standard Proposals" />--%>
            </div>
            <div class="col-sm-4">
                <h3>Useful Forms</h3>
                <hr />
                <a href="/SalesRequest">Request Sales</a>
                <br />
                <a href="/ServiceRequest">Request Service</a>
                <br />
                <a href="/RequestRMA">Request RMA</a>
                <br />
                 <a href="/CustomTooling">Custom Tooling Quotation</a>
                <br />
            </div>
            <div class="col-sm-4">
                <h3>Quick Directory</h3>
                <hr />
                <a href="/Service/pdf/Service-Report.pdf" target="_blank">Service Report (pdf)</a>
                <br />
                <a href="/Service/pdf/Service-Report.docx" target="_blank">Service Report (docx)</a>
                <br />
                <a href="/ATW">Auto Tooling Wizard</a>
                <br />
                <a href="/MTW">Manual Tooling Wizard</a>
                <br />
                <a href="/BTMTW">BTM Tooling Wizard</a>
                <br />
                <a href="/MachineManuals">Machine Manuals</a>
                <br />
                <a href="/ServiceProcedures">Service Procedures</a>
                <br />
                <a href="/ProductAvailability">Product Search</a>
                <br />

               <%-- <a href="/DistributorNA/pdf/priceList/haeger-price-list-march-2018.pdf" target="_blank">Complete Price List March 2018</a>
                <br />
                <a href="/DistributorNA/pdf/priceList/haeger-short-price-list-march-2018.pdf" target="_blank">Short Price List March 2018</a>
                <br />--%>
                <%--<a href="/pdf/toolingCatalogs/manual-tooling-021213.pdf" target="_blank">Manual Tooling Catalog</a>
                <br />--%>
                <%--<a href="/pdf/toolingCatalogs/Auto-Tooling-Charts-JAN-2018-With-Pricing.pdf" target="_blank">Auto Tooling Catalog January 2018</a>
                <br />
                <a href="/DistributorNA/pdf/toolingCatalogs/824-OT-3-WITH-5-DIA-VAC-TIPS-Auto-Tooling-Charts-Oct2014.pdf" target="_blank">
                    824OT-3 w/ 1/2" Dia.Vacuum Tips Automatic Tooling Catalog
                </a>
                <br />
                <a href="/DistributorNA/pdf/toolingCatalogs/IMSTP-Tooling-Package.pdf" target="_blank">IMSTP Tooling Package List</a>--%>

                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />


            </div>
        </div>
    </div>

</asp:Content>
