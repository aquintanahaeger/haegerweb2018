﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="Haeger2018.ContactUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    
    <style>
       
        .haegerTile
        {
            height: 100%;
            width: 100%;
            justify-content:space-around;
        }
         span1{
            color:#0085ca;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="boxed animated fadeIn animation-delay-5">

    <section class="animated fadeIn margin-bottom-10">
  
        <div class="container-fluid">
            <div class="row margin-top-10 margin-bottom-20" style="border-bottom: 1px solid #ddd;">

                <div class="col-md-8 animated bounceInLeft animation-delay-5">
                    <h2 class="section-title margin-bottom-0" style="border-bottom: none;">Contact Haeger</h2>
                        <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                </div>
              <div class="col-md-4">
                    <div class="animated bounceInRight animation-delay-5 margin-bottom-10">
                        <a class="btn btn-primary btn-block" href="/DistLocator">
                            <div class="row row-vertical-align row-no-pad">
                                <div class="col-md-3"></div>
                                <div class="col-md-1">
                                    <i class="fa fa-search pull-right fa-2x"></i>
                                </div>
                                <div class="col-md-5">
                                    <div style="margin-left: 10px; text-align: left;">
                                        Need a Distributor?<br>
                                        Click here to search
                                    </div>
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row equal-height">
                <div class="col-md-6 hidden-sm hidden-xs">
                    <div class="mind-box mind-box-primary animated bounceInLeft animation-delay-7 cur-hover" style="pointer-events:none; min-height:350px">
                        <h4 class="mind-box-title">USA Headquarters</h4>
                        <i class="fa fa-globe" style="font-size:60px"  ></i>
                        <address class="margin-bottom-0">
                            <br />
                            811 Wakefield Drive<br/>
                            Oakdale, CA USA 95361<br/>
                            <div class="details">
                                <div class="margin-top-10">
                                    <i class="fa fa-phone" style="font-size:15px"></i>&nbsp;Toll Free:<span>+1 (800)-878-4343</span><br/>
                                    <i class="fa fa-phone" style="font-size:15px"></i>&nbsp;Phone:<span>+1 (209) 848-4000</span><br/>
<%--                                    <i class="fa fa-phone" style="font-size:15px"></i>&nbsp;Fax:<span>+1 (209) 847-6553</span><br/>--%>
                                </div>
                                <div class="margin-top-10" style="pointer-events:visible">
                                    <i class="fa fa-envelope" style="font-size:15px"></i>&nbsp;Sales: <a href="mailto:sales@haeger.com" class="dashed">sales@haeger.com</a><br/>
                                    <i class="fa fa-envelope" style="font-size:15px"></i>&nbsp;Service: <a href="mailto:service@haeger.com" class="dashed">service@haeger.com</a><br />
                                </div>
                            </div>
                        </address>
                        <br />
                    </div>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs">
                    <div class="mind-box mind-box-success animated bounceInDown animation-delay-5 cur-hover" style="pointer-events:none; min-height:350px">
                        <h4 class="mind-box-title">Europe Headquarters</h4>
                        <i class="fa fa-globe" style="font-size:60px"></i>
                        <address class="margin-bottom-0">
                            <br />
                            Textielstraat 18<br/>
                            7575 CA Oldenzaal<br/>
                            The Netherlands
                           <div class="details">
                                 <div class="margin-top-10">
                                    <i class="fa fa-phone" style="font-size:15px"></i>&nbsp;&nbsp;Phone:<span>+31 541 530 230</span><br/>
<%--                                    <i class="fa fa-phone" style="font-size:15px"></i>&nbsp;&nbsp;Fax:<span>+31 541 532 400</span><br/>--%>
                                </div>
                                <div class="margin-top-10" style="pointer-events:visible">
                                    <i class="fa fa-envelope" style="font-size:15px"></i>&nbsp;&nbsp;Contact: <a href="mailto:aasbroek@haeger.com" class="dashed">Angela Asbroek, Office Manager</a><br/>
                                    <i class="fa fa-envelope" style="font-size:15px"></i>&nbsp;&nbsp;Sales: <a href="mailto:europesales@haeger.com" class="dashed">europesales@haeger.com</a><br/>
                                    <i class="fa fa-envelope" style="font-size:15px"></i>&nbsp;Service: <a href="mailto:europeservice@haeger.com" class="dashed">europeservice@haeger.com</a>
                                </div>
                            </div>
                        </address>
                    </div>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs">
                    <div class="mind-box mind-box-info animated bounceInRight animation-delay-7 cur-hover" style="pointer-events:none; min-height:350px">
                        <h4 class="mind-box-title">Greater China Headquarters</h4>
                        <i class="fa fa-globe" style="font-size:60px"></i>
                        <address class="margin-bottom-0">
                            <br />
                            99 Middle Chenfeng Road<br />
                            Jiangsu Province, China<br />
                            215300<br />
                           <div class="details">
                                 <div class="margin-top-10">
                                    <i class="fa fa-phone" style="font-size:15px"></i>&nbsp;&nbsp;Phone:<span>+86 21 5695 4988</span><br>
<%--                                    <i class="fa fa-phone" style="font-size:15px"></i>&nbsp;&nbsp;Fax:<span>+86 21 5868 3988</span><br>--%>
                                </div>
                                <div class="margin-top-10" style="pointer-events:visible">
                                    <i class="fa fa-envelope" style="font-size:15px"></i>&nbsp;&nbsp;Contact: <a href="mailto:axue@haeger.com" class="dashed">Annie Xue, Commercial Manager</a><br>
                                    <i class="fa fa-envelope" style="font-size:15px"></i>&nbsp;&nbsp;Service: <a href="mailto:axue@haeger.com" class="dashed">axue@haeger.com</a>
                                </div>
                            </div>
                        </address>
                        <br />
                    </div>
                </div>
                 <div class="col-md-6 hidden-sm hidden-xs">
                    <div class="mind-box mind-box-warning animated bounceInRight animation-delay-7 cur-hover" style="pointer-events:none; min-height:350px">
                        <h4 class="mind-box-title">Asia Pacific Headquarters</h4>
                        <i class="fa fa-globe" style="font-size:60px"></i>
                        <address class="margin-bottom-0">
                            <br />
                            4 Kaki Bukit Avenue 1<br />
                            #04-05/06<br />
                            Singapore, 417939<br />
                           <div class="details">
                                 <div class="margin-top-10">
                                    <i class="fa fa-phone" style="font-size:15px"></i>&nbsp;&nbsp;Phone:<span>+65 67450660</span><br>
<%--                                    <i class="fa fa-phone" style="font-size:15px"></i>&nbsp;&nbsp;Fax:<span>+86 21 5868 3988</span><br>--%>
                                </div>
                                <div class="margin-top-10" style="pointer-events:visible">
                                    <i class="fa fa-envelope" style="font-size:15px"></i>&nbsp;&nbsp;Contact: <a href="mailto:michaelquek@pemnet.com" class="dashed">Michael Quek</a><br>
<%--                                    <i class="fa fa-envelope" style="font-size:15px"></i>&nbsp;&nbsp;Service: <a href="mailto:axue@haeger.com" class="dashed">axue@haeger.com</a>--%>
                                </div>
                            </div>
                        </address>
                        <br />
                    </div>
                </div>


            </div>
        </div>
    </section>



<%--Partners--%>
 <section>
        <div id="teams" class="container content">
            <div class="row">
                <div class="col-md-12">

                </div>
                <h4></h4>
                <div class="col-md-6">
                    <div class="row">

                        <%--Haley Meyer--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Haley Meyer</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Marketing & Service Coordinator&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="/images/haegerContactImages/Meyer-Haley.jpg"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">
                                        </p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>
                                        </div>
                                        <div>
                                            <img src="/images/flags/US.png" class="country-flag" />&nbsp;United States</div>
                                         <div>Phone: (209) 848-4000 ext. 5208</div>
                                        <div><i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;
                                            <a href="mailto:hmeyer@haeger.com" class="dashed">hmeyer@haeger.com</a>
                                        </div>
                                        <p>

                                        </p>
                                    </div>

                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">

                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--Alberto Martinez--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Alberto Martinez</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Service Engineer&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">

                                        </div>
                                        <img src="/images/haegerContactImages/Martinez-Alberto.jpg"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;" />
                                        <p class="no-margin-top">

                                        </p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right" />
                                        </div>
                                        <div>
                                            <img src="/images/flags/US.png" class="country-flag" />&nbsp;United States</div>
                                         <div>Phone: (209) 882-2654</div>
                                        <div><i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;
                                            <a href="mailto:amartinez@haeger.com" class="dashed">amartinez@haeger.com</a>
                                        </div>
                                        <p>

                                        </p>
                                    </div>

                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">

                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--Michael Splaine--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Michael Splaine</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right text-nowrap">North East Sales and Service Engineer
                                                    &nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Splaine-Mike.jpg"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;" />
                                        <p class="no-margin-top"></p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                             <img src="/images/ba_logo.png" class="pull-right" />
                                        </div>
                                        <div>
                                            <img src="/images/flags/US.png" class="country-flag" />&nbsp;United States</div>
                                        <div>Phone: (781) 504-2676</div>
                                       <div><i class="fa fa-envelope">
                                                   </i>&nbsp;&nbsp;&nbsp;<a href="mailto:msplaine@haeger.com" class="dashed">msplaine@haeger.com</a>
                                              </div>
                                         </div>
                                        <p></p>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <%--Joe Garcia--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Joe Garcia</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3>
                                                    <small class="pull-right text-nowrap">South Eastern Sales &amp; Service Manager&nbsp;&nbsp;&nbsp;</small>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Garcia-Joe.JPG"
                                                class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">
                                        </p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                    </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>
                                        </div>
                                        <div>
                                            <img src="/images/flags/US.png" class="country-flag" />&nbsp;United States</div>
                                        <div>Phone: (209) 556-7919</div>
                                        <div><i class="fa fa-envelope"></i>
                                            &nbsp;&nbsp;&nbsp;<a href="mailto:jgarcia@haeger.com" class="dashed">jgarcia@haeger.com</a>
                                        </div>
                                        <p>
                                        </p>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <%--Jack Kimberlin--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Jack Kimberlin</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3>
                                                    <small class="pull-right text-nowrap">Regional Sales & Service Manager&nbsp;&nbsp;&nbsp;</small>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Kimberlin-Jack.png"
                                                class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">
                                        </p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                    </a>
                                            </strong>
                                           <img src="/images/ba_logo.png" class="pull-right"/> 
                                        </div>
                                        <div>
                                            <img src="/images/flags/US.png" class="country-flag" />&nbsp;United States</div>
                                        <div>Phone: (360) 960-0089</div>
                                        <div><i class="fa fa-envelope"></i>
                                            &nbsp;&nbsp;&nbsp;<a href="mailto:jkimberlin@haeger.com" class="dashed">jkimberlin@haeger.com</a>
                                        </div>
                                        <p>
                                        </p>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <%--Sherry Dillard--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Sherry Dillard</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Inside Sales&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Dillard-Sherry.jpg"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">
                                        </p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>
                                        </div>
                                        <div>
                                            <img src="/images/flags/US.png" class="country-flag"/>&nbsp;United States</div>
                                        <div>Phone: 209-848-4000 ext. 5115</div>
                                        <div><i class="fa fa-envelope"></i>
                                            &nbsp;&nbsp;&nbsp;<a href="mailto:sdillard@haeger.com" class="dashed">sdillard@haeger.com</a>
                                        </div>
                                        <p>
                                        </p>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                         <%--Shannon Stanley--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Shannon Stanley</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Inside Sales&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Stanley-Shannon.png"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">
                                        </p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>
                                        </div>
                                        <div>
                                            <img src="/images/flags/US.png" class="country-flag"/>&nbsp;United States</div>
                                        <div>Phone: 209-848-4000 ext. 5115</div>
                                        <div><i class="fa fa-envelope"></i>
                                            &nbsp;&nbsp;&nbsp;<a href="mailto:sstanley@haeger.com" class="dashed">sstanley@haeger.com</a>
                                        </div>
                                        <p>
                                        </p>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--Rick Costa--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Rick Costa</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Inside Sales Manager&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Costa-Rick.JPG"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">
                                        </p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>
                                        </div>
                                        <div>
                                            <img src="/images/flags/US.png" class="country-flag"/>&nbsp;United States</div>
                                        <div>Phone: 209-848-4000 ext. 5115</div>
                                        <div><i class="fa fa-envelope"></i>
                                            &nbsp;&nbsp;&nbsp;<a href="mailto:rcosta@haeger.com" class="dashed">rcosta@haeger.com</a>
                                        </div>
                                        <p>
                                        </p>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        
                         <%--Ron Boggs--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Ron Boggs</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right text-nowrap">North American Sales &amp; Service Manager&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="/images/haegerContactImages/Boggs-Ron.JPG" class="member-image alignleft imageborder "
                                            style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">
                                        </p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong></a></strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>
                                        </div>
                                        <div>
                                            <img src="/images/flags/US.png" class="country-flag"/>&nbsp;United States</div>
                                        <div>Phone: (209) 765-6117</div>
                                        <div><i class="fa fa-envelope">
                                             </i>&nbsp;&nbsp;&nbsp;<a href="mailto:ronboggs@haeger.com" class="dashed">ronboggs@haeger.com</a>
                                        </div>
                                        <p>
                                        </p>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <%--Andy Yang--%>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Andy Yang</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Sales Manager&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Yang-Andy.jpg"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top"></p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right" />
                                        </div>
                                        <div>
                                            <img src="/images/flags/CN.png" class="country-flag" />&nbsp;China</div>
                                        <div>Phone: +86 21 56954988</div>                                       
                                        <div>
                                            <i class="fa fa-envelope"></i>
                                            &nbsp;&nbsp;&nbsp;<a href="mailto:ayang@haeger.com" class="dashed">ayang@haeger.com</a>
                                        </div>
                                        </div>
                                        <p></p>                               
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">                                            
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                         <%--Annie Xue--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Annie Xue</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Business Development Manager&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Xue-Annie.jpg"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top"></p>
                                        <div>
                                           <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>                                      
                                        <div>
                                            <img src="/images/flags/CN.png" class="country-flag"/>&nbsp;China</div>
                                        </div>
                                    <div>Phone: +86 21 56954988</div>
                                        <div>
                                            <i class="fa fa-envelope"></i>
                                            &nbsp;&nbsp;&nbsp;<a href="mailto:AXue@haeger.com" class="dashed">axue@haeger.com</a>
                                        </div>
                                        <p></p>
                                     </div>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                         <%--Gavin Zhu--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Gavin Zhu</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Sales Manager&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Zhu-Gavin.jpg"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top"></p>
                                        <div>
                                           <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>                                      
                                        <div>
                                            <img src="/images/flags/CN.png" class="country-flag"/>&nbsp;China</div>
                                        </div>
                                    <div>Phone: +86 21 56954988</div>
                                        <div>
                                            <i class="fa fa-envelope"></i>
                                            &nbsp;&nbsp;&nbsp;<a href="mailto:GZhu@haeger.com" class="dashed">gzhu@haeger.com</a>
                                        </div>
                                        <p></p>
                                     </div>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        <%--Jason Song--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Jason Song</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Service Supervisor&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Song-Jason.jpg"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top"></p>
                                        <div>
                                           <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>                                      
                                        <div>
                                            <img src="/images/flags/CN.png" class="country-flag"/>&nbsp;China</div>
                                        </div>
                                    <div>Phone: +86 21 56954988</div>
                                        <div>
                                            <i class="fa fa-envelope"></i>
                                            &nbsp;&nbsp;&nbsp;<a href="mailto:JSong@haeger.com" class="dashed">jsong@haeger.com</a>
                                        </div>
                                        <p></p>
                                     </div>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>

                         <%--Rob Kelder--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Rob Kelder</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Service &amp; Engineering Manager&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Kelder-Rob.JPG"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">
                                        </p>
                                        <div>
                                           <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>
                                        <div>
                                            <img src="/images/flags/NL.png" class="country-flag" />&nbsp;Netherlands</div>
                                        </div>
                                        <div>Phone: 0031-541-530230</div>
                                        <div><i class="fa fa-envelope">
                                             </i>&nbsp;&nbsp;&nbsp;<a href="mailto:rkelder@haeger.com" class="dashed">rkelder@haeger.com</a>
                                        </div>
                                    </div>
                                        <p>
                                        </p>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <%--Dennis Kuite--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Dennis Kuite</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Service Engineer&nbsp;&nbsp;&nbsp;</small></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <img src="images/haegerContactImages/Kuite-Dennis.png"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">
                                        </p>
                                        <div>
                                           <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>
                                        <div>
                                            <img src="/images/flags/DE.png" class="country-flag" />&nbsp;Germany</div>
                                        </div>
                                        <div>Phone: 0031-541-530230</div>
                                        <div><i class="fa fa-envelope">
                                             </i>&nbsp;&nbsp;&nbsp;<a href="mailto:dkuite@haeger.com" class="dashed">dkuite@haeger.com</a>
                                        </div>
                                    </div>
                                        <p>
                                        </p>
                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">
                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <%--Dennis Kuite--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Edwin Blanken</h3>

                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Sales Manager Europe&nbsp;&nbsp;&nbsp;</small></h3>

                                            </div>

                                        </div>
                                        <div class="clearfix">

                                        </div>
                                        <img src="images/haegerContactImages/Blanken-Edwin.png"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">

                                        </p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>

                                        
                                        <div>
                                            <img src="/images/flags/NL.png" class="country-flag">&nbsp;Netherlands</div>

                                        </div>
                                        <div>Phone: 0031-541-530230</div>
                                        <div><i class="fa fa-envelope">

                                             </i>&nbsp;&nbsp;&nbsp;<a href="mailto:eblanken@haeger.com " class="dashed">eblanken@haeger.com </a>

                                        </div>
                                    </div>
                                        <p>

                                        </p>

                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">

                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">

                                        </div>

                                    </div>

                                </div>

                            </div>


                        <%--Angela Asbroek--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Angela Asbroek</h3>

                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">Office Manager&nbsp;&nbsp;&nbsp;</small></h3>

                                            </div>

                                        </div>
                                        <div class="clearfix">

                                        </div>
                                        <img src="images/haegerContactImages/Asbroek-Angela.png"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">

                                        </p>
                                        <div>
                                           <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>

                                        
                                        <div>
                                            <img src="/images/flags/NL.png" class="country-flag" />&nbsp;Netherlands</div>

                                        </div>
                                        <div>Phone: 0031-541-530230</div>
                                        <div><i class="fa fa-envelope">

                                             </i>&nbsp;&nbsp;&nbsp;<a href="mailto:aasbroek@haeger.com" class="dashed">aasbroek@haeger.com</a>

                                        </div>
                                    </div>
                                        <p>

                                        </p>

                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">

                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">

                                        </div>

                                    </div>

                                </div>

                            </div>


                         <%--Wouter Kleizen--%>
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Wouter Kleizen</h3>

                                            </div>
                                            <div class="col-md-6">
                                                <h3><small class="pull-right">President of Haeger Inc.&nbsp;&nbsp;&nbsp;</small></h3>

                                            </div>

                                        </div>
                                        <div class="clearfix">

                                        </div>
                                        <img src="images/haegerContactImages/Kleizen-Wouter.jpg"
                                            class="member-image alignleft imageborder " style="max-height:150px; max-width:150px;"/>
                                        <p class="no-margin-top">

                                        </p>
                                        <div>
                                           <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>
                                            <img src="/images/ba_logo.png" class="pull-right"/>

                                        
                                        <div>
                                            <img src="/images/flags/NL.png" class="country-flag" />&nbsp;Netherlands</div>

                                        </div>
                                        <div>Phone: 0031-541-530230</div>
                                        <div><i class="fa fa-envelope">

                                             </i>&nbsp;&nbsp;&nbsp;<a href="mailto:wkleizen@haeger.com" class="dashed">wkleizen@haeger.com</a>

                                        </div>
                                    </div>
                                        <p>

                                        </p>

                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">

                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
             </div>
               
            <div class="row">
                <div class="col-md-12">

                </div>
                <%--<div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default contact animated fadeInUp member">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="row" style="padding-bottom:10px;border-bottom: 1px solid #ddd;">
                                            <div class="col-md-6">
                                                <h3>Headquarters</h3>

                                            </div>
                                            <div class="col-md-12">
                                                <h3><small class="pull-right">Haeger Division Headquarters&nbsp;&nbsp;&nbsp;</small></h3>

                                            </div>

                                        </div>
                                        <div class="clearfix">

                                        </div>
                                        <p class="no-margin-top"></p>
                                        <div>
                                            <strong><a href="Default.aspx" class="dashed" target="_ext">
                                                <strong>Haeger</strong>
                                                </a>
                                            </strong>

                                        </div>
                                        <div>
                                            <img src="/images/flags/US.png" class="country-flag">&nbsp;United States</div>

                                        </div>
                                        <div>Phone: +1 (800)-878-4343</div>
                                        <div>811 Wakefield Drive</div>
                                        <div>Oakdale, California 95361</div>
                                        <div>
                                            <i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;<a href="mailto:info@haeger.com" class="dashed">info@haeger.com</a>

                                        </div>
                                        <p></p>

                                    </div>
                                    <div class="row row-align row-middle">
                                        <div class="col-md-4 col-center">

                                        </div>
                                        <div class="col-md-8 col-center hidden-xs quote">

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>--%>

                </div>

            </div>

        </div>

      </section>
        </div>

</asp:Content>
