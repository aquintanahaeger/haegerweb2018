﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.PartOutSalesNA
{
    public partial class ProjRegSum : System.Web.UI.Page
    {
        


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void grdProjSum_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["selectedItem"] = grdProjSum.SelectedDataKey.Values["column1"].ToString();
            Response.Redirect("/PartOutSalesNA/ProjRegDetail");
        }

        protected void btnBackMain_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartOutSalesNA/PartOutSalesNAPage");

        }

        protected void btnProjArchive_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartOutSalesNA/ProjRegArchive");
        }
    }
}