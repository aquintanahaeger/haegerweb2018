﻿<%@ Page Title="Oustide Sales - NA" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PartOutSalesNAPage.aspx.cs" Inherits="Haeger2018.PartOutSalesNA.PartOutSalesNAPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
     <style>
        span1{
            color:#0085ca;
        }

        .distLinks{
            font-weight:bold;
            font-size:18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container-fluid">    
        <div class="section-title">
            <h2 style="margin: 0;">Haeger North America Outside Sales Page</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-4 col-xs-12">
                 <h3>Useful Pages</h3>
                <hr />
                <asp:Button ID="btnProjRegSum" CssClass="btn btn-primary" runat="server" Text="Project Registration Summary" OnClick="btnProjRegSum_Click" Width="220px" />  <br /><br />
                <asp:Button ID="btnStdProp" CssClass="btn btn-primary" runat="server" Text="Standard Proposals" OnClick="btnStdProp_Click" Width="220px" /> <br /><br />
                <asp:Button ID="btnProjArchive" CssClass="btn btn-primary" runat="server" Text="Project Archive" Width="220px" OnClick="btnProjArchive_Click" />
            </div>
            <div class="col-sm-4">
                <h3>Useful Forms</h3>
                <hr />
                <a href="/DistributorNA/ProjReg">Project Registration</a>
                <br />
                <a href="/CustomerNA/PartInformationNA">Part and Tooling Availability</a>
                <br />
                <a href="/SalesRequest">Request Sales</a>
                <br />
                <a href="/ServiceRequest">Request Service</a>
                <br />
                <a href="/RequestRMA">Request RMA</a>
                <br />
                 <a href="/CustomTooling">Custom Tooling Quotation</a>
                <br />
            </div>
            <div class="col-sm-4">
                <h3>Quick Directory</h3>
                <hr />
                <a href="/ATW">Auto Tooling Wizard</a>
                <br />
                <a href="/MTW">Manual Tooling Wizard</a>
                <br />
                <a href="/BTMTW">BTM Tooling Wizard</a>
                <br />
                <a href="/MachineManuals">Machine Manuals</a>
                <br />
                <a href="/ServiceProcedures">Service Procedures</a>
                <br />
                <a href="/DistributorNA/pdf/priceList/haeger-price-list-march-2018.pdf" target="_blank">Complete Price List March 2018</a>
                <br />
                <a href="/DistributorNA/pdf/priceList/haeger-short-price-list-march-2018.pdf" target="_blank">Short Price List March 2018</a>
                <br />
                <a href="/pdf/toolingCatalogs/manual-tooling-catalog-021213.pdf" target="_blank">Manual Tooling Catalog</a>
                <br />
                <a href="/DistributorNA/pdf/toolingCatalogs/Auto-Tooling-Charts-JAN-2018-With-Pricing.pdf" target="_blank">Auto Tooling Catalog January 2018</a>
                <br />
                <a href="/DistributorNA/pdf/toolingCatalogs/824-OT-3-WITH-5-DIA-VAC-TIPS-Auto-Tooling-Charts-Oct2014.pdf" target="_blank">
                    824OT-3 w/ 1/2" Dia.Vacuum Tips Automatic Tooling Catalog
                </a>
                <br />
                <a href="/DistributorNA/pdf/toolingCatalogs/IMSTP-Tooling-Package.pdf" target="_blank">IMSTP Tooling Package List</a>




            </div>
        </div>
    </div>

</asp:Content>
