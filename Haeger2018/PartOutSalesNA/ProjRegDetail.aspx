﻿<%@ Page Title="Project Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjRegDetail.aspx.cs" Inherits="Haeger2018.PartOutSalesNA.ProjRegDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        span1
        {
            color:#0085ca;
        }

    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <div class="container-fluid">  
         
         <div class="section-title">
            <h2 style="margin: 0;">Haeger Project Registration Details Page</h2>
            <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
        </div>
         <br />
          <div class="row">
             <div class="col-xs-12">
                <asp:Button ID="btnBackMain" class="btn btn-primary" runat ="server" Text="Back to Main" OnClick="btnBackMain_Click" />
                <asp:Button ID="btnBackAll" class="btn btn-primary" runat ="server" Text="All Projects" OnClick="btnBackAll_Click" />
             </div>
         </div>



          <div class="row">
            <div class="col-xs-3">

            </div>
             <div class="col-sm-6 col-xs-12">
                 <asp:DetailsView ID="dtvProjRegDet" CssClass="table table-striped table-bordered table-condensed" runat="server" Height="100px" Width="500px" AutoGenerateEditButton="True" AutoGenerateRows="False" DataKeyNames="column1" DataSourceID="sqlProjRegDet">
                     <Fields>
                         <asp:BoundField DataField="column1" HeaderText="column1" ReadOnly="True" SortExpression="column1" Visible="false" />
                         <asp:BoundField DataField="company" HeaderText="Company" SortExpression="company" />
                         <asp:BoundField DataField="address" HeaderText="Address" SortExpression="address" />
                         <asp:BoundField DataField="city" HeaderText="City" SortExpression="city" />
                         <asp:BoundField DataField="state" HeaderText="State" SortExpression="state" />
                         <asp:BoundField DataField="zipCode" HeaderText="Zip Code" SortExpression="zipCode" />
                         <asp:BoundField DataField="country" HeaderText="Country" SortExpression="country" />
                         <asp:BoundField DataField="phone" HeaderText="phone" SortExpression="phone" />
                         <asp:BoundField DataField="fax" HeaderText="Fax" SortExpression="fax" />
                         <asp:BoundField DataField="contactName" HeaderText="Contact Name" SortExpression="contactName" />
                         <asp:BoundField DataField="contactEmail" HeaderText="Contact Email" SortExpression="contactEmail" />
                         <asp:BoundField DataField="contactPosition" HeaderText="Contact Position" SortExpression="contactPosition" />
                         <asp:BoundField DataField="contactPhone" HeaderText="Contact Phone" SortExpression="contactPhone" />
                         <asp:BoundField DataField="businessType" HeaderText="Business Type" SortExpression="businessType" />
                         <asp:BoundField DataField="industryType" HeaderText="Industry Type" SortExpression="industryType" />
                         <asp:BoundField DataField="salesPerson" HeaderText="Sales Person" SortExpression="salesPerson" />
                         <asp:BoundField DataField="distributorName" HeaderText="Distributor" SortExpression="distributorName" />
                         <asp:TemplateField HeaderText="Priority Month To Close" SortExpression="priorityMonthToClose">
                             <EditItemTemplate>
                                 <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="sqlPriorityDet" DataTextField="priorityMonthToClose" DataValueField="priorityMonthToClose" SelectedValue='<%# Bind("priorityMonthToClose") %>'>
                                 </asp:DropDownList>
                             </EditItemTemplate>
                             <InsertItemTemplate>
                                 <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("priorityMonthToClose") %>'></asp:TextBox>
                             </InsertItemTemplate>
                             <ItemTemplate>
                                 <asp:Label ID="Label1" runat="server" Text='<%# Bind("priorityMonthToClose") %>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:BoundField DataField="clinchPurchases" HeaderText="Clinch Purchases" SortExpression="clinchPurchases" />
                         <asp:BoundField DataField="percentFromPEM" HeaderText="Percent From PEM" SortExpression="percentFromPEM" />
                         <asp:BoundField DataField="typeOfOther" HeaderText="Type Of Other" SortExpression="typeOfOther" />
                         <asp:BoundField DataField="projectMachineType" HeaderText="Machine Type" SortExpression="projectMachineType" />
                     </Fields>
                 </asp:DetailsView>
                 <asp:SqlDataSource ID="sqlProjRegDet" runat="server" ConnectionString="<%$ ConnectionStrings:ProjRegConnectionString %>" 
                     OldValuesParameterFormatString="original_{0}" 
                     SelectCommand="SELECT [_projID] AS column1, [company], [address], [city], [state], [zipCode], [country], [phone], [fax], [contactName], [contactEmail], [contactPosition], [contactPhone], [businessType], [industryType], [salesPerson], [distributorName], [priorityMonthToClose], [clinchPurchases], [percentFromPEM], [typeOfOther], [dateCreated], [projectMachineType] FROM [ProjRegData] WHERE ([_projID] = @column1)" DeleteCommand="DELETE FROM [ProjRegData] WHERE [_projID] = @original_column1" InsertCommand="INSERT INTO [ProjRegData] ([_projID], [company], [address], [city], [state], [zipCode], [country], [phone], [fax], [contactName], [contactEmail], [contactPosition], [contactPhone], [businessType], [industryType], [salesPerson], [distributorName], [priorityMonthToClose], [clinchPurchases], [percentFromPEM], [typeOfOther], [dateCreated], [projectMachineType]) VALUES (@column1, @company, @address, @city, @state, @zipCode, @country, @phone, @fax, @contactName, @contactEmail, @contactPosition, @contactPhone, @businessType, @industryType, @salesPerson, @distributorName, @priorityMonthToClose, @clinchPurchases, @percentFromPEM, @typeOfOther, @dateCreated, @projectMachineType)" UpdateCommand="UPDATE [ProjRegData] SET [company] = @company, [address] = @address, [city] = @city, [state] = @state, [zipCode] = @zipCode, [country] = @country, [phone] = @phone, [fax] = @fax, [contactName] = @contactName, [contactEmail] = @contactEmail, [contactPosition] = @contactPosition, [contactPhone] = @contactPhone, [businessType] = @businessType, [industryType] = @industryType, [salesPerson] = @salesPerson, [distributorName] = @distributorName, [priorityMonthToClose] = @priorityMonthToClose, [clinchPurchases] = @clinchPurchases, [percentFromPEM] = @percentFromPEM, [typeOfOther] = @typeOfOther, [dateCreated] = @dateCreated, [projectMachineType] = @projectMachineType WHERE [_projID] = @original_column1">
                     <DeleteParameters>
                         <asp:Parameter Name="original_column1" Type="String" />
                     </DeleteParameters>
                     <InsertParameters>
                         <asp:Parameter Name="column1" Type="String" />
                         <asp:Parameter Name="company" Type="String" />
                         <asp:Parameter Name="address" Type="String" />
                         <asp:Parameter Name="city" Type="String" />
                         <asp:Parameter Name="state" Type="String" />
                         <asp:Parameter Name="zipCode" Type="String" />
                         <asp:Parameter Name="country" Type="String" />
                         <asp:Parameter Name="phone" Type="String" />
                         <asp:Parameter Name="fax" Type="String" />
                         <asp:Parameter Name="contactName" Type="String" />
                         <asp:Parameter Name="contactEmail" Type="String" />
                         <asp:Parameter Name="contactPosition" Type="String" />
                         <asp:Parameter Name="contactPhone" Type="String" />
                         <asp:Parameter Name="businessType" Type="String" />
                         <asp:Parameter Name="industryType" Type="String" />
                         <asp:Parameter Name="salesPerson" Type="String" />
                         <asp:Parameter Name="distributorName" Type="String" />
                         <asp:Parameter Name="priorityMonthToClose" Type="String" />
                         <asp:Parameter Name="clinchPurchases" Type="Decimal" />
                         <asp:Parameter Name="percentFromPEM" Type="Decimal" />
                         <asp:Parameter Name="typeOfOther" Type="String" />
                         <asp:Parameter Name="dateCreated" Type="DateTime" />
                         <asp:Parameter Name="projectMachineType" Type="String" />
                     </InsertParameters>
                     <SelectParameters>
                         <asp:SessionParameter Name="column1" SessionField="selectedItem" Type="String" />
                     </SelectParameters>
                     <UpdateParameters>
                         <asp:Parameter Name="company" Type="String" />
                         <asp:Parameter Name="address" Type="String" />
                         <asp:Parameter Name="city" Type="String" />
                         <asp:Parameter Name="state" Type="String" />
                         <asp:Parameter Name="zipCode" Type="String" />
                         <asp:Parameter Name="country" Type="String" />
                         <asp:Parameter Name="phone" Type="String" />
                         <asp:Parameter Name="fax" Type="String" />
                         <asp:Parameter Name="contactName" Type="String" />
                         <asp:Parameter Name="contactEmail" Type="String" />
                         <asp:Parameter Name="contactPosition" Type="String" />
                         <asp:Parameter Name="contactPhone" Type="String" />
                         <asp:Parameter Name="businessType" Type="String" />
                         <asp:Parameter Name="industryType" Type="String" />
                         <asp:Parameter Name="salesPerson" Type="String" />
                         <asp:Parameter Name="distributorName" Type="String" />
                         <asp:Parameter Name="priorityMonthToClose" Type="String" />
                         <asp:Parameter Name="clinchPurchases" Type="Decimal" />
                         <asp:Parameter Name="percentFromPEM" Type="Decimal" />
                         <asp:Parameter Name="typeOfOther" Type="String" />
                         <asp:Parameter Name="dateCreated" Type="DateTime" />
                         <asp:Parameter Name="projectMachineType" Type="String" />
                         <asp:Parameter Name="original_column1" Type="String" />
                     </UpdateParameters>
                 </asp:SqlDataSource>
                 <asp:SqlDataSource ID="sqlPriorityDet" runat="server" ConnectionString="<%$ ConnectionStrings:ProjRegConnectionString %>" SelectCommand="SELECT DISTINCT [priorityMonthToClose] FROM [ProjRegData] WHERE ([priorityMonthToClose] IS NOT NULL)"></asp:SqlDataSource>




            </div>
        </div>




    </div>



</asp:Content>
