﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Haeger2018.PartOutSalesNA
{
    public partial class PartOutSalesNAPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnProjRegSum_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartOutSalesNA/ProjRegSum");
        }

        protected void btnStdProp_Click(object sender, EventArgs e)
        {
            Response.Redirect("/DistributorNA/StandardProposalsUS");
        }

        protected void btnProjArchive_Click(object sender, EventArgs e)
        {
            Response.Redirect("/PartOutSalesNA/ProjRegArchive");
        }
    }
}