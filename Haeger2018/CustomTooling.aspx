﻿<%@ Page Title="Custom Tooling Quote" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomTooling.aspx.cs" Inherits="Haeger2018.CustomTooling" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    
    <style>
     
        .requiredRed
        {
            width:auto;
            text-align:right;
            color:red;
            font-size:14px;
            font-weight:bold;
        }

        h7
        {
            font:normal;
            color:black;
            font-size:18px;

        }

        .red
        {
            color:red;
        }

        .rbl input[type="radio"]
        {
           margin-left: 20px;
           margin-right: 1px;
        }

        .rbl2 input[type=radio]
        {
            margin-left:10px;
            margin-right:1px;
        }
         span1{
            color:#0085ca;
        }
         /* The container */
.container1 {
    display:block;
    position:center;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-size: 16px;
}

/* Hide the browser's default checkbox */
.container1 input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    
}

 .checkbox .btn, .checkbox-inline .btn {
    padding-left: 2em;
    min-width: 8em;
    }
    .checkbox label, .checkbox-inline label {
    text-align: left;
    padding-left: 0.5em;
    }
    .checkbox input[type="checkbox"]{
        float:none;
    }


/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: red;
    
}

.container1:hover {
    background-color:greenyellow;
}

/* On mouse-over, add a grey background color */
.container1:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container1 input:checked ~ .checkmark {
    background-color: #0085ca;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container1 input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container1 .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid ">
         <div class="section-title">
                    <h2 style="margin: 0;">Haeger Custom Tooling Request for Quotation Form</h2>
                    <h3 style="text-transform:initial; color:black; margin:0;">A <span1> PennEngineering&reg</span1> Company</h3>
                   
               </div>
            <br />
       
        <div class="row">
            <div class="col-sm-3 col-centered">
                <h7>Items in <h7 class="red">RED</h7> must be filled in</h7>
            </div>
        </div>
        <br />
        <div class="form-group">
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblName" runat="server" CssClass="requiredRed" Text="Label">Name:</asp:Label>
                </div>
                <div class="col-sm-6">  
                    <asp:TextBox ID="txtName" CssClass="form-control" runat="server" Width="50%"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="* Name Required" 
                                                   CssClass="requiredRed" ControlToValidate="txtName">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblDistributor" runat="server" CssClass="requiredRed" Text="Label">Distributor:</asp:Label>
                </div>
                <div class="col-sm-6">
                    <asp:TextBox CssClass="form-control" ID="txtDistributor" runat="server" Width="50%">

                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDistributor" runat="server" CssClass="requiredRed"
                                        ErrorMessage="* Distributor Name or Haeger Direct" ControlToValidate="txtDistributor">

                    </asp:RequiredFieldValidator>

                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblSalesPerson" runat="server" CssClass="requiredRed" Text="Label">Salesperson:</asp:Label>
                </div>
                <div class="col-sm-6">
                    <asp:TextBox ID="txtSalesPerson" CssClass="form-control" runat="server" Width="50%">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSalesPerson" runat="server" CssClass="requiredRed"
                                        ErrorMessage="* Salesperson Required" ControlToValidate="txtSalesPerson">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblEmail" runat="server" CssClass="requiredRed" Text="Label">Email Addresses:</asp:Label>
                </div>
                <div class="col-sm-6">
                    <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" Width="50%">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="requiredRed"
                                        ErrorMessage="* Email Required" ControlToValidate="txtEmail">
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server"
                                     ErrorMessage="  * Invalid Email Format" CssClass="requiredRed" ControlToValidate="txtEmail"
                                     SetFocusOnError="True"
                                     ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                   <%-- <asp:Label ID="Label5" runat="server" CssClass="requiredRed" Text="Label">Email Address:</asp:Label>--%>
                </div>
                <div class="col-sm-6">
                    <asp:TextBox ID="txtEmail2" CssClass="form-control" runat="server" Width="50%">
                    </asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="requiredRed"
                                        ErrorMessage="* Email Required" ControlToValidate="txtEmail">
                    </asp:RequiredFieldValidator>--%>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                     ErrorMessage="  * Invalid Email Format" CssClass="requiredRed" ControlToValidate="txtEmail2"
                                     SetFocusOnError="True"
                                     ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                   <%-- <asp:Label ID="Label5" runat="server" CssClass="requiredRed" Text="Label">Email Address:</asp:Label>--%>
                </div>
                <div class="col-sm-6">
                    <asp:TextBox ID="txtEmail3" CssClass="form-control" runat="server" Width="50%">
                    </asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="requiredRed"
                                        ErrorMessage="* Email Required" ControlToValidate="txtEmail">
                    </asp:RequiredFieldValidator>--%>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                     ErrorMessage="  * Invalid Email Format" CssClass="requiredRed" ControlToValidate="txtEmail3"
                                     SetFocusOnError="True"
                                     ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblCustomer" runat="server" CssClass="requiredRed" Text="Label">Customer:</asp:Label>
                </div>
                <div class="col-sm-6">
                    <asp:TextBox ID="txtCustomer" CssClass="form-control" runat="server" Width="50%">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCustomer" runat="server" CssClass="requiredRed"
                                        ErrorMessage="* Customer/Company Name Required" ControlToValidate="txtCustomer">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblContactName" runat="server" CssClass="requiredRed" Text="Label">Contact Name:</asp:Label>
                </div>
                <div class="col-sm-6">
                    <asp:TextBox ID="txtContactName" CssClass="form-control" runat="server" Width="50%">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvContactName" runat="server" CssClass="requiredRed"
                                        ErrorMessage="* Contact Name Required" ControlToValidate="txtContactName">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblPhone" runat="server" Text="Label">Phone Number:</asp:Label>
                </div>
                <div class="col-sm-6">
                    <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server" Width="50%">
                    </asp:TextBox>
                    <asp:RegularExpressionValidator ID="revPhone" runat="server" CssClass="requiredRed"
                        ErrorMessage="* Phone Number Format Invalid" ControlToValidate="txtPhone" SetFocusOnError="True"
                        ValidationExpression="^[0-9\- \/?:.\(\),\s]+$">
                    </asp:RegularExpressionValidator>
                </div>
               
            </div>
            <br />
            <div class="row">
                 <div class="col-sm-2">
                    <asp:Label ID="lblExt" runat="server" Text="">Ext.</asp:Label>
                </div>
                <div class="col-sm-2">
                    <asp:TextBox ID="txtExt" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
            </div>

            <br />
            <%--Description of tooling requirement--%>
            <div class="row">
                <div class="col-sm-10 ">
                    <asp:Label ID="lblDescriptionTooling" runat="server" CssClass="requiredRed" Text="Label">
                        Description of Tooling Requirements
                    </asp:Label>
                    <asp:Label ID="lblDescToolDetails" runat="server" Text="Label">
                            (Desired type of tooling - Automatic Top Feed, Automatic Bottom Feed, manual, J-Frame, etc.):
                    </asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-3">

                    <asp:TextBox ID="txtDescTools" CssClass="form-control" runat="server" TextMode="MultiLine" Columns="100" Rows="10"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-2">
                    <asp:RequiredFieldValidator ID="rfvDescTool" runat="server" CssClass="requiredRed"
                        ErrorMessage="          * Description Required" ControlToValidate="txtDescTools">
                    </asp:RequiredFieldValidator>
                </div>
            </div>

            <br />

            <%--Installed Machine Question--%>
            <div class="row">
                <div class="col-sm-4">
                    <asp:Label ID="lblInstalledQ" runat="server" CssClass="requiredRed" Text="Label">
                        Is this tooling for an installed machine?
                    </asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <asp:RadioButtonList ID="rblInstalledQ" runat="server" RepeatDirection="Horizontal" CssClass="rbl">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="rfbInstalledQ" runat="server" CssClass="requiredRed"
                        ErrorMessage="* Response Required" ControlToValidate="rblInstalledQ">
                    </asp:RequiredFieldValidator>
                </div>
            </div>

             <br />

            <%--Robot Ready Machine Question--%>
            <div class="row">
                <div class="col-sm-4">
                    <asp:Label ID="lblRobotReadyQ" runat="server" CssClass="requiredRed" Text="Label">
                        Is this tooling for a robot-ready machine?
                    </asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <asp:RadioButtonList ID="rblRobotReadyQ" runat="server" RepeatDirection="Horizontal" CssClass="rbl">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="rfvRobotReadyQ" runat="server" CssClass="requiredRed"
                        ErrorMessage="* Response Required" ControlToValidate="rblRobotReadyQ">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <br />
            <%--Original Order Number--%>
            <div class="row">
                <div class="col-sm-3">
                    <asp:Label ID="lblOrigOrderNum" runat="server" Text="Label">Original Order Number (for replacement parts):</asp:Label>
                </div>
                <div class="col-sm-6">
                    <asp:TextBox ID="txtOrigOrderNum" CssClass="form-control" runat="server" Width="50%">
                    </asp:TextBox>
                  
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblCTMachineType" runat="server" Text="Label" CssClass="requiredRed">
                        Haeger Model:
                    </asp:Label>
                </div>
                <div class="col-sm-4 col-sm-pull-1">
                    <asp:DropDownList ID="drpCTMachineType" class="btn btn-default dropdown-toggle notranslate" runat="server" AppendDataBoundItems="true"
                                        DataSourceID="sqlCTMachineType" 
                                        DataTextField="machineType" DataValueField="machineType">
                        <asp:ListItem Text="----Make a Selection----" Value="" />

                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlCTMachineType" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:MobileAppDataConnectionString %>" 
                        SelectCommand="SELECT DISTINCT [machineType] FROM [machineNameSetTypes]">

                    </asp:SqlDataSource>
                    <asp:RequiredFieldValidator ID="rfvCTMachineType" runat="server" CssClass="requiredRed"
                        ErrorMessage="* Haeger Model Required" ControlToValidate="drpCTMachineType" >
                    </asp:RequiredFieldValidator>
                </div> 
            </div>
            <br />
            <div class="row">
                <div class="col-sm-6">
                    <asp:Label ID="lblOtherMachine" runat="server" Text="Label">If Other:</asp:Label>
                    <asp:TextBox ID="txtOtherMachine" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="lblMASSize" runat="server" Text="Label">
                        MAS Size:
                    </asp:Label>
                </div>
                <div class="col-sm-4 col-sm-pull-1">
                    <asp:RadioButtonList ID="rblMASSize" runat="server" CssClass="rbl" RepeatDirection="Horizontal">
                        <asp:ListItem>MAS-350</asp:ListItem>
                        <asp:ListItem>14"</asp:ListItem>
                        <asp:ListItem>9"</asp:ListItem>
                        <asp:ListItem>18"</asp:ListItem>

                    </asp:RadioButtonList>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-sm-6">
                    <h2>Fastener Information</h2>
                </div>
            </div>

            <%--Fastener Manufacturer--%>
            <div class="row">
                <div class="col-sm-10">
                    <asp:Label ID="lblFastManu" runat="server" CssClass="requiredRed" Text="Label">
                        Fastener Manufacturer:
                    </asp:Label>
                    <asp:Label ID="lblFTypeList" runat="server" Text="">
                            
                    </asp:Label>
                </div>
               
            </div>
            <div class="row">
                 <div class="col-sm-6">
                    <asp:TextBox ID="txtFastManu" CssClass="form-control" runat="server" Width="50%">
                    </asp:TextBox>
                  
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10">
                    <asp:Label ID="lblFastNum" runat="server" CssClass="requiredRed" Text="Label">
                        Fastener Part Number:
                    </asp:Label>
                    <asp:Label ID="lblFastNumDetail" runat="server" Text="">
                            (If no part number, please attach a signed spec/tolerance sheet):
                    </asp:Label>

                </div>
            </div>
            <div class="row">
                 <div class="col-sm-6">
                    <asp:TextBox ID="txtFastNum" CssClass="form-control" runat="server" Width="50%">
                    </asp:TextBox>
                  
                </div>
            </div>
             <%--Description of Fastener--%>
            <div class="row">
                <div class="col-sm-10">
                    <asp:Label ID="lblDescripFast" runat="server" Text="Label">
                        Description 
                    </asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:TextBox ID="txtDescripFast" CssClass="form-control" runat="server" TextMode="MultiLine" Columns="100" Rows="10"></asp:TextBox>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-6">
                    <h2>Part Information</h2>
                </div>
            </div>
            <%--Part Description--%>
            <div class="row">
                <div class="col-sm-10">
                    <asp:Label ID="lblPartDescr" runat="server" CssClass="requiredRed" Text="Label">
                        Part Description:
                    </asp:Label>
                    <asp:Label ID="lblPartDescrDetail" runat="server" Text="Label">
                            (Flat, Formed, Reverse flange, to be inserted in the flat, fastener near flange, etc. - part may be required.):
                    </asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:TextBox ID="txtPartDescr" CssClass="form-control" runat="server" TextMode="MultiLine" Columns="100" Rows="10"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-2">
                    <asp:RequiredFieldValidator ID="rfvPartDescr" runat="server" CssClass="requiredRed"
                        ErrorMessage="* Part Description Required" ControlToValidate="txtPartDescr">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
             <%--Part Material--%>
            <div class="row">
                <div class="col-sm-10">
                    <asp:Label ID="lblPartMaterial" runat="server" CssClass="requiredRed" Text="Label">
                        Part Material:
                    </asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:TextBox ID="txtPartMaterial" CssClass="form-control" runat="server" TextMode="MultiLine" Columns="100" Rows="10"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-2">
                    <asp:RequiredFieldValidator ID="rfvPartmaterial" runat="server" CssClass="requiredRed"
                        ErrorMessage="* Part Material Required" ControlToValidate="txtPartMaterial">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <br />
            </div> <%--End Form Group--%>
           <%--Attachments Form Group--%>
        <div class="form-group" >
            <div class="row">
                <div class="col-sm-10">
                    <asp:Label ID="lblAttach" runat="server" Text="Label">
                       <b>Attachment(s)</b> 
                    </asp:Label>
                    <asp:Label ID="lblAttachDetail" runat="server" Text="Label">
                        (Please attach any applicable drawings of sample parts or specifications needed -- file formats such as DWG, DXF, PDF, and DOC are preferred)
                    </asp:Label>
                </div>
            </div>
            <br />
             <div class="row">
                <div class="col-sm-6">
                    <asp:FileUpload ID="FileUpload1" runat="server" /><br />
              
                <asp:Label ID="Label1" runat="server"></asp:Label>
       
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:FileUpload ID="FileUpload2" runat="server" /><br />
               
                <asp:Label ID="Label2" runat="server"></asp:Label>
                
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:FileUpload ID="FileUpload3" runat="server" /><br />
               
                <asp:Label ID="Label3" runat="server"></asp:Label>
                
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:FileUpload ID="FileUpload4" runat="server" /><br />
               
                <asp:Label ID="Label4" runat="server"></asp:Label>
                
                </div>
            </div>

            </div> <%--End Attachments Form Group--%>
            
         <div class="row">
                 <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12 text-nowrap">
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    <span>
                        <asp:Label ID="lblConsentWrn" CssClass="pull-left" runat="server" BorderStyle="Solid" BorderColor="GreenYellow" Text="" Visible="false">
                             <i class="fa fa-arrow-down"></i> Consent must be checked 
                        </asp:Label>
                    </span>
                </div>
            </div>
            
            <%--GDPR Consent Checkbox--%>
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 text-nowrap">
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    <label class="container1">
                         <p>I have read the 
                            <asp:HyperLink ID="hlPrivacy" runat="server" NavigateUrl="/PrivacyPolicy" Target="_blank">Privacy Policy</asp:HyperLink>
                            /
                            <asp:HyperLink ID="hlTOS" runat="server" NavigateUrl="/TOS" Target="_blank">Terms of Use</asp:HyperLink>. 
                            I consent to the collection and use of the personal data submitted on the form above.</p>
                        <input id="gdprConsentBox" runat="server" type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                   
   
                </div>
                
            </div>

            <br />
            <br />



            <div class="form-group">        
              <div class="col-sm-6 col-sm-push-3">
                  <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-large btn-block btn-primary" OnClick="btnSubmit_Click" />
                  <asp:Label ID="lblResult" runat="server"></asp:Label>

                  <asp:ValidationSummary CssClass="alert-error" ID="ValidationSummary1" runat="server" />
              </div>
            </div>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OptInConsentConnectionString %>" SelectCommand="SELECT * FROM [CustToolingConsent]"></asp:SqlDataSource>



        </div> <%--End Conatiner--%>
</asp:Content>
